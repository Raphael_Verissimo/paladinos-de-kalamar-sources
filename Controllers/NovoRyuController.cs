using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Skills;
using Sprites;
using Actions;
using Commands;

namespace Characters
{
    public class NovoRyuController : HeroController
    {       
		void Start ()
		{
            this.ActorId = "Ryu";

			MovimentSpeed = 1.5f;
			RunningSpeed = 2.5f;
            ComboIndex = 1;

            Body = GetComponent<Rigidbody> ();
            SpriteController = GetComponentInChildren<SpriteController>();

            Direction = Vector3.right;
            IsFacingRight = true;

            SetUpSkills();
            SetUpStateMachie();
        }

        void FixedUpdate()
        {
            //Debug.Log("Posi��o do Heroi: " + transform.position);

            //Debug.Log("Speed :" + this.body.velocity);  
            UpdateLock();
            UpdateTimers();
        }

        #region Ryu Methods
        override internal void PlaySkill(CommandChainType command)
		{
            //Debug.Log("PlaySkill");

            // Caso esteja com seu cooldown pronto
            if (ReadySkills.Contains(CommandAllias[command]))
            {
                // TODO Fazer o controle de Lock e Unlock pelo Script de Skills
                ReadySkills.Remove(CommandAllias[command]);
                LockHero(LockTimes[CommandAllias[command]]);
                StartCoroutine(ResetCooldown(CommandAllias[command]));                

                switch (CommandAllias[command])
                {
                    // Moviment Skills
                    case SkillType.DASH:                        
                        PlayDash(command);
                    break;

                    case SkillType.JUMP:
                        PlayJump(command);
                    break;

                    // Attack Skills
                    case SkillType.JAB:
                    case SkillType.DIRETO:
                    case SkillType.CANELADA:
                    case SkillType.STRONG_KICK:
                        PlayAttack(command);
                    break;
					
                    case SkillType.HADOUKEN:
                        PlayHadouken(command);
                    break;

                    case SkillType.SHORYUKEN:
                        PlayShoryuken(command);
                    break;

                    case SkillType.ATTACK_DAS_CORUJA:
                        PlayAtaqueDasCoruja(command);
                    break;

                    case SkillType.SHINKU_HADOUKEN:
                        PlayShinkuHadouken(command);
                    break;
                }                
            }
		}
		
		override internal void MoveTo (Vector3 direction)
		{
            if (CanChangeState(CharacterState.WALKING))
            {
                // Set Current State 
                ChangeState(CharacterState.WALKING);

                Direction = direction;
                Velocity = Direction * MovimentSpeed;

                if (SpriteController)
                {
                    SpriteController.PlayLoop(AnimationType.Walk, IsFacingRight);
                }
            }			
        }
		
		override internal void HoldAttack ()
        {
            //Debug.Log("HoldAttack Attack !!!");
            IsHoldingAttack = true;
        }

        override internal void ReleaseAttack ()
        {
            //Debug.Log("Release Attack !!!");
            IsHoldingAttack = false;

            if (IsChargingSpecial) 
            {
                ShotSpecial ();
            }
        }
        #endregion

        private void SetUpStateMachie()
        {
            // Estado Dispon�vel
            PossibleChanges.Add(CharacterState.WAITING_FOR_IDLE, new List<CharacterState>
            {
                CharacterState.IDLE
            });

            PossibleChanges.Add(CharacterState.IDLE, new List<CharacterState>
            {
                CharacterState.IDLE, CharacterState.WAITING_FOR_IDLE,
                CharacterState.WALKING, CharacterState.DASHING,
                CharacterState.JUMPING, CharacterState.ATTACKING,
                CharacterState.ACTING, CharacterState.HITED
            });
            // ==================================================================

            // Estados de Movimento Interativo
            PossibleChanges.Add(CharacterState.WALKING, new List<CharacterState>
            {
                CharacterState.IDLE, CharacterState.WAITING_FOR_IDLE,
                CharacterState.WALKING, CharacterState.DASHING,
                CharacterState.JUMPING, CharacterState.ATTACKING,
                CharacterState.ACTING, CharacterState.HITED
            });
            
            PossibleChanges.Add(CharacterState.DASHING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.ACTING,
                CharacterState.JUMPING,
                CharacterState.ATTACKING,
                CharacterState.DASHATTACK,
                CharacterState.WAITING_FOR_IDLE,
            });

            PossibleChanges.Add(CharacterState.JUMPING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.ACTING,
                CharacterState.DASHING,
                CharacterState.ATTACKING,
                CharacterState.JUMPATTACK,
                CharacterState.WAITING_FOR_IDLE,
            });
            // ==================================================================

            // Estado de A��o 
            PossibleChanges.Add(CharacterState.ACTING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.WAITING_FOR_IDLE
            });

            PossibleChanges.Add(CharacterState.ATTACKING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.ATTACKING,
                CharacterState.WAITING_FOR_IDLE
            });

            PossibleChanges.Add(CharacterState.DASHATTACK, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.WAITING_FOR_IDLE
            });

            PossibleChanges.Add(CharacterState.JUMPATTACK, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.WAITING_FOR_IDLE
            });
            // ==================================================================

            // Estado de Interrup��o (Pode ser Atinjido em todos os estados)
            PossibleChanges.Add(CharacterState.HITED, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.PUSHED,
                CharacterState.FALLING,
                CharacterState.HITING_FLOOR,
                CharacterState.WAITING_FOR_IDLE,
            });
            // ==================================================================

            // Estados de Movimenta��o Passiva
            PossibleChanges.Add(CharacterState.PUSHED, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.FALLING,
                CharacterState.HITING_FLOOR
            });

            PossibleChanges.Add(CharacterState.FALLING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.HITING_FLOOR
            });

            PossibleChanges.Add(CharacterState.HITING_FLOOR, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.LAY_DOWN,
            });

            PossibleChanges.Add(CharacterState.LAY_DOWN, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.GETTING_UP
            });

            PossibleChanges.Add(CharacterState.GETTING_UP, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.WAITING_FOR_IDLE
            }); 
            // ====================================================================================================

            ChangeState(CharacterState.WAITING_FOR_IDLE);
        }

        private void SetUpSkills()
		{
            // ================== Skills de Movimento Interativo ================
            ReadySkills.Add(SkillType.DASH);
			LockTimes.Add (SkillType.DASH, 0f);    
			MaxCoolDowns.Add (SkillType.DASH, 2f); 

			CommandAllias.Add (CommandChainType.RUN_FORWARD, SkillType.DASH);
            CommandAllias.Add (CommandChainType.RUN_BACKWARD, SkillType.DASH);

            LockTimes.Add(SkillType.JUMP, 0.2f);
            MaxCoolDowns.Add(SkillType.JUMP, 1.5f);
            ReadySkills.Add(SkillType.JUMP);
            CommandAllias.Add(CommandChainType.JUMP, SkillType.JUMP);
            // ==================================================================

            // ======================== Skills de Attaque =======================
            AttackSequence.Add(SkillType.JAB);
            AttackSequence.Add(SkillType.DIRETO);
            AttackSequence.Add(SkillType.CANELADA);
            AttackSequence.Add(SkillType.STRONG_KICK);

            CommandAllias.Add(CommandChainType.NORMAL_ATTACK, SkillType.JAB);

            LockTimes.Add(SkillType.JAB, 0.25f);
            MaxCoolDowns.Add(SkillType.JAB, 0.25f);
            ReadySkills.Add(SkillType.JAB);

            LockTimes.Add(SkillType.DIRETO, 0.3f);
            MaxCoolDowns.Add(SkillType.DIRETO, 0.3f);
            ReadySkills.Add(SkillType.DIRETO);

            LockTimes.Add(SkillType.CANELADA, 0.4f);
            MaxCoolDowns.Add(SkillType.CANELADA, 0.4f);
            ReadySkills.Add(SkillType.CANELADA);

            LockTimes.Add(SkillType.STRONG_KICK, 1f);
            MaxCoolDowns.Add(SkillType.STRONG_KICK, 1f);
            ReadySkills.Add(SkillType.STRONG_KICK);

            LockTimes.Add(SkillType.DASH_ATTACK, 1f);
            MaxCoolDowns.Add(SkillType.DASH_ATTACK, 1f);
            ReadySkills.Add(SkillType.DASH_ATTACK);

            LockTimes.Add(SkillType.VOADORA, 1f);
            MaxCoolDowns.Add(SkillType.VOADORA, 1f);
            ReadySkills.Add(SkillType.VOADORA);

            // SKILLS 
            CommandAllias.Add(CommandChainType.DOWN_FORWARD_ATTACK, SkillType.HADOUKEN);
            CommandDirections.Add(CommandChainType.DOWN_FORWARD_ATTACK, true);

            CommandAllias.Add(CommandChainType.DOWN_BACKWARD_ATTACK, SkillType.HADOUKEN);
            CommandDirections.Add(CommandChainType.DOWN_BACKWARD_ATTACK, false);

            LockTimes.Add(SkillType.HADOUKEN, 1.5f);
            MaxCoolDowns.Add(SkillType.HADOUKEN, 5f);
            ReadySkills.Add(SkillType.HADOUKEN);

            CommandAllias.Add(CommandChainType.FORWARD_DOWN_FORWARD_ATTACK, SkillType.SHORYUKEN);
            CommandDirections.Add(CommandChainType.FORWARD_DOWN_FORWARD_ATTACK, true);

            CommandAllias.Add(CommandChainType.BACK_DOWN_BACKWARD_ATTACK, SkillType.SHORYUKEN);
            CommandDirections.Add(CommandChainType.BACK_DOWN_BACKWARD_ATTACK, false);

            LockTimes.Add(SkillType.SHORYUKEN, 1.8f);
            MaxCoolDowns.Add(SkillType.SHORYUKEN, 5f);
            ReadySkills.Add(SkillType.SHORYUKEN);

            CommandAllias.Add(CommandChainType.FORWARD_DOWN_BACK_ATTACK, SkillType.ATTACK_DAS_CORUJA);
            CommandDirections.Add(CommandChainType.FORWARD_DOWN_BACK_ATTACK, true);

            CommandAllias.Add(CommandChainType.BACK_DOWN_FORWARD_ATTACK, SkillType.ATTACK_DAS_CORUJA);
            CommandDirections.Add(CommandChainType.BACK_DOWN_FORWARD_ATTACK, false);

            LockTimes.Add(SkillType.ATTACK_DAS_CORUJA, 2f);
            MaxCoolDowns.Add(SkillType.ATTACK_DAS_CORUJA, 5f);
            ReadySkills.Add(SkillType.ATTACK_DAS_CORUJA);

            CommandAllias.Add(CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK, SkillType.SHINKU_HADOUKEN);
            CommandDirections.Add(CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK, true);

            CommandAllias.Add(CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK, SkillType.SHINKU_HADOUKEN);
            CommandDirections.Add(CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK, false);

            LockTimes.Add(SkillType.SHINKU_HADOUKEN, 100f);
            MaxCoolDowns.Add(SkillType.SHINKU_HADOUKEN, 10f);
            ReadySkills.Add(SkillType.SHINKU_HADOUKEN);
            // ==================================================================
        }

        private void PlayDash(CommandChainType command)
		{
            if (CanChangeState(CharacterState.DASHING))
            {            
                ChangeState(CharacterState.DASHING);

                // Atualiza Dire��o
                Direction = Vector3.right;
                if (command == CommandChainType.RUN_BACKWARD) 
				    Direction = Vector3.left;

                if (SpriteController)
                {
                    SpriteController.Play(AnimationType.Run, IsFacingRight);
                }
                SkillManager.UseSkill (CommandAllias[command], this);
            }
        }

        private void PlayJump(CommandChainType command)
        {
            if (CanChangeState(CharacterState.JUMPING))
            {
                ChangeState(CharacterState.JUMPING);

                if (SpriteController)
                {
                    SpriteController.Play(AnimationType.Jump, IsFacingRight);
                }
                SkillManager.UseSkill(CommandAllias[command], this);
            }
        }

        private void PlayAttack(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ATTACKING))
            {
                switch (State)
                {
                    case CharacterState.IDLE:
                    case CharacterState.WALKING:
                    case CharacterState.ATTACKING:
                        PlaySequence(command);
                    break;

                    case CharacterState.DASHING:
                        PlayDashAttack();
                    break;

                    case CharacterState.JUMPING:
                        PlayJumpAttack();
                    break;
                }

                ChangeState(CharacterState.ATTACKING);
            }
        }

        private void PlaySequence(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ATTACKING))
            {
                CommandAllias[CommandChainType.NORMAL_ATTACK] = AttackSequence[ComboIndex-1];

                switch(CommandAllias[CommandChainType.NORMAL_ATTACK])
                {
                    case SkillType.JAB:
                    SequenceJab();
                    break;

                    case SkillType.DIRETO:
                    SequenceDireto();
                    break;

                    case SkillType.CANELADA:
                    SequenceCanelada();
                    break;

                    case SkillType.STRONG_KICK:
                    SequenceStrongKick();
                    break;
                }

                StartCoroutine(ResetCooldown(CommandAllias[command]));
                LockHero(LockTimes[CommandAllias[command]]);

                ComboIndex--;
                ComboIndex = ((ComboIndex + 1) % AttackSequence.Count);
                ComboIndex++;
            }
        }

        private void PlayDashAttack()
        {
            if (CanChangeState(CharacterState.DASHATTACK))
            {
                ChangeState(CharacterState.DASHATTACK);

                GameObject skillGO = SkillManager.UseSkill(SkillType.DASH_ATTACK, this);
                BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

                // Defini��o do Strong Punch
                float width = 1.0f;
                float maxTime = 0.4f;
                float coolDown = 0.2f;
                float movimentCoolDown = 0f;

                Vector3 offset = new Vector3(0.4f, 0.2f, 0);
                Vector3 moviment = this.Body.velocity;
                Vector3 pushForce = new Vector3(4 * Direction.x, 2f, 0);

                attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

                if (SpriteController)
                    SpriteController.Play(AnimationType.DashAttack, IsFacingRight);
            }                
        }

        private void PlayJumpAttack()
        {
            if (CanChangeState(CharacterState.JUMPATTACK))
            {
                ChangeState(CharacterState.JUMPATTACK);

                GameObject skillGO = SkillManager.UseSkill(SkillType.VOADORA, this);
                BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

                // Defini��o do Strong Punch
                float width = 0.6f;
                float maxTime = 0.5f;
                float coolDown = 0.2f;
                float movimentCoolDown = 0f;

                Vector3 offset = new Vector3(0.4f, 0f, 0);
                Vector3 moviment = this.Body.velocity;
                Vector3 pushForce = new Vector3(4 * Direction.x, 2f, 0);

                attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

                if (SpriteController)
                    SpriteController.Play(AnimationType.Voadora, IsFacingRight);
            }
        }

        private void PlayHadouken(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ACTING))
            {
                ChangeState(CharacterState.ACTING);

                GameObject skillGO = SkillManager.UseSkill(SkillType.HADOUKEN, this);
                Hadouken hadouken = (Hadouken)skillGO.GetComponent<Hadouken>();

                // Defini��o do Strong Punch
                int maxHits = 1;
                float width = 1f;
                float range = 10f;
                float speed = 5f;
                float hitSpeed = 2f;
                float coolDown = 0.3f;
                float hitCoolDown = 0.3f;

                Vector3 offset = new Vector3(transform.position.x + (Direction.x * 0.5f), 1.5f, -0.1f);
                Vector3 pushForce = new Vector3(1 * Direction.x, 2f, 0);

                hadouken.UseAction(this, width, range, speed, hitSpeed, maxHits, coolDown, hitCoolDown, offset, pushForce);

                if (SpriteController)
                    SpriteController.Play(AnimationType.Skill1, IsFacingRight);
            }
        }

        private void PlayShoryuken(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ACTING))
            {
                ChangeState(CharacterState.ACTING);

                GameObject skillGO = SkillManager.UseSkill(SkillType.SHORYUKEN, this);
                Shoryuken shoryuken = (Shoryuken)skillGO.GetComponent<Shoryuken>();

                // Defini��o do Strong Punch
                int maxHits = 3;
                float width = 1.2f;
                float speed = 5f;
                float hitSpeed = 2f;
                float coolDown = 0.3f;
                float hitCoolDown = 0.3f;

                Vector3 offset = new Vector3(0.1f, 0.2f, 0.1f);
                Vector3 moviment = new Vector3(0.5f * Direction.x, 6f, 0);
                Vector3 pushForce = new Vector3(1 * Direction.x, 2f, 0);

                shoryuken.UseAction(this, width, speed, hitSpeed, maxHits, coolDown, hitCoolDown, offset, moviment, pushForce);

                if (SpriteController)
                    SpriteController.Play(AnimationType.Skill2, IsFacingRight);
            }
        }

        private void PlayAtaqueDasCoruja(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ACTING))
            {
                ChangeState(CharacterState.ACTING);

                GameObject skillGO = SkillManager.UseSkill(SkillType.ATTACK_DAS_CORUJA, this);
                AtaqueDasCoruja ataqueDasCoruja = (AtaqueDasCoruja)skillGO.GetComponent<AtaqueDasCoruja>();

                // Defini��o do Strong Punch
                int maxHits = 3;
                float width = 1f;
                float range = 3f;
                float speed = 2.5f;
                float hitSpeed = 2f;
                float coolDown = 0.3f;
                float hitCoolDown = 0.3f;

                Vector3 offset = new Vector3(0.1f, 0.2f, 0.1f);
                Vector3 moviment = new Vector3(3f * Direction.x, 0f, 0);
                Vector3 pushForce = new Vector3(1 * Direction.x, 2f, 0);

                ataqueDasCoruja.UseAction(this, width, range, speed, hitSpeed, maxHits, coolDown, hitCoolDown, offset, moviment, pushForce);

                if (SpriteController)
                    SpriteController.Play(AnimationType.Skill3, IsFacingRight);
            }
        }

        private void PlayShinkuHadouken(CommandChainType command)
        {
			if (CanChangeState(CharacterState.ACTING))
            {
                ChangeState(CharacterState.ACTING);

                GameObject skillGO = SkillManager.UseSkill(SkillType.SHINKU_HADOUKEN, this);
                ShinkuHadouken shinkuHadouken = (ShinkuHadouken)skillGO.GetComponent<ShinkuHadouken>();

                // Defini��o do Shinku Hadouken
                int maxHits = 3;
                float width = 1f;
                float range = 3f;
                float speed = 2.5f;
                float hitSpeed = 2f;
                float coolDown = 0.3f;
                float hitCoolDown = 0.3f;

                Vector3 offset = new Vector3(0.1f, 0.2f, 0.1f);
                Vector3 moviment = new Vector3(3f * Direction.x, 0f, 0);
                Vector3 pushForce = new Vector3(1 * Direction.x, 2f, 0);

                shinkuHadouken.UseAction(this, width, range, speed, hitSpeed, maxHits, coolDown, hitCoolDown, offset, pushForce);

				if (SpriteController)
				{
					//spriteController.PlaySpecial1(isFacingRight);
					IList<AnimationType> actions = new List<AnimationType>();

					actions.Add(AnimationType.Special1);
					actions.Add(AnimationType.Special2);

                    SpriteController.PlaySequence(actions, IsFacingRight);
					//spriteController.PlaySpecial1(isFacingRight);

					EffectsSources.clip = SpecialStartClip;
					EffectsSources.Play();

					StartCoroutine(DelayedVoice(ChargingSpecialVoice, 1.8f));
				} 
            }
        }

        private void SequenceJab()
        {
            GameObject skillGO = SkillManager.UseSkill(SkillType.JAB, this);
            BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

            // Defini��o do Jab
            float width = 0.4f;
            float maxTime = 0.2f;
            float coolDown = 0.1f;
            float movimentCoolDown = 0f;

            Vector3 offset = new Vector3(0.6f, 0.4f, 0);
            Vector3 moviment = new Vector3(0.3f * Direction.x, 0.5f, 0);    
            Vector3 pushForce = new Vector3(2 * Direction.x, 2f, 0);

            attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

            if(SpriteController)
                SpriteController.Play(AnimationType.Attack1, IsFacingRight);
        }

        private void SequenceDireto()
        {
            GameObject skillGO = SkillManager.UseSkill(SkillType.DIRETO, this);
            BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

            // Defini��o do Direto
            float width = 0.5f;
            float maxTime = 0.25f;
            float coolDown = 0.1f;
            float movimentCoolDown = 0f;

            Vector3 offset = new Vector3(0.7f, 0.3f, 0);
            Vector3 moviment = new Vector3(0.6f * Direction.x, 1f, 0);
            Vector3 pushForce = new Vector3(0.5f * Direction.x, 1f, 0);

            attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

            if (SpriteController)
                SpriteController.Play(AnimationType.Attack2, IsFacingRight);
        }

        private void SequenceCanelada()
        {
            GameObject skillGO = SkillManager.UseSkill(SkillType.KICK, this);
            BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

            // Defini��o do Kick
            float width = 0.6f;
            float maxTime = 0.3f;
            float coolDown = 0.15f;
            float movimentCoolDown = 0f;

            Vector3 offset = new Vector3(0.5f, 0f, 0);
            Vector3 moviment = new Vector3(2f * Direction.x, 1.5f, 0);
            Vector3 pushForce = new Vector3(0.5f * Direction.x, 1f, 0);

            attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

            if (SpriteController)
                SpriteController.Play(AnimationType.Attack4, IsFacingRight);
        }
        
        private void SequenceStrongKick()
        {
            GameObject skillGO = SkillManager.UseSkill(SkillType.STRONG_KICK, this);
            BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

            // Defini��o do Strong Kick
            float width = 0.8f;
            float maxTime = 0.5f;
            float coolDown = 0.2f;
            float movimentCoolDown = 0f;

            Vector3 offset = new Vector3(0.4f, 0.3f, 0);
            Vector3 moviment = new Vector3(0.3f * Direction.x, 2f, 0);
            Vector3 pushForce = new Vector3(0.5f * Direction.x, 1f, 0);

            attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

            if (SpriteController)
                SpriteController.Play(AnimationType.ForceAttack, IsFacingRight);
        } 

        private void ShotSpecial()
		{
            IsChargingSpecial = false;
            LockHero(0.3f);

			GetComponentInChildren<ShinkuHadouken> ().OnShot ();

			if (SpriteController)
			{
                //spriteController.PlaySpecial1(isFacingRight);
                IList<AnimationType> actions = new List<AnimationType>();

                actions.Add(AnimationType.Special3);
                actions.Add(AnimationType.Idle);

                SpriteController.PlaySequence(actions, IsFacingRight);

                VoiceSources.clip = ShotSpecialVoice;
                VoiceSources.Play();
			} 
		}

        IEnumerator RecursiveChargeSpecial(float delay)
        {
            IsChargingSpecial = true;

            yield return new WaitForSeconds (delay);

            if (IsHoldingAttack)
            {
                if (SpriteController && IsChargingSpecial) 
                {
                    SpriteController.PlaySpecial2 (IsFacingRight);

                    GetComponentInChildren<ShinkuHadouken> ().OnCharge ();

                    if (IsChargingSpecial)
                        StartCoroutine (RecursiveChargeSpecial (0.9f));
                } 
            }
        }

        IEnumerator ResetCooldown(SkillType skill)
		{
			//Debug.Log ("SkillType:" + skill + " cooldown:" + MaxCoolDowns [skill]);
			yield return new WaitForSeconds (MaxCoolDowns[skill]);

			ReadySkills.Add(skill);
			//Debug.Log ("SkillType:" + skill + " Ready");
		}

        IEnumerator DelayedVoice(AudioClip clip, float delay)
        {
            yield return new WaitForSeconds(delay);

            VoiceSources.clip = clip;
            VoiceSources.Play();
        }
    }
}