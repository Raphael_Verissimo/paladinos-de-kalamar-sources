using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Skills;
using Sprites;
using Actions;
using Commands;
using Effects;

namespace Characters
{
	public class CharacterController : MonoBehaviour
	{ 
        public AudioClip SpecialStartClip;
        public AudioClip ChargingSpecialClip;
        public AudioClip ShotSpecialClip;

        public AudioClip SpecialStartVoice;
        public AudioClip ChargingSpecialVoice;
        public AudioClip ShotSpecialVoice;

        public AudioSource effectsSources;
        public AudioSource voiceSources;

		internal CharacterModel model;

		internal bool isMoving;
        internal bool isRunning;
        internal bool isJumping;
		internal bool isLocked;
		internal bool isPushing;
		internal bool isHited;
        internal bool isHiting;
        internal bool isFalling;
        internal bool isMovingAction;
        internal bool isRunningAttack;
        internal bool isJumpingAttack;

		internal float movimentSpeedFactor;
		internal float runningSpeedFactor;
        public Vector3 currentDirection;
        public float  currentrVelocity;
		internal float originalY;

		public Vector3 movingActionDirection;

		internal float timeToPush;
		internal float timeToPushCoolDown;

		internal float timeToResume;
		internal float timeToResumeCoolDown;

        internal CommandChainType lastLockedCommand;
        internal CommandChainType lastRecievedCommand;

		internal IDictionary<CommandChainType, SkillType> commandAllias = new Dictionary<CommandChainType, SkillType> ();
		internal IDictionary<CommandChainType, bool> commandDirections = new Dictionary<CommandChainType, bool> ();
		internal IDictionary<SkillType, float> maxCoolDowns = new Dictionary<SkillType, float> ();
		internal IDictionary<SkillType, float> lockTimes = new Dictionary<SkillType, float> ();
		internal IList<SkillType> availableSkills =  new List<SkillType>();
        internal IList<SkillType> attackSequence =  new List<SkillType>();

		//internal Animator characterAnimator;
		internal Rigidbody body;

		internal bool testAttack;

        internal SpriteController spriteController;
        internal AudioSource audioSource;

		internal bool isFacingRight;
		internal bool onCharge;
		internal bool isChargingSpecial;
		internal bool isHoldingAttack;

        internal int comboIndex;
        internal bool startCombo;
        internal float comboTimmer;
        internal float comboCooldown;

		internal CharacterState currentState;
		internal CharacterState lastState;

        internal Vector3 Direction
		{
			get
			{
				if (currentDirection == Vector3.zero)
				{
					return new Vector3 (1, 0, 0);
				}

				if (currentDirection.x > 0) 
				{
					return new Vector3 (1, 0, 0);
				}
				else
				{
					return new Vector3 (-1, 0, 0);
				}
			}
		}

        internal Vector3 CurrentDirection
        {
            get
            {
                return currentDirection;
            }
        }

        internal Vector3 CurrentVelocity
        {
            get
            {
                return body.velocity;
            }
        }

        internal CharacterModel Model
		{
			get
			{
				return model;
			}
		}

		void Start ()
		{					
            body = GetComponent<Rigidbody> ();
		}

		void FixedUpdate ()
		{
			if (lastState != currentState) 
			{
				lastState = currentState;
				Debug.Log ("Current State : " + currentState);
			}				

			// TODO: Mover para controle de movimento
			if (isMoving && !isLocked && !isMovingAction)
            {                        
                //Debug.Log("Direction: " + currentDirection);

                body.velocity = currentDirection * currentrVelocity;

                //if (this.gameObject.name.Equals("Goku"))
                //    Debug.Log(this.gameObject.name + " is Moving to: " + currentDirection + " at :"+currentrVelocity +"speed");
                
                //Debug.Log("velocity: " + body.velocity);
			}
			else
			{
                //if (this.gameObject.name.Equals("Goku"))
                //    Debug.Log(this.gameObject.name + " is Trying to Move but is Locked:"+isLocked + " or is Hited:" + isHited + " or is Pushed:" + isPushing + " or is MovingAction:" + isMovingAction + " moving status:" + isMoving);
                                
				if (!isPushing && !isMovingAction)
				{
					//body.velocity = Vector3.zero;
				}
			}

            if (isRunning)
            {
                body.velocity = new Vector3(body.velocity.x * 0.93f, body.velocity.y, body.velocity.z);
            }
		}

		#region ReceivedCommands
        internal void ApplyCommand (CommandChainType command, Vector3 characterPos)
		{				
			//Debug.Log("Apply Command: " + command);

            if (!isLocked && !isHited)
			{
				switch (command)
				{
				    case CommandChainType.MOVE_UP:
					    MoveTo (characterPos, new Vector3 (0, 0, 1));
					    break;
				    case CommandChainType.MOVE_UP_FORWARD:
					    MoveTo (characterPos, new Vector3 (1, 0, 1));
					    break;
				    case CommandChainType.MOVE_FORWARD:
					    MoveTo (characterPos, new Vector3 (1, 0, 0));
					    break;
				    case CommandChainType.MOVE_FORWARD_DOWN:
					    MoveTo (characterPos, new Vector3 (1, 0, -1));
					    break;
				    case CommandChainType.MOVE_DOWN:
					    MoveTo (characterPos, new Vector3 (0, 0, -1));
					    break;
				    case CommandChainType.MOVE_DOWN_BACKWARD:
					    MoveTo (characterPos, new Vector3 (-1, 0, -1));
					    break;
				    case CommandChainType.MOVE_BACKWARD:
					    MoveTo (characterPos, new Vector3 (-1, 0, 0));
					    break;
				    case CommandChainType.MOVE_BACKWARD_UP:
					    MoveTo (characterPos, new Vector3 (-1, 0, 1));
					    break;

				    case CommandChainType.RUN_UP:
					    DashTo (characterPos, new Vector3 (0, 0, 1));
					    break;
				    case CommandChainType.RUN_UP_FORWARD:
					    DashTo (characterPos, new Vector3 (1, 0, 1));
					    break;
				    case CommandChainType.RUN_FORWARD:
					    DashTo (characterPos, new Vector3 (1, 0, 0));
					    break;
				    case CommandChainType.RUN_FORWARD_DOWN:
					    DashTo (characterPos, new Vector3 (1, 0, -1));
					    break;
				    case CommandChainType.RUN_DOWN:
					    DashTo (characterPos, new Vector3 (0, 0, -1));
					    break;
				    case CommandChainType.RUN_DOWN_BACKWARD:
					    DashTo (characterPos, new Vector3 (-1, 0, -1));
					    break;
				    case CommandChainType.RUN_BACKWARD:
					    DashTo (characterPos, new Vector3 (-1, 0, 0));
					    break;
				    case CommandChainType.RUN_BACKWARD_UP:
					    DashTo (characterPos, new Vector3 (-1, 0, 1));
					    break;

                    case CommandChainType.JUMP:
                        Jump ();
                    break;  

				    case CommandChainType.STOP:
                        Idle ();
				    break;	

				    default: 
						    if (commandAllias.ContainsKey(command))
						    {												
							    if (availableSkills.Contains (commandAllias [command])) 
							    {
                                    PlaySkill (command);                            
                                    PlaySkillAnimation (commandAllias [command]);
							    }							
						    }
				    break;
				}

                //Debug.Log("Add Last Command: " + command);
                lastRecievedCommand = command;
			}
            else
            {
                //Debug.Log("Command: " + command + " Is Running: " + isRunning);

                //if (this.gameObject.name.Equals("Goku"))
                //    Debug.Log(this.gameObject.name + " is Trying to: " + command + " but is Locked:"+isLocked + " or is Hited:" + isHited);

                lastLockedCommand = command;
            }

            if (isRunning && !isRunningAttack)
            {                
                if (command == CommandChainType.NORMAL_ATTACK)
                {
                    isRunningAttack = true;
                    Debug.Log("Running Attack ");

                    PlaySkill (CommandChainType.NORMAL_ATTACK);                            
                    PlaySkillAnimation (SkillType.DASH_ATTACK);   
                }
            }

            if (isJumping && !isJumpingAttack)
            {                
                if (command == CommandChainType.NORMAL_ATTACK)
                {
                    isJumpingAttack = true;
                    Debug.Log("Jumping Attack ");

                    PlaySkill (CommandChainType.NORMAL_ATTACK);                            
                    PlaySkillAnimation (SkillType.VOADORA);   
                }
            }

			if (command == CommandChainType.HOLD_ATTACK)
				HoldAttack ();

			if (command == CommandChainType.RELEASE_ATTACK)
				ReleaseAttack ();
			
		}

        internal void ApplyCommand(CommandChainType command, float delay)
        {
            StartCoroutine(DelayedApplyCommand(command, delay));
        }

        internal void ApplyDamage (DamageType damageType, int amout, EffectType effectType, int effectAmount, float effetcDuration)
		{
			Debug.Log ("Apply Damage !!!");

			// TODO processar dano

			// TODO processar effeito
		}

        internal void ChangeDirection(float delay)
        {
            StartCoroutine(DelayedChangeDirection(delay));
        }

        internal void ChangeDirection()
        {
            currentDirection = new Vector3(-currentDirection.x, currentDirection.y, currentDirection.z);

            if (currentDirection.x > 0)
                isFacingRight = true;

            if (currentDirection.x < 0)
                isFacingRight = false;
        }

        internal void ChangePosition(Vector3 position, float delay)
        {
            StartCoroutine(DelayedChangePosition(position, delay));
        }

        internal void ApplyActionMoviment(Vector3 actionDirection, float delay, bool useGravity)
		{
			StartCoroutine(DelayedActionMoviment(actionDirection, delay, useGravity));
		}

        //internal void PushCharacter(Vector3 pushForce, float delay)
		//{
			//StartCoroutine (DelayedPush (pushForce, delay));
		//}

        internal void PushCharacter(Vector3 pushForce, float delay)
		{
            HitCharacter (pushForce, delay);
		}

        internal void LandCharacter()
		{
			Land ();
		}

        internal void LoseGame()
        {
            HitFloor (100f);
            //spriteController.PlayHitFloor(isFacingRight);

            isLocked = true;

            if (this.gameObject.tag.Equals("MainPlayer"))
            {
                Vector3 panelPosition = this.transform.position;
                panelPosition.y = panelPosition.y + 2f;

                EffectsManager.PlayEffect(EffectType.LOSE_PANEL, panelPosition);
            }
            else
            {
                //GameManager.MainPlayer.WinGame();
            }
        }

        internal void WinGame()
        {
            isLocked = true;

            if (this.gameObject.tag.Equals("MainPlayer"))
            {
                Vector3 panelPosition = this.transform.position;
                panelPosition.y = panelPosition.y + 2f;

                EffectsManager.PlayEffect(EffectType.WIN_PANEL, panelPosition);
            }
            else
            {
                Idle();
                this.GetComponent<EnemyController>().gameObject.SetActive(true);
                isLocked = true;
            }
        }

        // Interrompe o Movimento e Animação por um periodo de tempo.
        internal void ActionHitDelayedResume(float delay)
		{
			StartCoroutine (DelayedResumeSpeed (delay));
			spriteController.PauseAnimation (delay);
		}

        // Interrompe os movimentos feito por skills
        internal void DashLand ()
        { 
            ResetSequence();

            if (isFacingRight)
                spriteController.PlayLand(true);
            else
                spriteController.PlayLand(false); 

            //currentrVelocity = currentrVelocity * 0.7f;
            //body.useGravity = true;
            body.velocity = new Vector3(body.velocity.x * 0.4f, body.velocity.y - 1f, body.velocity.z);

            StartCoroutine(DelayedStopActionMoviment(0.3f));  
        }

        // Interrompe os movimentos feito por skills
        internal void StopActionMoviment ()
		{			
            ResetSequence();

            currentrVelocity = 0;
			body.useGravity = true;
            body.velocity = Vector3.zero;

            //Debug.Log("Apply Last Current Command:" + CommandManager.GetCurrentCommand);
            ResumeLastMoviment();
		}

        internal void ResumeLastMoviment()
        {
            CommandType currentCommand = CommandManager.GetCurrentCommand;
            isLocked = false;

            switch (currentCommand)
            {                
                case CommandType.HOLD_KEY_UP:
                    ApplyCommand(CommandChainType.MOVE_UP, this.gameObject.transform.position);
                break;

                case CommandType.HOLD_KEY_UP_FORWARD:
                    ApplyCommand(CommandChainType.MOVE_UP_FORWARD, this.gameObject.transform.position);
                break;

                case CommandType.HOLD_KEY_FORWARD:
                    ApplyCommand(CommandChainType.MOVE_FORWARD, this.gameObject.transform.position);
                break;

                case CommandType.HOLD_KEY_FORWARD_DOWN:
                    ApplyCommand(CommandChainType.MOVE_FORWARD_DOWN, this.gameObject.transform.position);
                break;

                case CommandType.HOLD_KEY_DOWN:
                    ApplyCommand(CommandChainType.MOVE_DOWN, this.gameObject.transform.position);
                break;

                case CommandType.HOLD_KEY_DOWN_BACKWARD:
                    ApplyCommand(CommandChainType.MOVE_DOWN_BACKWARD, this.gameObject.transform.position);
                break;

                case CommandType.HOLD_KEY_BACKWARD:
                    ApplyCommand(CommandChainType.MOVE_BACKWARD, this.gameObject.transform.position);
                break;

                case CommandType.HOLD_KEY_BACKWARD_UP:
                    ApplyCommand(CommandChainType.MOVE_BACKWARD_UP, this.gameObject.transform.position);
                break;
            }
        }

        // Para os movimentos e retorna a Idle 
        internal void Idle ()
		{
            //StopActionMoviment();
			currentState = CharacterState.IDLE;

            isMoving = false;
            isRunning = false;
            //isJumping = false;
            isLocked = false;
            isPushing = false;
            isHited = false;
            isHiting = false;
            isFalling = false;
            isMovingAction = false;
            isRunningAttack = false;
            isJumpingAttack = false;

			if (spriteController)
			{
				spriteController.PlayIdle(isFacingRight);
			} 
		} 

        // Para os movimentos e retorna a Idle 
        internal void Jump ()
        {
			currentState = CharacterState.JUMPING;

            isJumping = true;
            PlaySkill(CommandChainType.JUMP);
            
            if (spriteController)
            {
                spriteController.PlayJump(isFacingRight);
            } 
        } 

        // Acerta o chão, tocando a animação enqt espera para voltar a idle; 
        internal void HitFloor (float delay)
        {
			currentState = CharacterState.HITING_FLOOR;

            spriteController.PlayHitFloor(isFacingRight);

            isMoving = false;
            isRunning = false;

            StartCoroutine(DelayedIdle(delay));
        }

        // Empurra o personagem na direção
        internal void Push(Vector3 pushForce)
        {
            if (!isPushing)
            {
                isPushing = true;
                body.useGravity = true;
                body.velocity = Vector3.zero;
                body.AddForce (pushForce, ForceMode.Impulse);
            }
        }

        // Recoloca o personagem no chão, difernte de hit floor ele imediatamente retorna ao idle.  
        internal void Land()
        {    
            if (isPushing || isMovingAction)
            {               
                ResetSequence();
                DissmissAction();

                isPushing = false;
                isMovingAction = false;
                body.useGravity = true;

                if (isFalling)
                {
                    isFalling = false;
                    HitFloor (0.8f);
                }
                else
                {
                    Idle ();
                }
            }

            if (isJumping)
            {
                ResumeLastMoviment();
                isJumping = false;
            } 
        }  

        // TODO REVER 
        internal void HitCharacter(Vector3 pushForce, float delay)
        {
            //UnityEditor.EditorApplication.isPaused = true;

            //DissmissAction();
            isPushing = false;
            isHoldingAttack = false;
            isChargingSpecial = false;

            if (pushForce.x > -0.5f && pushForce.x < 0.5f)
            {
				currentState = CharacterState.HITED;

                if (pushForce.x > 0)
                    spriteController.PlayHit1(false);
                else
                    spriteController.PlayHit1(true);                
            }
            else
            {
                if (pushForce.x > -1f && pushForce.x < 1f)
				{   
					currentState = CharacterState.PUSHED;

                    if (pushForce.x > 0)
                        spriteController.PlayHit2(false);
                    else
                        spriteController.PlayHit2(true);
                }
                else
				{
					currentState = CharacterState.FALLING;

                    isFalling = true;
                    isLocked = true;
                    isHited = true;

                    if (pushForce.x > 0)
                        spriteController.PlayFall(false);
                    else
                        spriteController.PlayFall(true);
                }
            }

            Push(pushForce);
        }

        internal void DissmissAction()
        {
            Action[] actions = this.GetComponentsInChildren<Action>();

            foreach(Action action in actions)
            {
                DestroyObject(action.gameObject);
            }
        }

        internal void DelayedStop(float delay)
        {
            StartCoroutine(DelayedStopActionMoviment(delay));
        }

        internal void DelayedLand(float delay)
        {
            //body.velocity = new Vector3(body.velocity.x * 0.7f, body.velocity.y, body.velocity.z);
            StartCoroutine(DelayedDashLand(delay));
        }

        internal void ResetSequence()
        {
            comboIndex = -1;
            startCombo = false;
            comboTimmer = 0.3f;
            comboCooldown = 0.5f;
            availableSkills.Add(SkillType.JAB);
            availableSkills.Add(SkillType.VOADORA);
        }
		#endregion

		#region Virtual Methods
        virtual internal void MoveTo (Vector3 origin, Vector3 direction) { }
        virtual internal void DashTo(Vector3 origin, Vector3 direction) { }

        virtual internal void HoldAttack () { }
        virtual internal void ReleaseAttack () { }
        virtual internal void ForceAttack (Vector3 origin, Vector3 direction) { }
        virtual internal void RunningAttack (Vector3 origin, Vector3 direction) { }
        virtual internal void DirectionalAttack (Vector3 origin, Vector3 direction) { }		

        virtual internal void PlaySkill(CommandChainType command) { }
		virtual internal void PlaySkillAnimation(SkillType type) { }

        //virtual internal void HitCharacter(Vector3 pushForce) { }
		#endregion

        IEnumerator DelayedActionMoviment(Vector3 actionDirection, float delay, bool useGravity)
        {
            yield return new WaitForSeconds (delay);

            if (!isMovingAction)
            {
                isMovingAction = true;
                body.useGravity = useGravity;
                movingActionDirection = actionDirection;

                body.AddForce (actionDirection, ForceMode.Impulse);
            }

            body.velocity = Vector3.zero;
        }

        IEnumerator DelayedResumeSpeed(float delay)
        {
            Vector3 currentSpeed = body.velocity;
            body.isKinematic = true;
            body.velocity = Vector3.zero;

            yield return new WaitForSeconds (delay);

            body.isKinematic = false;
            body.AddRelativeForce(currentSpeed, ForceMode.Impulse); 
        }

        IEnumerator DelayedChangePosition(Vector3 position, float delay)
        {           
            yield return new WaitForSeconds (delay);

            this.transform.position = position;
        }

        IEnumerator DelayedApplyCommand(CommandChainType command, float delay)
        {           
            yield return new WaitForSeconds (delay);

            ApplyCommand(command, this.transform.position);
        }

        IEnumerator DelayedChangeDirection(float delay)
        {           
            yield return new WaitForSeconds (delay);

            ChangeDirection();
        }

        IEnumerator DelayedIdle(float delay)
        {
            yield return new WaitForSeconds (delay);
            Idle();
        }

        IEnumerator DelayedUnlock(float delay)
        {
            yield return new WaitForSeconds (delay);
            isLocked = false;
        }

        IEnumerator DelayedStopActionMoviment(float delay)
        {
            yield return new WaitForSeconds (delay);
            StopActionMoviment();
        }

        IEnumerator DelayedDashLand(float delay)
        {
            yield return new WaitForSeconds (delay);
            DashLand();
        }
	}
}