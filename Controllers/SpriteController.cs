﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprites
{
    public class SpriteController : MonoBehaviour
    {

        [SerializeField]
        private Sprite[] idleSpriteSheet;
        public float idleSpriteSheetInterval;

        [SerializeField]
        private Sprite[] walkSpriteSheet;
        public float walkSpriteSheetInterval;

        [SerializeField]
        private Sprite[] runSpriteSheet;
        public float runSpriteSheetInterval;

        [SerializeField]
        private Sprite[] attack1SpriteSheet;
        public float attack1SpriteSheetInterval;

        [SerializeField]
        private Sprite[] attack2SpriteSheet;
        public float attack2SpriteSheetInterval;

        [SerializeField]
        private Sprite[] attack3SpriteSheet;
        public float attack3SpriteSheetInterval;

        [SerializeField]
        private Sprite[] attack4SpriteSheet;
        public float attack4SpriteSheetInterval;

        [SerializeField]
        private Sprite[] attack5SpriteSheet;
        public float attack5SpriteSheetInterval;

        [SerializeField]
        private Sprite[] forceAttackSpriteSheet;
        public float forceAttackSpriteSheetInterval;

        [SerializeField]
        private Sprite[] skill1SpriteSheet;
        public float skill1SpriteSheetInterval;

        [SerializeField]
        private Sprite[] skill2SpriteSheet;
        public float skill2SpriteSheetInterval;

        [SerializeField]
        private Sprite[] skill3SpriteSheet;
        public float skill3SpriteSheetInterval;

        [SerializeField]
        private Sprite[] specialSpriteSheet1;
        public float specialSpriteSheet1Interval;

        [SerializeField]
        private Sprite[] specialSpriteSheet2;
        public float specialSpriteSheet2Interval;

        [SerializeField]
        private Sprite[] specialSpriteSheet3;
        public float specialSpriteSheet3Interval;

        [SerializeField]
        private Sprite[] specialSpriteSheet4;
        public float specialSpriteSheet4Interval;

        [SerializeField]
        private Sprite[] specialSpriteSheet5;
        public float specialSpriteSheet5Interval;

        [SerializeField]
        private Sprite[] hitSpriteSheet1;
        public float hitSpriteSheet1Interval;

        [SerializeField]
        private Sprite[] hitSpriteSheet2;
        public float hitSpriteSheet2Interval;

        [SerializeField]
        private Sprite[] fallSpriteSheet;
        public float fallSpriteSheetInterval;

        [SerializeField]
        private Sprite[] hitFloorSpriteSheet;
        public float hitFloorSpriteSheetInterval;

        [SerializeField]
        private Sprite[] landSpriteSheet;
        public float landSpriteSheetInterval;

        [SerializeField]
        private Sprite[] dashAttackSpriteSheet;
        public float dashAttackSpriteSheetInterval;

        [SerializeField]
        private Sprite[] jumpSpriteSheet;
        public float jumpSpriteSheetInterval;

        [SerializeField]
        private Sprite[] voadoraSpriteSheet;
        public float voadoraSpriteSheetInterval;

        private float lastTime;

        private int currentIndex;
        private SpriteRenderer spriteRenderer;
        private Sprite[] currentSpriteSheet;
        private Vector3 originalDirection;
        private Vector3 invertDirection;
        private bool facingRight;
        private bool isOneShot;
        private bool isContinuos;
        private bool isPaused;
        private bool isSequence;
        private int sequenceIndex;
        private IList<AnimationType> animationSequence;

        private AnimationType currentAction;

        private IDictionary<Sprite[], float> Intervals;

        private bool isPlaying;

        // Use this for initialization
        void Start()
        {
            isPaused = false;
            isPlaying = false;
            currentAction = AnimationType.Idle;
            currentSpriteSheet = idleSpriteSheet;
            animationSequence = new List<AnimationType>();

            sequenceIndex = 0;
            currentIndex = 0;
            lastTime = 0;
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();

            facingRight = true;

            originalDirection = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
            invertDirection = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            Intervals = new Dictionary<Sprite[], float>();
            Intervals.Add(idleSpriteSheet, idleSpriteSheetInterval);
            Intervals.Add(walkSpriteSheet, walkSpriteSheetInterval);
            Intervals.Add(runSpriteSheet, runSpriteSheetInterval);
            Intervals.Add(attack1SpriteSheet, attack1SpriteSheetInterval);
            Intervals.Add(attack2SpriteSheet, attack2SpriteSheetInterval);
            Intervals.Add(attack3SpriteSheet, attack3SpriteSheetInterval);
            Intervals.Add(attack4SpriteSheet, attack4SpriteSheetInterval);
            Intervals.Add(attack5SpriteSheet, attack5SpriteSheetInterval);
            Intervals.Add(forceAttackSpriteSheet, forceAttackSpriteSheetInterval);
            Intervals.Add(skill1SpriteSheet, skill1SpriteSheetInterval);
            Intervals.Add(skill2SpriteSheet, skill2SpriteSheetInterval);
            Intervals.Add(skill3SpriteSheet, skill3SpriteSheetInterval);
            Intervals.Add(specialSpriteSheet1, specialSpriteSheet1Interval);
            Intervals.Add(specialSpriteSheet2, specialSpriteSheet2Interval);
            Intervals.Add(specialSpriteSheet3, specialSpriteSheet3Interval);
            Intervals.Add(specialSpriteSheet4, specialSpriteSheet4Interval);
            Intervals.Add(specialSpriteSheet5, specialSpriteSheet5Interval);
            Intervals.Add(hitSpriteSheet1, hitSpriteSheet1Interval);
            Intervals.Add(hitSpriteSheet2, hitSpriteSheet2Interval);
            Intervals.Add(fallSpriteSheet, fallSpriteSheetInterval);
            Intervals.Add(hitFloorSpriteSheet, hitFloorSpriteSheetInterval);
            Intervals.Add(landSpriteSheet, landSpriteSheetInterval);
            Intervals.Add(dashAttackSpriteSheet, dashAttackSpriteSheetInterval);
            Intervals.Add(jumpSpriteSheet, jumpSpriteSheetInterval);
            Intervals.Add(voadoraSpriteSheet, voadoraSpriteSheetInterval);
        }

        // Update is called once per frame
        void Update()
        {
            //UpdateCurrentSprite();
            UpdateSprite();
        }

        public void Play(AnimationType action, bool inverted)
        {
            isPlaying = false;
            this.facingRight = inverted;
            ChangeAction(action);
            isPlaying = true;

            SetCountersToOneShot();
        }

        public void PlayLoop(AnimationType action, bool inverted)
        {
            isPlaying = false;
            this.facingRight = inverted;
            ChangeAction(action);
            isPlaying = true;

            SetCountersToLoop();
        }

        public void PlaySequence(IList<AnimationType> actions, bool inverted)
        {
            isPlaying = false;

            currentAction = actions[0];
            this.facingRight = inverted;
            animationSequence = actions;

            isPlaying = true;
        }

        public void Stop()
        {
            isPlaying = false;
        }

        private void ChangeAction(AnimationType action)
        {
            currentAction = action;

            switch (action)
            {
                case AnimationType.Idle:
                    currentSpriteSheet = idleSpriteSheet;
                    break;

                case AnimationType.Walk:
                    currentSpriteSheet = walkSpriteSheet;
                    break;

                case AnimationType.Run:
                    currentSpriteSheet = runSpriteSheet;
                    break;

                case AnimationType.Attack1:
                    currentSpriteSheet = attack1SpriteSheet;
                    break;

                case AnimationType.Attack2:
                    currentSpriteSheet = attack2SpriteSheet;
                    break;

                case AnimationType.Attack3:
                    currentSpriteSheet = attack3SpriteSheet;
                    break;

                case AnimationType.Attack4:
                    currentSpriteSheet = attack4SpriteSheet;
                    break;

                case AnimationType.Attack5:
                    currentSpriteSheet = attack5SpriteSheet;
                    break;

                case AnimationType.ForceAttack:
                    currentSpriteSheet = forceAttackSpriteSheet;
                    break;

                case AnimationType.Skill1:
                    currentSpriteSheet = skill1SpriteSheet;
                    break;

                case AnimationType.Skill2:
                    currentSpriteSheet = skill2SpriteSheet;
                    break;

                case AnimationType.Skill3:
                    currentSpriteSheet = skill3SpriteSheet;
                    break;

                case AnimationType.Special1:
                    currentSpriteSheet = specialSpriteSheet1;
                    break;

                case AnimationType.Special2:
                    currentSpriteSheet = specialSpriteSheet2;
                    break;

                case AnimationType.Special3:
                    currentSpriteSheet = specialSpriteSheet3;
                    break;

                case AnimationType.Special4:
                    currentSpriteSheet = specialSpriteSheet4;
                    break;

                case AnimationType.Special5:
                    currentSpriteSheet = specialSpriteSheet5;
                    break;

                case AnimationType.Hit1:
                    currentSpriteSheet = hitSpriteSheet1;
                    break;

                case AnimationType.Hit2:
                    currentSpriteSheet = hitSpriteSheet2;
                    break;

                case AnimationType.Fall:
                    currentSpriteSheet = fallSpriteSheet;
                    break;

                case AnimationType.HitFloor:
                    currentSpriteSheet = hitFloorSpriteSheet;
                    break;

                case AnimationType.Land:
                    currentSpriteSheet = landSpriteSheet;
                    break;

                case AnimationType.DashAttack:
                    currentSpriteSheet = dashAttackSpriteSheet;
                    break;

                case AnimationType.Jump:
                    currentSpriteSheet = jumpSpriteSheet;
                    break;

                case AnimationType.Voadora:
                    currentSpriteSheet = voadoraSpriteSheet;
                    break;
            }
        }

        private void SetCountersToOneShot()
        {
            currentIndex = 0;
            isOneShot = true;
        }

        private void SetCountersToLoop()
        {
            currentIndex = 0;
            isOneShot = false;
        }

        private void UpdateSprite()
        {
            if (isPlaying)
            {
                if (Time.fixedTime - lastTime > Intervals[currentSpriteSheet])
                {
                    if (currentSpriteSheet.Length > 0)
                    {
                        lastTime = Time.fixedTime;                        

                        if (!facingRight)
                        {
                            transform.localScale = invertDirection;
                        }
                        else
                        {
                            transform.localScale = originalDirection;
                        }

                        spriteRenderer.sprite = currentSpriteSheet[currentIndex];

                        //Debug.Log("Action: " + currentAction + " - frame: " + currentIndex);

                        if ((currentIndex == currentSpriteSheet.Length - 1))
                        {
                            if (isSequence)
                            {
                                if (animationSequence.Count - 1 > sequenceIndex)
                                {
                                    sequenceIndex++;
                                    currentAction = animationSequence[sequenceIndex];
                                    currentIndex = 0;
                                }
                            }

                            if (isOneShot)
                            {
                                Stop();
                            }
                        }

                        currentIndex = (currentIndex + 1) % currentSpriteSheet.Length;
                    }
                }
            }
        }

        public void PlayIdle(bool inverted) { currentAction = AnimationType.Idle; this.facingRight = inverted; }
        public void PlayWalk(bool inverted) { currentAction = AnimationType.Walk; this.facingRight = inverted; }
        public void PlayRun(bool inverted) { currentAction = AnimationType.Run; this.facingRight = inverted; currentIndex = -1; isContinuos = true; }
        public void PlayAttack1(bool inverted) { currentAction = AnimationType.Attack1; this.facingRight = inverted; resetCounters(); }
        public void PlayAttack2(bool inverted) { currentAction = AnimationType.Attack2; this.facingRight = inverted; resetCounters(); }
        public void PlayAttack3(bool inverted) { currentAction = AnimationType.Attack3; this.facingRight = inverted; resetCounters(); }
        public void PlayAttack4(bool inverted) { currentAction = AnimationType.Attack4; this.facingRight = inverted; resetCounters(); }
        public void PlayAttack5(bool inverted) { currentAction = AnimationType.Attack5; this.facingRight = inverted; resetCounters(); }
        public void PlayForceAttack(bool inverted) { currentAction = AnimationType.ForceAttack; this.facingRight = inverted; resetCounters(); }
        public void PlaySkill1(bool inverted) { currentAction = AnimationType.Skill1; this.facingRight = inverted; resetCounters(); }
        public void PlaySkill2(bool inverted) { currentAction = AnimationType.Skill2; this.facingRight = inverted; resetCounters(); }
        public void PlaySkill3(bool inverted) { currentAction = AnimationType.Skill3; this.facingRight = inverted; resetCounters(); }
        public void PlaySpecial1(bool inverted) { currentAction = AnimationType.Special1; this.facingRight = inverted; resetCounters(); }
        public void PlaySpecial2(bool inverted) { currentAction = AnimationType.Special2; this.facingRight = inverted; }
        public void PlaySpecial3(bool inverted) { currentAction = AnimationType.Special3; this.facingRight = inverted; }
        public void PlaySpecial4(bool inverted) { currentAction = AnimationType.Special4; this.facingRight = inverted; }
        public void PlaySpecial5(bool inverted) { currentAction = AnimationType.Special5; this.facingRight = inverted; resetCounters(); }

        public void PlayHit1(bool inverted) { currentAction = AnimationType.Hit1; this.facingRight = inverted; resetCounters(); }
        public void PlayHit2(bool inverted) { currentAction = AnimationType.Hit2; this.facingRight = inverted; resetCounters(); }
        public void PlayFall(bool inverted) { currentAction = AnimationType.Fall; this.facingRight = inverted; currentIndex = -1; isContinuos = true; }
        public void PlayHitFloor(bool inverted) { currentAction = AnimationType.HitFloor; this.facingRight = inverted; resetCounters(); }
        public void PlayLand(bool inverted) { currentAction = AnimationType.Land; this.facingRight = inverted; resetCounters(); }
        public void PlayDashAttack(bool inverted) { currentAction = AnimationType.DashAttack; this.facingRight = inverted; resetCounters(); }

        public void PlayJump(bool inverted) { currentAction = AnimationType.Jump; this.facingRight = inverted; currentIndex = -1; isContinuos = true; }
        public void PlayVoadora(bool inverted) { currentAction = AnimationType.Voadora; this.facingRight = inverted; resetCounters(); }



        private void UpdateCurrentSprite()
        {
            switch (currentAction)
            {
                case AnimationType.Idle:
                    currentSpriteSheet = idleSpriteSheet;
                    break;

                case AnimationType.Walk:
                    currentSpriteSheet = walkSpriteSheet;
                    break;

                case AnimationType.Run:
                    currentSpriteSheet = runSpriteSheet;
                    break;

                case AnimationType.Attack1:
                    currentSpriteSheet = attack1SpriteSheet;
                    break;

                case AnimationType.Attack2:
                    currentSpriteSheet = attack2SpriteSheet;
                    break;

                case AnimationType.Attack3:
                    currentSpriteSheet = attack3SpriteSheet;
                    break;

                case AnimationType.Attack4:
                    currentSpriteSheet = attack4SpriteSheet;
                    break;

                case AnimationType.Attack5:
                    currentSpriteSheet = attack5SpriteSheet;
                    break;

                case AnimationType.ForceAttack:
                    currentSpriteSheet = forceAttackSpriteSheet;
                    break;

                case AnimationType.Skill1:
                    currentSpriteSheet = skill1SpriteSheet;
                    break;

                case AnimationType.Skill2:
                    currentSpriteSheet = skill2SpriteSheet;
                    break;

                case AnimationType.Skill3:
                    currentSpriteSheet = skill3SpriteSheet;
                    break;

                case AnimationType.Special1:
                    currentSpriteSheet = specialSpriteSheet1;
                    break;

                case AnimationType.Special2:
                    currentSpriteSheet = specialSpriteSheet2;
                    break;

                case AnimationType.Special3:
                    currentSpriteSheet = specialSpriteSheet3;
                    break;

                case AnimationType.Special4:
                    currentSpriteSheet = specialSpriteSheet4;
                    break;

                case AnimationType.Special5:
                    currentSpriteSheet = specialSpriteSheet5;
                    break;

                case AnimationType.Hit1:
                    currentSpriteSheet = hitSpriteSheet1;
                    break;

                case AnimationType.Hit2:
                    currentSpriteSheet = hitSpriteSheet2;
                    break;

                case AnimationType.Fall:
                    currentSpriteSheet = fallSpriteSheet;
                    break;

                case AnimationType.HitFloor:
                    currentSpriteSheet = hitFloorSpriteSheet;
                    break;

                case AnimationType.Land:
                    currentSpriteSheet = landSpriteSheet;
                    break;

                case AnimationType.DashAttack:
                    currentSpriteSheet = dashAttackSpriteSheet;
                    break;

                case AnimationType.Jump:
                    currentSpriteSheet = jumpSpriteSheet;
                    break;

                case AnimationType.Voadora:
                    currentSpriteSheet = voadoraSpriteSheet;
                    break;
            }

            //Debug.Log("Tempo : " + (Time.fixedTime - lastTime) + " Indice : " + currentIndex + " max:" + currentSpriteSheet.Length + " facingRight:" + facingRight);
            //Debug.Log(" facingRight:" + facingRight);
            Debug.Log("Animation: " + currentAction + " frame: " + currentIndex);

            if (!isPaused)
            {
                if (Time.fixedTime - lastTime > Intervals[currentSpriteSheet])
                {
                    if (currentSpriteSheet.Length > 0)
                    {
                        lastTime = Time.fixedTime;
                        currentIndex = (currentIndex + 1) % currentSpriteSheet.Length;

                        if (!facingRight)
                        {
                            transform.localScale = invertDirection;
                        }
                        else
                        {
                            transform.localScale = originalDirection;
                        }

                        spriteRenderer.sprite = currentSpriteSheet[currentIndex];
                        if ((currentIndex == currentSpriteSheet.Length - 1))
                        {
                            if (isOneShot)
                            {
                                isOneShot = false;
                                currentAction = AnimationType.Idle;
                                currentIndex = 0;
                            }

                            if (isContinuos)
                            {
                                currentIndex = currentIndex - 1;
                            }

                            if (isSequence)
                            {
                                if (animationSequence.Count - 1 > sequenceIndex)
                                {
                                    isOneShot = false;
                                    sequenceIndex++;
                                    currentAction = animationSequence[sequenceIndex];
                                    currentIndex = 0;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void resetCounters()
        {
            currentIndex = -1;
            lastTime = Time.fixedTime;
            isOneShot = true;
        }

        public void PauseAnimation(float delay)
        {
            StartCoroutine(DelayedResume(delay));
        }

        IEnumerator DelayedResume(float delay)
        {
            isPaused = true;

            yield return new WaitForSeconds(delay);

            isPaused = false;
        }        
    }

    public enum AnimationType
    {
        Idle,
        Walk,
        Run,
        Attack1,
        Attack2,
        Attack3,
        Attack4,
        Attack5,
        ForceAttack,
        DashAttack,
        Skill1,
        Skill2,
        Skill3,
        Special1,
        Special2,
        Special3,
        Special4,
        Special5,
        Hit1,
        Hit2,
        Fall,
        HitFloor,
        Land,
        Jump,
        Voadora
    }
}