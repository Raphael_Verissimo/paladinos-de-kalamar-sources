using UnityEngine;
using System.Collections;
using Actions;

namespace Skills
{
	public class SkillController : MonoBehaviour 
	{
		public void UseSkill(SkillType type, Characters.CharacterController caster)
		{
			//Debug.Log ("Usou: " + type);

			switch (type) 
			{
				case SkillType.JAB:					
					ActionManager.UseAction (ActionType.NORMAL_ATTACK, caster);
				break;

                case SkillType.DIRETO:                 
                    ActionManager.UseAction (ActionType.DIRECT_ATTACK, caster);
                break;

                case SkillType.PUNCH:                 
                    ActionManager.UseAction (ActionType.PUNCH_ATTACK, caster);
                    break;

                case SkillType.STRONG_PUNCH:                 
                    ActionManager.UseAction (ActionType.STRONG_PUNCH_ATTACK, caster);
                    break;

                case SkillType.CANELADA:                 
                    ActionManager.UseAction (ActionType.CANELADA_ATTACK, caster);
                break;

                case SkillType.KICK:                 
                    ActionManager.UseAction (ActionType.KICK_ATTACK, caster);
                    break;

                case SkillType.STRONG_KICK:                 
                    ActionManager.UseAction (ActionType.STRONG_KICK_ATTACK, caster);
                    break;

				case SkillType.FORCE_ATTACK:					
					ActionManager.UseAction (ActionType.FORCE_ATTACK, caster);
				break;

                case SkillType.DASH:                    
                    ActionManager.UseAction (ActionType.DASH, caster);
                break;

                case SkillType.RYU_DASH:                    
                    ActionManager.UseAction (ActionType.RYU_DASH, caster);
                break;

                case SkillType.DASH_ATTACK:                    
                    ActionManager.UseAction (ActionType.DASH_ATTACK, caster);
                break;

                case SkillType.RYU_DASH_ATTACK:                    
                    ActionManager.UseAction (ActionType.RYU_DASH_ATTACK, caster);
                break;

                case SkillType.JUMP:                    
                    ActionManager.UseAction (ActionType.JUMP, caster);
                break;

                case SkillType.VOADORA:                    
                    ActionManager.UseAction (ActionType.VOADORA, caster);
                break;

				case SkillType.HADOUKEN:					
					ActionManager.UseAction (ActionType.HADOUKEN, caster);
				break;

				case SkillType.SHORYUKEN:					
					ActionManager.UseAction (ActionType.SHORYUKEN, caster);
				break;

				case SkillType.ATTACK_DAS_CORUJA:					
					ActionManager.UseAction (ActionType.ATTACK_DAS_CORUJA, caster);
				break;

				case SkillType.SHINKU_HADOUKEN:					
					ActionManager.UseAction (ActionType.SHINKU_HADOUKEN, caster);
				break;

                case SkillType.KIBLAST:                    
                    ActionManager.UseAction (ActionType.KIBLAST, caster);
                break;

                case SkillType.COUNTER_TELEPORT:                    
                    ActionManager.UseAction (ActionType.COUNTER_TELEPORT, caster);
                break;

                case SkillType.TRIPLE_STRIKE:                    
                    ActionManager.UseAction (ActionType.TRIPLE_STRIKE, caster);
                break;

                case SkillType.KAME_HAME_HA:                    
                    ActionManager.UseAction (ActionType.KAME_HAME_HA, caster);
                break;
			}
		}

		public GameObject UseSkill(SkillType type, Characters.HeroController caster)
		{
            //Debug.Log ("Usou: " + type);
            GameObject returnedSkill = null;

            switch (type) 
			{
                case SkillType.JAB:
                returnedSkill = ActionManager.UseAction(ActionType.NORMAL_ATTACK, caster);
                break;

                case SkillType.DIRETO:
                returnedSkill = ActionManager.UseAction(ActionType.DIRECT_ATTACK, caster);
                break;

                case SkillType.PUNCH:
                returnedSkill = ActionManager.UseAction(ActionType.PUNCH_ATTACK, caster);
                break;

                case SkillType.STRONG_PUNCH:
                returnedSkill = ActionManager.UseAction(ActionType.STRONG_PUNCH_ATTACK, caster);
                break;

                case SkillType.CANELADA:
                returnedSkill = ActionManager.UseAction(ActionType.CANELADA_ATTACK, caster);
                break;

                case SkillType.KICK:
                returnedSkill = ActionManager.UseAction(ActionType.KICK_ATTACK, caster);
                break;

                case SkillType.STRONG_KICK:
                returnedSkill = ActionManager.UseAction(ActionType.STRONG_KICK_ATTACK, caster);
                break;

                case SkillType.FORCE_ATTACK:
                returnedSkill = ActionManager.UseAction(ActionType.FORCE_ATTACK, caster);
                break;

                case SkillType.DASH:
                returnedSkill = ActionManager.UseAction (ActionType.DASH, caster);
				break;

                case SkillType.JUMP:
                returnedSkill = ActionManager.UseAction(ActionType.JUMP, caster);
                break;

                case SkillType.DASH_ATTACK:
                returnedSkill = ActionManager.UseAction(ActionType.DASH_ATTACK, caster);
                break;

                case SkillType.VOADORA:
                returnedSkill = ActionManager.UseAction(ActionType.VOADORA, caster);
                break;

                case SkillType.HADOUKEN:
                returnedSkill = ActionManager.UseAction(ActionType.HADOUKEN, caster);
                break;

                case SkillType.SHORYUKEN:
                returnedSkill = ActionManager.UseAction(ActionType.SHORYUKEN, caster);
                break;

                case SkillType.ATTACK_DAS_CORUJA:
                returnedSkill = ActionManager.UseAction(ActionType.ATTACK_DAS_CORUJA, caster);
                break;

                case SkillType.SHINKU_HADOUKEN:
                returnedSkill = ActionManager.UseAction(ActionType.SHINKU_HADOUKEN, caster);
                break;

                case SkillType.KIBLAST:
                returnedSkill = ActionManager.UseAction(ActionType.KIBLAST, caster);
                break;

                case SkillType.COUNTER_TELEPORT:
                returnedSkill = ActionManager.UseAction(ActionType.COUNTER_TELEPORT, caster);
                break;

                case SkillType.TRIPLE_STRIKE:
                returnedSkill = ActionManager.UseAction(ActionType.TRIPLE_STRIKE, caster);
                break;

                case SkillType.KAME_HAME_HA:
                returnedSkill = ActionManager.UseAction(ActionType.KAME_HAME_HA, caster);
                break;
            }

            return returnedSkill;
        }
	}
}
