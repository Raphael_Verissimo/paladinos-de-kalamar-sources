using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Skills;
using Sprites;
using Actions;
using Commands;

namespace Characters
{
    public class NovoGokuController : HeroController
    {       
		void Start ()
		{
            this.ActorId = "Goku";

            MovimentSpeed = 1.5f;
			RunningSpeed = 2.5f;
            ComboIndex = 1;

            Body = GetComponent<Rigidbody> ();
            SpriteController = GetComponentInChildren<SpriteController>();

            Direction = Vector3.right;
            IsFacingRight = true;

            SetUpSkills();
            SetUpStateMachie();
        }

        void FixedUpdate()
        {
            //Debug.Log("Posi��o do Heroi: " + transform.position);

            //Debug.Log("Speed :" + this.body.velocity);  
            UpdateLock();
        }

        #region Goku Methods
        override internal void PlaySkill(CommandChainType command)
		{
            //Debug.Log("PlaySkill");

            // Caso esteja com seu cooldown pronto
            if (ReadySkills.Contains(CommandAllias[command]))
            {
                // TODO Fazer o controle de Lock e Unlock pelo Script de Skills
                ReadySkills.Remove(CommandAllias[command]);
                LockHero(LockTimes[CommandAllias[command]]);
                StartCoroutine(ResetCooldown(CommandAllias[command]));                

                switch (CommandAllias[command])
                {
                    // Moviment Skills
                    case SkillType.DASH:                        
                        PlayDash(command);
                    break;

                    case SkillType.JUMP:
                        PlayJump(command);
                    break;

                    // Attack Skills
                    case SkillType.JAB:
                    case SkillType.KICK:
                    case SkillType.PUNCH:
                    case SkillType.DIRETO:
                    case SkillType.STRONG_KICK:
                    case SkillType.STRONG_PUNCH:
                        PlayAttack(command);
                    break;				
					
                    case SkillType.KIBLAST:
                        PlayKiBlast(command);
                    break;

                    case SkillType.COUNTER_TELEPORT:
                        PlayTeleport(command);
                    break;

                    case SkillType.TRIPLE_STRIKE:
                        PlayTripleStrike(command);
                    break;

                    case SkillType.KAME_HAME_HA:
                        PlayKameHameHa(command);
                    break;
                }                
            }
		}
		
		override internal void MoveTo (Vector3 direction)
		{
            if (CanChangeState(CharacterState.WALKING))
            {
                // Set Current State 
                ChangeState(CharacterState.WALKING);

                Direction = direction;
                Velocity = Direction * MovimentSpeed;

                if (SpriteController)
                {
                    SpriteController.PlayLoop(AnimationType.Walk, IsFacingRight);
                }
            }			
        }	
		
		override internal void HoldAttack ()
        {
            //Debug.Log("HoldAttack Attack !!!");
            IsHoldingAttack = true;
        }

        override internal void ReleaseAttack ()
        {
            //Debug.Log("Release Attack !!!");
            IsHoldingAttack = false;

            if (IsChargingSpecial) 
            {
                ShotSpecial ();
            }
        }
        #endregion

        private void SetUpStateMachie()
        {
            // Estado Dispon�vel
            PossibleChanges.Add(CharacterState.WAITING_FOR_IDLE, new List<CharacterState>
            {
                CharacterState.IDLE
            });

            PossibleChanges.Add(CharacterState.IDLE, new List<CharacterState>
            {
                CharacterState.IDLE, CharacterState.WAITING_FOR_IDLE,
                CharacterState.WALKING, CharacterState.DASHING,
                CharacterState.JUMPING, CharacterState.ATTACKING,
                CharacterState.ACTING, CharacterState.HITED
            });
            // ==================================================================

            // Estados de Movimento Interativo
            PossibleChanges.Add(CharacterState.WALKING, new List<CharacterState>
            {
                CharacterState.IDLE, CharacterState.WAITING_FOR_IDLE,
                CharacterState.WALKING, CharacterState.DASHING,
                CharacterState.JUMPING, CharacterState.ATTACKING,
                CharacterState.ACTING, CharacterState.HITED
            });

            PossibleChanges.Add(CharacterState.DASHING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.ACTING,
                CharacterState.JUMPING,
                CharacterState.ATTACKING,
                CharacterState.DASHATTACK,
                CharacterState.WAITING_FOR_IDLE,
            });

            PossibleChanges.Add(CharacterState.JUMPING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.ACTING,
                CharacterState.DASHING,
                CharacterState.ATTACKING,
                CharacterState.JUMPATTACK,
                CharacterState.WAITING_FOR_IDLE,
            });
            // ==================================================================

            // Estado de A��o 
            PossibleChanges.Add(CharacterState.ACTING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.WAITING_FOR_IDLE
            });

            PossibleChanges.Add(CharacterState.ATTACKING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.ATTACKING,
                CharacterState.WAITING_FOR_IDLE
            });

            PossibleChanges.Add(CharacterState.DASHATTACK, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.WAITING_FOR_IDLE
            });

            PossibleChanges.Add(CharacterState.JUMPATTACK, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.WAITING_FOR_IDLE
            });
            // ==================================================================

            // Estado de Interrup��o (Pode ser Atinjido em todos os estados)
            PossibleChanges.Add(CharacterState.HITED, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.PUSHED,
                CharacterState.FALLING,
                CharacterState.HITING_FLOOR,
                CharacterState.WAITING_FOR_IDLE,
            });
            // ==================================================================

            // Estados de Movimenta��o Passiva
            PossibleChanges.Add(CharacterState.PUSHED, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.FALLING,
                CharacterState.HITING_FLOOR
            });

            PossibleChanges.Add(CharacterState.FALLING, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.HITING_FLOOR
            });

            PossibleChanges.Add(CharacterState.HITING_FLOOR, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.LAY_DOWN,
            });

            PossibleChanges.Add(CharacterState.LAY_DOWN, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.GETTING_UP
            });

            PossibleChanges.Add(CharacterState.GETTING_UP, new List<CharacterState>
            {
                CharacterState.HITED,
                CharacterState.WAITING_FOR_IDLE
            });
            // ====================================================================================================

            ChangeState(CharacterState.WAITING_FOR_IDLE);
        }

        private void SetUpSkills()
		{
            // ================== Skills de Movimento Interativo ================
            ReadySkills.Add(SkillType.DASH);
			LockTimes.Add (SkillType.DASH, 0f);    
			MaxCoolDowns.Add (SkillType.DASH, 2f); 

			CommandAllias.Add (CommandChainType.RUN_FORWARD, SkillType.DASH);
            CommandAllias.Add (CommandChainType.RUN_BACKWARD, SkillType.DASH);

            LockTimes.Add(SkillType.JUMP, 0.2f);
            MaxCoolDowns.Add(SkillType.JUMP, 1.5f);
            ReadySkills.Add(SkillType.JUMP);
            CommandAllias.Add(CommandChainType.JUMP, SkillType.JUMP);
            // ==================================================================

            // ======================== Skills de Attaque =======================
            AttackSequence.Add(SkillType.JAB);
            AttackSequence.Add(SkillType.PUNCH);
            AttackSequence.Add(SkillType.KICK);
            AttackSequence.Add(SkillType.STRONG_KICK);
            AttackSequence.Add(SkillType.STRONG_PUNCH);

            CommandAllias.Add(CommandChainType.NORMAL_ATTACK, SkillType.JAB);

            LockTimes.Add(SkillType.JAB, 0.35f);
            MaxCoolDowns.Add(SkillType.JAB, 0.35f);
            ReadySkills.Add(SkillType.JAB);

            LockTimes.Add(SkillType.PUNCH, 0.5f);
            MaxCoolDowns.Add(SkillType.PUNCH, 0.5f);
            ReadySkills.Add(SkillType.PUNCH);

            LockTimes.Add(SkillType.KICK, 0.55f);
            MaxCoolDowns.Add(SkillType.KICK, 0.55f);
            ReadySkills.Add(SkillType.KICK);

            LockTimes.Add(SkillType.STRONG_KICK, 0.7f);
            MaxCoolDowns.Add(SkillType.STRONG_KICK, 0.7f);
            ReadySkills.Add(SkillType.STRONG_KICK);

            LockTimes.Add(SkillType.STRONG_PUNCH, 1f);
            MaxCoolDowns.Add(SkillType.STRONG_PUNCH, 1f);
            ReadySkills.Add(SkillType.STRONG_PUNCH);

            LockTimes.Add(SkillType.DASH_ATTACK, 1f);
            MaxCoolDowns.Add(SkillType.DASH_ATTACK, 1f);
            ReadySkills.Add(SkillType.DASH_ATTACK);

            LockTimes.Add(SkillType.VOADORA, 1f);
            MaxCoolDowns.Add(SkillType.VOADORA, 1f);
            ReadySkills.Add(SkillType.VOADORA);
            // ==================================================================
			
			// ============================= Skills =========================================
            CommandAllias.Add (CommandChainType.DOWN_FORWARD_ATTACK, SkillType.KIBLAST);
            CommandDirections.Add (CommandChainType.DOWN_FORWARD_ATTACK, true);

            CommandAllias.Add (CommandChainType.DOWN_BACKWARD_ATTACK, SkillType.KIBLAST);  
            CommandDirections.Add (CommandChainType.DOWN_BACKWARD_ATTACK, false);

            LockTimes.Add (SkillType.KIBLAST, 0.1f);   
            MaxCoolDowns.Add (SkillType.KIBLAST, 1f);  
            ReadySkills.Add(SkillType.KIBLAST);

            CommandAllias.Add (CommandChainType.FORWARD_DOWN_FORWARD_ATTACK, SkillType.COUNTER_TELEPORT);
            CommandDirections.Add (CommandChainType.FORWARD_DOWN_FORWARD_ATTACK, true);

            CommandAllias.Add (CommandChainType.BACK_DOWN_BACKWARD_ATTACK, SkillType.COUNTER_TELEPORT);    
            CommandDirections.Add (CommandChainType.BACK_DOWN_BACKWARD_ATTACK, false);

            LockTimes.Add (SkillType.COUNTER_TELEPORT, 0.1f);  
            MaxCoolDowns.Add (SkillType.COUNTER_TELEPORT, 0.1f); 
            ReadySkills.Add(SkillType.COUNTER_TELEPORT);

            CommandAllias.Add (CommandChainType.FORWARD_DOWN_BACK_ATTACK, SkillType.TRIPLE_STRIKE);
            CommandDirections.Add (CommandChainType.FORWARD_DOWN_BACK_ATTACK, true);

            CommandAllias.Add (CommandChainType.BACK_DOWN_FORWARD_ATTACK, SkillType.TRIPLE_STRIKE); 
            CommandDirections.Add (CommandChainType.BACK_DOWN_FORWARD_ATTACK, false);

            LockTimes.Add (SkillType.TRIPLE_STRIKE, 2f);    
            MaxCoolDowns.Add (SkillType.TRIPLE_STRIKE, 5f); 
            ReadySkills.Add(SkillType.TRIPLE_STRIKE);

            CommandAllias.Add (CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK, SkillType.KAME_HAME_HA);
            CommandDirections.Add (CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK, true);

            CommandAllias.Add (CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK, SkillType.KAME_HAME_HA); 
            CommandDirections.Add (CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK, false);

            LockTimes.Add (SkillType.KAME_HAME_HA, 100f);    
            MaxCoolDowns.Add (SkillType.KAME_HAME_HA, 5f); 
            ReadySkills.Add(SkillType.KAME_HAME_HA);
			// ======================================================================================
        }

        private void PlayDash(CommandChainType command)
		{
            if (CanChangeState(CharacterState.DASHING))
            {            
                ChangeState(CharacterState.DASHING);

                // Atualiza Dire��o
                Direction = Vector3.right;
                if (command == CommandChainType.RUN_BACKWARD) 
				    Direction = Vector3.left;

                if (SpriteController)
                {
                    SpriteController.Play(AnimationType.Run, IsFacingRight);
                }
                SkillManager.UseSkill (CommandAllias[command], this);
            }
        }

        private void PlayJump(CommandChainType command)
        {
            if (CanChangeState(CharacterState.JUMPING))
            {
                ChangeState(CharacterState.JUMPING);

                if (SpriteController)
                {
                    SpriteController.Play(AnimationType.Jump, IsFacingRight);
                }
                SkillManager.UseSkill(CommandAllias[command], this);
            }
        }

        private void PlayAttack(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ATTACKING))
            {
                switch (State)
                {
                    case CharacterState.IDLE:
                    case CharacterState.WALKING:
                    case CharacterState.ATTACKING:
                        PlaySequence(command);
                    break;

                    case CharacterState.DASHING:
                        PlayDashAttack();
                    break;

                    case CharacterState.JUMPING:
                        PlayJumpAttack();
                    break;
                }

                ChangeState(CharacterState.ATTACKING);
            }
        }

        private void PlaySequence(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ATTACKING))
            {
                CommandAllias[CommandChainType.NORMAL_ATTACK] = AttackSequence[ComboIndex-1];

                switch(CommandAllias[CommandChainType.NORMAL_ATTACK])
                {
                    case SkillType.JAB:
                    SequenceJab();
                    break;

                    case SkillType.PUNCH:
                    SequencePunch();
                    break;

                    case SkillType.KICK:
                    SequenceKick();
                    break;

                    case SkillType.STRONG_PUNCH:
                    SequenceStrongPunch();
                    break;

                    case SkillType.STRONG_KICK:
                    SequenceStrongKick();
                    break;
                }

                StartCoroutine(ResetCooldown(CommandAllias[command]));
                LockHero(LockTimes[CommandAllias[command]]);

                ComboIndex--;
                ComboIndex = ((ComboIndex + 1) % AttackSequence.Count);
                ComboIndex++;
            }
        }

        private void PlayDashAttack()
        {
            if (CanChangeState(CharacterState.DASHATTACK))
            {
                ChangeState(CharacterState.DASHATTACK);

                GameObject skillGO = SkillManager.UseSkill(SkillType.DASH_ATTACK, this);
                BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

                // Defini��o do Strong Punch
                float width = 1.0f;
                float maxTime = 0.4f;
                float coolDown = 0.2f;
                float movimentCoolDown = 0f;

                Vector3 offset = new Vector3(0.4f, 0.2f, 0);
                Vector3 moviment = this.Body.velocity;
                Vector3 pushForce = new Vector3(4 * Direction.x, 2f, 0);

                attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

                if (SpriteController)
                    SpriteController.Play(AnimationType.DashAttack, IsFacingRight);
            }                
        }

        private void PlayJumpAttack()
        {
            if (CanChangeState(CharacterState.JUMPATTACK))
            {
                ChangeState(CharacterState.JUMPATTACK);

                GameObject skillGO = SkillManager.UseSkill(SkillType.VOADORA, this);
                BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

                // Defini��o do Strong Punch
                float width = 0.6f;
                float maxTime = 0.5f;
                float coolDown = 0.2f;
                float movimentCoolDown = 0f;

                Vector3 offset = new Vector3(0.4f, 0f, 0);
                Vector3 moviment = this.Body.velocity;
                Vector3 pushForce = new Vector3(4 * Direction.x, 2f, 0);

                attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

                if (SpriteController)
                    SpriteController.Play(AnimationType.Voadora, IsFacingRight);
            }
        }
		
		private void PlayKiBlast(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ACTING))
            {
                ChangeState(CharacterState.ACTING);

                GameObject skillGO = SkillManager.UseSkill(SkillType.KIBLAST, this);
                KiBlast kiBlast = (KiBlast)skillGO.GetComponent<KiBlast>();

                // Defini��o do Strong Punch
                int maxHits = 1;
                float width = 1f;
                float range = 10f;
                float speed = 6f;
                float hitSpeed = 2f;
                float coolDown = 0.2f;
                float hitCoolDown = 0.3f;

                Vector3 offset = new Vector3(transform.position.x + (Direction.x * 0.5f), 2f, -0.1f);
                Vector3 pushForce = new Vector3(1 * Direction.x, 2f, 0);

                kiBlast.UseAction(this, width, range, speed, hitSpeed, maxHits, coolDown, hitCoolDown, offset, pushForce);

                if (SpriteController)
                    SpriteController.Play(AnimationType.Skill1, IsFacingRight);
            }
        }

        private void PlayTeleport(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ACTING))
            {
                ChangeState(CharacterState.ACTING);

                GameObject skillGO = SkillManager.UseSkill(SkillType.COUNTER_TELEPORT, this);
                CounterTeleport counterTeleport = (CounterTeleport)skillGO.GetComponent<CounterTeleport>();

                // Defini��o do Strong Punch
                float coolDown = 0.5f;

                Vector3 offset = transform.position;
				offset += new Vector3(Direction.x * 4, 0f, 0f);

                counterTeleport.UseAction(this, coolDown, offset);

                if (SpriteController)
                    SpriteController.Play(AnimationType.Skill2, IsFacingRight);
            }
        }

        private void PlayTripleStrike(CommandChainType command)
        {
            if (CanChangeState(CharacterState.ACTING))
            {
                ChangeState(CharacterState.ACTING);

                GameObject skillGO = SkillManager.UseSkill(SkillType.TRIPLE_STRIKE, this);
                TripleStrike tripleStrike = (TripleStrike)skillGO.GetComponent<TripleStrike>();

                // Defini��o do Strong Punch
                int maxHits = 3;
                float width = 1f;
                float range = 3f;
                float speed = 2.5f;
                float hitSpeed = 2f;
                float coolDown = 0.3f;
                float hitCoolDown = 0.3f;

                Vector3 offset = new Vector3(0.1f, 0.2f, 0.1f);
                Vector3 moviment = new Vector3(3f * Direction.x, 2f, 0);
                Vector3 pushForce = new Vector3(1 * Direction.x, 2f, 0);

                tripleStrike.UseAction(this, width, range, speed, hitSpeed, maxHits, coolDown, hitCoolDown, offset, moviment, pushForce);

                if (SpriteController)
                    SpriteController.Play(AnimationType.Skill3, IsFacingRight);
            }
        }

        private void PlayKameHameHa(CommandChainType command)
        {
			if (CanChangeState(CharacterState.ACTING))
            {
                ChangeState(CharacterState.ACTING);

                GameObject skillGO = SkillManager.UseSkill(SkillType.KAME_HAME_HA, this);
                KameHameHa kameHameHa = (KameHameHa)skillGO.GetComponent<KameHameHa>();

                // Defini��o do Shinku Hadouken
                int maxHits = 3;
                float width = 1f;
                float range = 3f;
                float speed = 2.5f;
                float hitSpeed = 2f;
                float coolDown = 0.3f;
                float hitCoolDown = 0.3f;

                Vector3 offset = new Vector3(0.1f, 0.2f, 0.1f);
                Vector3 moviment = new Vector3(3f * Direction.x, 0f, 0);
                Vector3 pushForce = new Vector3(1 * Direction.x, 2f, 0);

                kameHameHa.UseAction(this, width, range, speed, hitSpeed, maxHits, coolDown, hitCoolDown, offset, pushForce);

				if (SpriteController)
				{
					//spriteController.PlaySpecial1(isFacingRight);
					IList<AnimationType> actions = new List<AnimationType>();

					actions.Add(AnimationType.Special1);
					actions.Add(AnimationType.Special2);

                    SpriteController.PlaySequence(actions, IsFacingRight);
					//spriteController.PlaySpecial1(isFacingRight);

					EffectsSources.clip = SpecialStartClip;
					EffectsSources.Play();

					StartCoroutine(DelayedVoice(ChargingSpecialVoice, 1.8f));
				} 
            }
        }

        private void SequenceJab()
        {
            GameObject skillGO = SkillManager.UseSkill(SkillType.JAB, this);
            BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

            // Defini��o do Jab
            float width = 0.4f;
            float maxTime = 0.2f;
            float coolDown = 0.1f;
            float movimentCoolDown = 0f;

            Vector3 offset = new Vector3(0.6f, 0.4f, 0);
            Vector3 moviment = new Vector3(4 * Direction.x, 1.2f, 0);
            Vector3 pushForce = new Vector3(2 * Direction.x, 2f, 0);

            attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

            if(SpriteController)
                SpriteController.Play(AnimationType.Attack1, IsFacingRight);
        }

        private void SequencePunch()
        {
            GameObject skillGO = SkillManager.UseSkill(SkillType.DIRETO, this);
            BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

            // Defini��o do Direto
            float width = 0.5f;
            float maxTime = 0.25f;
            float coolDown = 0.1f;
            float movimentCoolDown = 0f;

            Vector3 offset = new Vector3(0.7f, 0.3f, 0);
            Vector3 moviment = Vector3.zero;
            Vector3 pushForce = new Vector3(0.5f * Direction.x, 1f, 0);

            attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

            if (SpriteController)
                SpriteController.Play(AnimationType.Attack2, IsFacingRight);
        }

        private void SequenceKick()
        {
            GameObject skillGO = SkillManager.UseSkill(SkillType.KICK, this);
            BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

            // Defini��o do Kick
            float width = 0.7f;
            float maxTime = 0.3f;
            float coolDown = 0.15f;
            float movimentCoolDown = 0f;

            Vector3 offset = new Vector3(0.6f, 0.3f, 0);
            Vector3 moviment = new Vector3(0.8f * Direction.x, 2.5f, 0); ;
            Vector3 pushForce = new Vector3(2f * Direction.x, 0.5f, 0);

            attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

            if (SpriteController)
                SpriteController.Play(AnimationType.Attack4, IsFacingRight);
        }

        private void SequenceStrongPunch()
        {
            GameObject skillGO = SkillManager.UseSkill(SkillType.STRONG_PUNCH, this);
            BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

            // Defini��o do Strong Punch
            float width = 1.1f;
            float maxTime = 0.7f;
            float coolDown = 0.5f;
            float movimentCoolDown = 0f;

            Vector3 offset = new Vector3(0.4f, 0.2f, 0);
            Vector3 moviment = new Vector3(2f * Direction.x, 4.5f, 0); ;
            Vector3 pushForce = new Vector3(4 * Direction.x, 2f, 0);

            attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

            if (SpriteController)
                SpriteController.Play(AnimationType.Attack5, IsFacingRight);
        }

        private void SequenceStrongKick()
        {
            GameObject skillGO = SkillManager.UseSkill(SkillType.STRONG_KICK, this);
            BasicAttack attack = (BasicAttack)skillGO.GetComponent<BasicAttack>();

            // Defini��o do Strong Kick
            float width = 0.8f;
            float maxTime = 0.5f;
            float coolDown = 0.2f;
            float movimentCoolDown = 0f;

            Vector3 offset = new Vector3(0.4f, 0.4f, 0);
            Vector3 moviment = new Vector3(1.5f * Direction.x, 3f, 0); ;
            Vector3 pushForce = new Vector3(0f * Direction.x, 1f, 0);

            attack.UseAction(this, width, maxTime, coolDown, movimentCoolDown, offset, moviment, pushForce);

            if (SpriteController)
                SpriteController.Play(AnimationType.Attack3, IsFacingRight);
        }    

		private void ShotSpecial()
		{
            IsChargingSpecial = false;
            LockHero(0.3f);

			GetComponentInChildren<KameHameHa> ().OnShot ();

			if (SpriteController)
			{
                //spriteController.PlaySpecial1(isFacingRight);
                IList<AnimationType> actions = new List<AnimationType>();

                actions.Add(AnimationType.Special3);
                actions.Add(AnimationType.Idle);

                SpriteController.PlaySequence(actions, IsFacingRight);

                VoiceSources.clip = ShotSpecialVoice;
                VoiceSources.Play();
			} 
		}
		
        IEnumerator RecursiveChargeSpecial(float delay)
        {
            IsChargingSpecial = true;

            yield return new WaitForSeconds (delay);

            if (IsHoldingAttack)
            {
                if (SpriteController && IsChargingSpecial) 
                {
                    SpriteController.PlaySpecial2 (IsFacingRight);

                    GetComponentInChildren<ShinkuHadouken> ().OnCharge ();

                    if (IsChargingSpecial)
                        StartCoroutine (RecursiveChargeSpecial (0.9f));
                } 
            }
        }

        IEnumerator ResetCooldown(SkillType skill)
		{
			//Debug.Log ("SkillType:" + skill + " cooldown:" + MaxCoolDowns [skill]);
			yield return new WaitForSeconds (MaxCoolDowns[skill]);

			ReadySkills.Add(skill);
			//Debug.Log ("SkillType:" + skill + " Ready");
		}

        IEnumerator DelayedVoice(AudioClip clip, float delay)
        {
            yield return new WaitForSeconds(delay);

            VoiceSources.clip = clip;
            VoiceSources.Play();
        }
    }
}