﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;

namespace Actions
{	
	public class ActionController : MonoBehaviour 
	{

		// Use this for initialization
		void Start () 
		{
			
		}
		
		// Update is called once per frame
		void Update () 
		{
			
		}

		public GameObject UseAction(ActionType type, HeroController caster)
		{
			GameObject returnedAction = null;
			switch (type) 
			{
                case ActionType.NORMAL_ATTACK:
                case ActionType.PUNCH_ATTACK:
                case ActionType.STRONG_PUNCH_ATTACK:
                case ActionType.DIRECT_ATTACK:
                case ActionType.KICK_ATTACK:
                case ActionType.STRONG_KICK_ATTACK:
                case ActionType.CANELADA_ATTACK:
                case ActionType.FORCE_ATTACK:
                case ActionType.DASH_ATTACK:
                case ActionType.VOADORA:
                    returnedAction = ObjectManager.InstantiateObject("Actions/BasicAttack", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.DASH:
                    returnedAction = ObjectManager.InstantiateObject("Actions/Dash", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.RYU_DASH:
                    returnedAction = ObjectManager.InstantiateObject("Actions/RyuDash", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;                

                case ActionType.RYU_DASH_ATTACK:
                    returnedAction = ObjectManager.InstantiateObject("Actions/RyuDashAttack", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.JUMP:
                    returnedAction = ObjectManager.InstantiateObject("Actions/Jump", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;                

                case ActionType.HADOUKEN:
                    returnedAction = ObjectManager.InstantiateObject("Actions/Hadouken", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.KIBLAST:
                    returnedAction = ObjectManager.InstantiateObject("Actions/KiBlast", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.SHORYUKEN:
                    returnedAction = ObjectManager.InstantiateObject("Actions/Shoryuken", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.COUNTER_TELEPORT:
                    returnedAction = ObjectManager.InstantiateObject("Actions/CounterTeleport", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.ATTACK_DAS_CORUJA:
                    returnedAction = ObjectManager.InstantiateObject("Actions/AtaqueDasCoruja", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.TRIPLE_STRIKE:
                    returnedAction = ObjectManager.InstantiateObject("Actions/TripleStrike", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.SHINKU_HADOUKEN:
                    returnedAction = ObjectManager.InstantiateObject("Actions/ShinkuHadouken", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;

                case ActionType.KAME_HAME_HA:
                    returnedAction = ObjectManager.InstantiateObject("Actions/KameHameHa", caster.transform.position);
                    returnedAction.transform.parent = caster.transform;
                break;
            }

			return returnedAction;
		}

		public GameObject UseAction(ActionType type, Characters.CharacterController caster)
		{
			GameObject returnedAction = null;
			switch (type) 
			{
			case ActionType.NORMAL_ATTACK:					
				returnedAction = ObjectManager.InstantiateObject ("Actions/Jab", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.PUNCH_ATTACK:                  
				returnedAction = ObjectManager.InstantiateObject ("Actions/Punch", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.STRONG_PUNCH_ATTACK:                  
				returnedAction = ObjectManager.InstantiateObject ("Actions/StrongPunch", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.DIRECT_ATTACK:                  
				returnedAction = ObjectManager.InstantiateObject ("Actions/Direto", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.KICK_ATTACK:                  
				returnedAction = ObjectManager.InstantiateObject ("Actions/Kick", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.STRONG_KICK_ATTACK:                  
				returnedAction = ObjectManager.InstantiateObject ("Actions/StrongKick", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.CANELADA_ATTACK:                  
				returnedAction = ObjectManager.InstantiateObject ("Actions/Canelada", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.FORCE_ATTACK:					
				returnedAction = ObjectManager.InstantiateObject ("Actions/ForceAttack", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.DASH:                   
				returnedAction = ObjectManager.InstantiateObject ("Actions/Dash", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.RYU_DASH:                   
				returnedAction = ObjectManager.InstantiateObject ("Actions/RyuDash", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.DASH_ATTACK:                   
				returnedAction = ObjectManager.InstantiateObject ("Actions/DashAttack", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.RYU_DASH_ATTACK:                   
				returnedAction = ObjectManager.InstantiateObject ("Actions/RyuDashAttack", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.JUMP:                   
				returnedAction = ObjectManager.InstantiateObject ("Actions/Jump", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.VOADORA:                   
				returnedAction = ObjectManager.InstantiateObject ("Actions/Voadora", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.HADOUKEN:					
				returnedAction = ObjectManager.InstantiateObject ("Actions/Hadouken", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.KIBLAST:                   
				returnedAction = ObjectManager.InstantiateObject ("Actions/KiBlast", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.SHORYUKEN:					
				returnedAction = ObjectManager.InstantiateObject ("Actions/Shoryuken", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.COUNTER_TELEPORT:                  
				returnedAction = ObjectManager.InstantiateObject ("Actions/CounterTeleport", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.ATTACK_DAS_CORUJA:					
				returnedAction = ObjectManager.InstantiateObject ("Actions/AtaqueDasCoruja", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.TRIPLE_STRIKE:                  
				returnedAction = ObjectManager.InstantiateObject ("Actions/TripleStrike", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.SHINKU_HADOUKEN:					
				returnedAction = ObjectManager.InstantiateObject ("Actions/ShinkuHadouken", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;

			case ActionType.KAME_HAME_HA:                    
				returnedAction = ObjectManager.InstantiateObject ("Actions/KameHameHa", caster.transform.position);
				returnedAction.transform.parent = caster.transform;
				break;
			}

			return null;
		}
	}
}