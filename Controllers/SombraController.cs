﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SombraController : MonoBehaviour {

	public GameObject character;

	public float originalHeight;
	public float HeightOffSet;

	// Use this for initialization
	void Start () {
		transform.localPosition = Vector3.zero;
		//originalHeight = this.transform.position.y;
		//HeightOffSet = character.transform.position.y - originalHeight;
	}

	// Update is called once per frame
	void Update () {

		float height = character.transform.position.y - originalHeight;
		//Debug.Log ("height" + height + " sacleFactor" + (1 - height/40) +" localScale" + transform.localScale);

		if (height > 0.01f)
			transform.localScale = new Vector3 ((1 - height/2), 1, (1 - height/2));
		
		transform.position = new Vector3(transform.position.x, originalHeight, transform.position.z);
	}
}
