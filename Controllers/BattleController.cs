﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Commands;
using Characters;

namespace Battle
{
    public class BattleController : MonoBehaviour
    {
        private Dictionary<string, CommandChainType> battleActionOrder = new Dictionary<string, CommandChainType>();
        private Dictionary<string, HeroController> actorList = new Dictionary<string, HeroController>();

        private string nextActor = "";
        private string nextActionKey = "";
        private float nextActionTime = float.MaxValue;

        // Use this for initialization
        void Start()
        {
            GeneretaFakeScript();
        }

        // Update is called once per frame
        void Update()
        {
            //Caso existam comandos na Lista de Execução
            if (battleActionOrder.Count > 0)
            {
                if (nextActionKey.Equals(""))
                    UpdateNextAction();

                //Debug.Log(".... " + GameManager.GameTime + " Proxima Ação: " + nextActionTime);

                if (GameManager.GameTime >= nextActionTime)
                    PlayNextBattleAction();
            }
        }

        #region Public API
        public void AddCommand(float time, HeroController hero, CommandChainType command)
        {
            if (battleActionOrder == null)
                battleActionOrder = new Dictionary<string, CommandChainType>();

            if (actorList == null)
                actorList = new Dictionary<string, HeroController>();

            string battleActionKey = (time).ToString() + "|" + hero.ActorId;
            battleActionOrder.Add(battleActionKey, command);

            if (!actorList.ContainsKey(hero.ActorId))
                actorList.Add(hero.ActorId, hero);

            ////////////////// DEBUG !!! ///////////////////
            //Debug.Log("Novo comando Adicionado: " + command);
            //Debug.Log("Total de Comandos: " + battleActionOrder.Count);

            foreach (string key in battleActionOrder.Keys)
            {
                //Debug.Log("Key: " + key + " comando: " + battleActionOrder[key]);
            }
            ////////////////////////////////////////////////
        }

        public void UpdateNextAction()
        {
            nextActionKey = "";
            nextActionTime = float.MaxValue;

            foreach (string key in battleActionOrder.Keys)
            {
                float currentTime = float.Parse((key.Split('|')[0]));

                if (currentTime <= nextActionTime)
                {
                    nextActionKey = key;
                    nextActionTime = currentTime;
                    nextActor = key.Split('|')[1];
                }
            }

            //Debug.Log("Proximo Heroi: " + nextActionKey + " Comando:" + battleActionOrder[nextActionKey]);
        }
        #endregion


        #region Interanl API
        private void PlayNextBattleAction()
        {
            if (actorList.ContainsKey(nextActor))
            {
                //Debug.Log("Total de Comandos: " + battleActionOrder.Count);

                HeroController hero = actorList[nextActor];
                CommandChainType command = battleActionOrder[nextActionKey];
                //Debug.Log(nextActionKey + " Executa: " + battleActionOrder[nextActionKey]);
				
				hero.ApplyCommand(command);

                string removedKey = "";
                foreach (string key in battleActionOrder.Keys)
                {
                    if (key == nextActionKey)
                    {
                        //Debug.Log("Removendo Key: " + key + " comando: " + battleActionOrder[key]);
                        removedKey = key; 
                    }
                }
                battleActionOrder.Remove(removedKey);
                nextActionKey = "";

                //Debug.Log("Total de Comandos: " + battleActionOrder.Count);                
                ////////////////////////////////////////////////
                //GameManager.PlayCommand(hero, command);
            }
        }
		
		private void GeneretaFakeScript()
		{
			HeroController hero = GameManager.MainPlayer; 
			
			AddCommand(GameManager.GameTime, hero, CommandChainType.NORMAL_ATTACK);
			
			//AddCommand(GameManager.GameTime + 2f, hero, CommandChainType.NORMAL_ATTACK);
		}
        #endregion
    }
}
