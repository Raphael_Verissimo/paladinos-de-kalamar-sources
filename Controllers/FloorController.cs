﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Characters;

public class FloorController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.tag.Equals("Player") || other.gameObject.tag.Equals("MainPlayer"))
        {
			/*
            HeroController controller = CharacterManager.GetHeroController(other.gameObject);

            if (controller != null)
                controller.LandCharacter();
			*/

			HeroController hero = CharacterManager.GetHeroController(other.gameObject);

            if (hero != null)
                hero.ReachFloor();
        }
    }

	void OnTriggerStay(Collider other)
	{
		if(other.gameObject.tag.Equals("Player") || other.gameObject.tag.Equals("MainPlayer"))
		{
			/*
			HeroController controller = CharacterManager.GetHeroController(other.gameObject);

            if (controller != null)
			    controller.LandCharacter ();
			*/

			HeroController hero = CharacterManager.GetHeroController(other.gameObject);

            if (hero != null)
                hero.ReachFloor();
        }
	}
}
