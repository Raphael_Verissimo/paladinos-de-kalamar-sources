using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Skills;
using Actions;
using Commands;
using Sprites;
using Effects;

namespace Characters
{
	public class HeroController : MonoBehaviour
	{
        #region Moviment Variables
        private bool isJumping;
        private bool isFacingRight;
        private bool isHoldingAttack;
        private bool isChargingSpecial;

        private bool isLocked;
        private float lockTimer;
        private float lockedTime;

        private bool isStoped;
        private float stopTimer;
        private float stopedTime;

        private bool isReleasedOnFlor;
        private float releaseOnFlorTimer;
        private float releasedOnFlorTime;

        private bool isChangingPosition;
        private float changePositionTimer;
        private float changedPositionTime;
        private Vector3 changedPosition;

        private bool isActionMoviment;
        private float actionMovimentimer;
        private float actionMovimentedTime;
        private Vector3 actionMoviment;

        private bool isResumedSpeed;
        private float resumeSpeedTimer;
        private float resumedSpeedTime;
        private Vector3 resumedSpeed;
		
        private float velocity;
		private Vector3 direction;

        private float runningSpeedFactor;
        private float movimentSpeedFactor;

        private IDictionary<SkillType, float> lockTimes = new Dictionary<SkillType, float> ();
        private IDictionary<SkillType, float> maxCoolDowns = new Dictionary<SkillType, float> ();
        #endregion

        #region Command Variables
        private int comboIndex;
        private bool startCombo;
        private float comboTimmer;
        private float comboCooldown;

        private IList<SkillType> readySkills =  new List<SkillType>();
        private IList<SkillType> attackSequence = new List<SkillType>();

        private IDictionary<CommandChainType, SkillType> commandAllias = new Dictionary<CommandChainType, SkillType> ();
        private IDictionary<CommandChainType, bool> commandDirections = new Dictionary<CommandChainType, bool>();
        #endregion

        private Rigidbody body;

        #region State Machine
        private string actorId;

        private CharacterState currentState;
        private CharacterState lastState;
        private Dictionary<CharacterState, List<CharacterState>> possibleChanges = new Dictionary<CharacterState, List<CharacterState>>();
        #endregion

        #region Animation Variables
        private SpriteController spriteController;
        #endregion

        #region Effects Variables
        private AudioSource effectsSources;
        private AudioSource voiceSources;

        public AudioClip SpecialStartClip;
        public AudioClip ChargingSpecialClip;
        public AudioClip ShotSpecialClip;

        public AudioClip SpecialStartVoice;
        public AudioClip ChargingSpecialVoice;
        public AudioClip ShotSpecialVoice;
        #endregion

        #region Unity Events
        void Start()
        {
            body = GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            
        }
        #endregion

        #region Public Properties
        internal string ActorId { get { return actorId; } set { actorId = value; }  }
        internal CharacterState State { get { return currentState; } }
        internal bool IsLocked { get { return isLocked; } set { isLocked = value; } }
        internal bool IsHoldingAttack { get { return isHoldingAttack; } set { isHoldingAttack = value; } }
        internal bool IsChargingSpecial { get { return isChargingSpecial; } set { isChargingSpecial = value; } }

        internal float LockTimer { get { return lockTimer; } set { lockTimer = value; } }
        internal float LockedTime { get { return lockedTime; } set { lockedTime = value; } }

        internal int ComboIndex { get { return comboIndex; } set { comboIndex = value; } }
        internal bool StartCombo { get { return startCombo; } set { startCombo = value; } }
        internal float ComboTimmer { get { return comboTimmer; } set { comboTimmer = value; } }
        internal float ComboCooldown { get { return comboCooldown; } set { comboCooldown = value; } }

        internal Rigidbody Body { get { return body; } set { body = value; } }
        internal SpriteController SpriteController { get { return spriteController; } set { spriteController = value; } }
        internal AudioSource EffectsSources { get { return effectsSources; } set { effectsSources = value; } }
        internal AudioSource VoiceSources { get { return voiceSources; } set { voiceSources = value; } }

        internal float RunningSpeed { get { return runningSpeedFactor; } set { runningSpeedFactor = value; } }
        internal float MovimentSpeed { get { return movimentSpeedFactor; } set { movimentSpeedFactor = value; } }

        internal IList<SkillType> ReadySkills { get { return readySkills; } set { readySkills = value; } }
        internal IList<SkillType> AttackSequence { get { return attackSequence; } set { attackSequence = value; } }

        internal IDictionary<SkillType, float> LockTimes { get { return lockTimes; } set { lockTimes = value; } }
        internal IDictionary<SkillType, float> MaxCoolDowns { get { return maxCoolDowns; } set { maxCoolDowns = value; } }
        internal IDictionary<CommandChainType, SkillType> CommandAllias { get { return commandAllias; } set { commandAllias = value; } }
        internal IDictionary<CommandChainType, bool> CommandDirections { get { return commandDirections; } set { commandDirections = value; } }
        internal Dictionary<CharacterState, List<CharacterState>> PossibleChanges { get { return possibleChanges; } set { possibleChanges = value; } }

        internal bool IsFacingRight
        {
            get
            {
                if (Direction.x > 0)
                    isFacingRight = true;

                if (Direction.x < 0)
                    isFacingRight = false;

                return isFacingRight;
            }

            set
            {
                isFacingRight = value;
            }
        }

        internal Vector3 Direction
        {
            get
            {
                return direction;
            }

            set
            {
                direction = value;
            }
        }

        internal Vector3 Velocity
        {
            get
            {
                return body.velocity;
            }

            set
            {
                body.velocity = value;
            }
        }
        #endregion

        #region Public API
        internal void ApplyCommand (CommandChainType command)
		{				
			//Debug.Log("Apply Command: " + command);
			if (!isLocked) {
				switch (command) {
				case CommandChainType.MOVE_UP:
					MoveTo (new Vector3 (0, 0, 1));
					break;
				case CommandChainType.MOVE_UP_FORWARD:
					MoveTo (new Vector3 (1, 0, 1));
					break;
				case CommandChainType.MOVE_FORWARD:
					MoveTo (new Vector3 (1, 0, 0));
					break;
				case CommandChainType.MOVE_FORWARD_DOWN:
					MoveTo (new Vector3 (1, 0, -1));
					break;
				case CommandChainType.MOVE_DOWN:
					MoveTo (new Vector3 (0, 0, -1));
					break;
				case CommandChainType.MOVE_DOWN_BACKWARD:
					MoveTo (new Vector3 (-1, 0, -1));
					break;
				case CommandChainType.MOVE_BACKWARD:
					MoveTo (new Vector3 (-1, 0, 0));
					break;
				case CommandChainType.MOVE_BACKWARD_UP:
					MoveTo (new Vector3 (-1, 0, 1));
					break;

                case CommandChainType.HOLD_ATTACK:
					HoldAttack();
				break;

                case CommandChainType.RELEASE_ATTACK:
                    Release();
                break;

                case CommandChainType.STOP:
                    Stop();
                break;

                default: 
				if (commandAllias.ContainsKey (command)) {												
					if (readySkills.Contains (commandAllias [command])) {
						PlaySkill (command);  
					}							
				}
				break;
				}
			}
		}

        // Para os movimentos e retorna a Idle 
        internal void Release()
        {
            if (CanChangeState(CharacterState.IDLE))
            {
                ChangeState(CharacterState.IDLE);                

                if (spriteController)
                {
                    spriteController.PlayLoop(AnimationType.Idle, IsFacingRight);
                }
            }
        }

        // Para os movimentos e retorna a Idle 
        internal void Stop ()
		{
            if (CanChangeState(CharacterState.IDLE))
            {
                ChangeState(CharacterState.IDLE);

                body.velocity = Vector3.zero;

                if (spriteController)
                {
                    spriteController.PlayLoop(AnimationType.Idle, IsFacingRight);
                }
            }
		}

        internal void HitFloor()
        {
            if (CanChangeState(CharacterState.HITING_FLOOR))
            {
                ChangeState(CharacterState.HITING_FLOOR);

                if (spriteController)
                {
                    //spriteController.Play(AnimationType.Jump, IsFacingRight);
                }

                Stop(1f);
            }
        }

        internal void ReleaseHeroOnFloor()
        {
            switch (currentState)
            {                
                case CharacterState.ACTING:
                case CharacterState.DASHING:
                case CharacterState.ATTACKING:
                case CharacterState.DASHATTACK:
                case CharacterState.WAITING_FOR_IDLE:
                ChangeState(CharacterState.WAITING_FOR_IDLE);
                    Stop();
                break;
            }
        }

        internal void ReleaseHeroOnAir()
        {
            switch (currentState)
            {
                case CharacterState.ACTING:
                case CharacterState.DASHING:
                case CharacterState.ATTACKING:
                case CharacterState.DASHATTACK:
                case CharacterState.JUMPATTACK:
                case CharacterState.WAITING_FOR_IDLE:
                ChangeState(CharacterState.WAITING_FOR_IDLE);
                break;
            }
        }

        // Recoloca o personagem no chão, difernte de hit floor ele imediatamente retorna ao idle.  
        internal void ReachFloor()
        {
            switch (currentState)
            {
                case CharacterState.ACTING:
                case CharacterState.JUMPING:
                case CharacterState.DASHING:
                case CharacterState.ATTACKING:
                case CharacterState.DASHATTACK:
                case CharacterState.JUMPATTACK:
                case CharacterState.WAITING_FOR_IDLE:
                ChangeState(CharacterState.WAITING_FOR_IDLE);
                    Stop();
                break;

                case CharacterState.FALLING:
                case CharacterState.PUSHED:
                    HitFloor();
                break;
            }
        }
		
		internal void ReleaseOnFloor(float delay)
		{
			ReleaseOnFlor(delay);
		}

        internal void ResumeLastMoviment()
        {
            CommandType currentCommand = CommandManager.GetCurrentCommand;
            isLocked = false;

            switch (currentCommand)
            {                
                case CommandType.HOLD_KEY_UP:
                    ApplyCommand(CommandChainType.MOVE_UP);
                break;

                case CommandType.HOLD_KEY_UP_FORWARD:
                    ApplyCommand(CommandChainType.MOVE_UP_FORWARD);
                break;

                case CommandType.HOLD_KEY_FORWARD:
                    ApplyCommand(CommandChainType.MOVE_FORWARD);
                break;

                case CommandType.HOLD_KEY_FORWARD_DOWN:
                    ApplyCommand(CommandChainType.MOVE_FORWARD_DOWN);
                break;

                case CommandType.HOLD_KEY_DOWN:
                    ApplyCommand(CommandChainType.MOVE_DOWN);
                break;

                case CommandType.HOLD_KEY_DOWN_BACKWARD:
                    ApplyCommand(CommandChainType.MOVE_DOWN_BACKWARD);
                break;

                case CommandType.HOLD_KEY_BACKWARD:
                    ApplyCommand(CommandChainType.MOVE_BACKWARD);
                break;

                case CommandType.HOLD_KEY_BACKWARD_UP:
                    ApplyCommand(CommandChainType.MOVE_BACKWARD_UP);
                break;
            }
        }
		
        internal bool CanChangeState(CharacterState nextState)
        {
            if (possibleChanges[currentState].Contains(nextState))
            {
                //Debug.Log("Current State: " + nextState);
                return true;
            }
                
            return false;
        }

        internal void ChangeState(CharacterState nextState)
        {
            if (CanChangeState(nextState))
            {
                lastState = currentState;
                currentState = nextState;

                //if (nextState != CharacterState.ATTACKING)
                //   ComboIndex = 1;
            }
        }

        internal void LockHero(float lockTime)
        {
            IsLocked = true;
            LockTimer = lockTime;
            LockedTime = Time.fixedTime;
        }

        internal void UpdateLock()
        {
            if (IsLocked)
            {
                //Debug.Log("Locked At: " + LockedTime + " Locked Time: " + (Time.fixedTime - LockedTime));

                if ((Time.fixedTime - LockedTime) > LockTimer)
                    UnlockHero();
            }
        }

        // Acerta o chão, tocando a animação enqt espera para voltar a idle; 
        internal void HitFloor(float delay)
        {
            /*
            currentState = CharacterState.HITING_FLOOR;

            spriteController.PlayHitFloor(isFacingRight);

            isMoving = false;
            isRunning = false;

            StartCoroutine(DelayedIdle(delay));
            */
        }

        // Empurra o personagem na direção
        internal void Push(Vector3 pushForce)
        {
            /*
            if (!isPushing)
            {
                isPushing = true;
                body.useGravity = true;
                body.velocity = Vector3.zero;
                body.AddForce(pushForce, ForceMode.Impulse);
            }
            */
        }

        // Recoloca o personagem no chão, difernte de hit floor ele imediatamente retorna ao idle.  
        internal void Land()
        {
            /*
            if (isPushing || isMovingAction)
            {
                ResetSequence();
                DissmissAction();

                isPushing = false;
                isMovingAction = false;
                body.useGravity = true;

                if (isFalling)
                {
                    isFalling = false;
                    HitFloor(0.8f);
                }
                else
                {
                    Idle();
                }
            }

            if (isJumping)
            {
                ResumeLastMoviment();
                isJumping = false;
            }
            */
        }

        // TODO REVER 
        internal void HitCharacter(Vector3 pushForce, float delay)
        {
            //UnityEditor.EditorApplication.isPaused = true;

            //DissmissAction();

            /*
            isPushing = false;
            isHoldingAttack = false;
            isChargingSpecial = false;

            if (pushForce.x > -0.5f && pushForce.x < 0.5f)
            {
                currentState = CharacterState.HITED;

                if (pushForce.x > 0)
                    spriteController.PlayHit1(false);
                else
                    spriteController.PlayHit1(true);
            }
            else
            {
                if (pushForce.x > -1f && pushForce.x < 1f)
                {
                    currentState = CharacterState.PUSHED;

                    if (pushForce.x > 0)
                        spriteController.PlayHit2(false);
                    else
                        spriteController.PlayHit2(true);
                }
                else
                {
                    currentState = CharacterState.FALLING;

                    isFalling = true;
                    isLocked = true;
                    isHited = true;

                    if (pushForce.x > 0)
                        spriteController.PlayFall(false);
                    else
                        spriteController.PlayFall(true);
                }
            }

            Push(pushForce);
            */
        }

        internal void PushCharacter(Vector3 pushForce, float delay)
        {
            HitCharacter(pushForce, delay);
        }

        // Interrompe o Movimento e Animação por um periodo de tempo.
        internal void ActionHitDelayedResume(float delay)
        {
            ResumeSpeed(delay);
            spriteController.PauseAnimation(delay);
        }

        internal void ActionMoviment(Vector3 actionDirection, float delay)
        {
            isActionMoviment = true;

            actionMovimentimer = delay;
            actionMovimentedTime = Time.fixedTime;

            actionMoviment = actionDirection;
        }

        internal void ChangePosition(Vector3 position, float delay)
        {
            isChangingPosition = true;

            changedPosition = position;
            changePositionTimer = delay;
            changedPositionTime = Time.fixedTime;
        }

        internal void ReleaseOnFlor(float delay)
        {
            releaseOnFlorTimer = delay;
            releasedOnFlorTime = Time.fixedTime;
        }

        private void ResumeSpeed(float delay)
        {
            resumeSpeedTimer = delay;
            resumedSpeedTime = Time.fixedTime;

            resumedSpeed = body.velocity;
            body.isKinematic = true;
            body.velocity = Vector3.zero;
        }

        internal void UpdateTimers()
        {
            if (isStoped)
                UpdateStop();

            if (isActionMoviment)
                UpdateActionMoviment();

            if (isChangingPosition)
                UpdateChangePosition();

            if (isReleasedOnFlor)
                UpdateReleaseOnFlor();

            if (isResumedSpeed)
                UpdateResumeSpeed();
        }
        #endregion

        #region Virtual Methods
        virtual internal void MoveTo (Vector3 direction) { }
        virtual internal void HoldAttack() { }
        virtual internal void ReleaseAttack() { }

        virtual internal void PlaySkill(CommandChainType command) { }
        #endregion

        #region Internal Methods
        private void UnlockHero()
        {
            IsLocked = false;
            LockTimer = 0;
        }
		
		
		private void UpdateReleaseOnFlor()
		{
			if ((Time.fixedTime - releasedOnFlorTime) > releaseOnFlorTimer)
			{
				releaseOnFlorTimer = 0;
				releasedOnFlorTime = 0;	
				ReleaseHeroOnFloor();
			}

            isReleasedOnFlor = false;
		}		
		
		private void UpdateChangePosition()
		{
			if ((Time.fixedTime - changedPositionTime) > changePositionTimer)
			{
				this.transform.position = changedPosition;
				changePositionTimer = 0;
				changedPositionTime = 0;
                changedPosition = Vector3.zero;
			}

            isChangingPosition = true;

        }       
		
		private void UpdateResumeSpeed()
		{
			if ((Time.fixedTime - resumedSpeedTime) > resumeSpeedTimer)
            {				
				body.isKinematic = false;
				body.AddRelativeForce(resumedSpeed, ForceMode.Impulse);
				
				resumeSpeedTimer = 0;
				resumedSpeedTime = 0;
                resumedSpeed = Vector3.zero;			
			}

            isResumedSpeed = false;
		}        
		
		private void UpdateActionMoviment()
		{
			if ((Time.fixedTime - actionMovimentedTime) > actionMovimentimer)
			{
                //body.velocity = Direction;
                body.AddForce(actionMoviment, ForceMode.Impulse);

                actionMovimentimer = 0;
				actionMovimentedTime = 0;
                actionMoviment = Vector3.zero;	
			}

            isActionMoviment = false;
        }

		private void Stop(float delay)
		{
            isStoped = true;
			stopTimer = delay;
			stopedTime = Time.fixedTime;
		}    

		private void UpdateStop()
		{
			if ((Time.fixedTime - stopedTime) > stopTimer)
			{
				Stop();
				
				stopTimer = 0;
				stopedTime = 0;
			}

            isStoped = false;
        }		
        #endregion
    }
}