using UnityEngine;
using System.Collections;

namespace Effects
{
	public class EffectsController : MonoBehaviour 
	{
        public void PlayEffect(EffectType effect, Vector3 position)
		{
			//Debug.Log ("Exibindo Effeito : " + effect);

			switch (effect) 
			{
                case EffectType.HIT:					
                    IntantiateEffect("Effects/Hit1", position);
				break;

                case EffectType.WIN_PANEL:                  
                    IntantiateEffect("Effects/WinPanel", position);
                break;

                case EffectType.LOSE_PANEL:                  
                    IntantiateEffect("Effects/LosePanel", position);
                break;

                case EffectType.EXPLOSION:					
                    IntantiateEffect("Effects/Hit1", position);
				break;
			}
		}

        private void IntantiateEffect(string effectName, Vector3 position)
        {
            GameObject prefab = ObjectManager.InstantiateObject (effectName, position);
        }
	}	

	public enum EffectType
	{
		NONE,
		HIT,
		EXPLOSION,
		SLOW,
		PARALYSE,
		CURSE,
		BURN,
		STUN,

        WIN_PANEL,
        LOSE_PANEL
	}

	public enum DamageType
	{
		PHYSICAL,
		ICE,  
		FIRE, 
		EARTH,
		LIGHTINING,
		POISON,
		ENERGY
	}
}
