using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Commands
{
	public class CommandController : MonoBehaviour 
	{
		#region Atributos Internos
		private float maxDelay;		
		private float currentDelay;

		private float lastCommandTime;
		private CommandType currentCommand;
		private CommandType currentFullCommand;

		private string fullCommandChainString;
		private string currentCommandChainString;
		private CommandChainType currentCommandChain;
		private List<CommandType> inacceptablesAttackCommands = new List<CommandType>();

		//private List<CommandType> commandList = new List<CommandType>();
		private IDictionary	<KeyCode, CommandType> commandAlias = new Dictionary<KeyCode, CommandType>();	

		private bool isHoldingKey;

		public List<CharacterCommand> twoKeyCommands = new List<CharacterCommand>();
		public List<CharacterCommand> threeKeyCommands = new List<CharacterCommand>();
		public List<CharacterCommand> fourKeyCommands = new List<CharacterCommand>();
		public List<CharacterCommand> fiveKeyCommands = new List<CharacterCommand>();

		private int maxKeys;
        private IDictionary	<string, CommandChainType> commandStringsAlias = new Dictionary<string, CommandChainType>();	
        private IDictionary <string, CommandChainType> attackCommandStringsAlias = new Dictionary<string, CommandChainType>();    
		#endregion
		
		#region Propriedades
		public CommandType CurrentCommand {get {return currentCommand;} set {currentCommand = value;}}
		public CommandChainType CurrentCommandChain {get {return currentCommandChain;} set {currentCommandChain = value;}}
		#endregion

		void Start ()
		{
			maxKeys = 7;
			maxDelay = 2.5f;
			currentDelay = maxDelay;
			lastCommandTime = 0;

			//commandList.Clear();

			CurrentCommandChain = CommandChainType.NONE;
			fullCommandChainString = "";
			currentCommandChainString = "";

			inacceptablesAttackCommands.Add (CommandType.NONE);
			inacceptablesAttackCommands.Add (CommandType.STOP);
			inacceptablesAttackCommands.Add (CommandType.KEY_ATTACK);
			inacceptablesAttackCommands.Add (CommandType.HOLD_KEY_ATTACK);
			inacceptablesAttackCommands.Add (CommandType.RELEASE_KEY_ATTACK);
			inacceptablesAttackCommands.Add (CommandType.KEY_UP_FORWARD);
			inacceptablesAttackCommands.Add (CommandType.KEY_BACKWARD_UP);
			inacceptablesAttackCommands.Add (CommandType.KEY_FORWARD_DOWN);
			inacceptablesAttackCommands.Add (CommandType.KEY_DOWN_BACKWARD);

			// Preenche todos o dicionario de command chain string
            foreach (MovimentCommandType command in Enum.GetValues(typeof(MovimentCommandType))) 
			{
				if (!commandStringsAlias.ContainsKey (command.ToCommandString ())) 
				{
                    commandStringsAlias.Add (command.ToCommandString (), MoveToCommand (command));
				}
			}

            // Preenche todos o dicionario de command chain string
            foreach (CommandChainType command in Enum.GetValues(typeof(CommandChainType))) 
            {
                if (!attackCommandStringsAlias.ContainsKey (command.ToCommandString ())) 
                {
                    attackCommandStringsAlias.Add (command.ToCommandString (), command);
                }
            }
		}
		
		#region Updates
		/**
		 * Metodo padr�o do "MonoBehaviour" do Unity3D, 
		 * ele � executado uma vez a cada frame.
		 */
		void FixedUpdate () 
		{
			VerifyCommands();
			UpdateList();
		}
		
		private void UpdateList()
		{
			if (currentDelay < 0)
			{
				//ClearCommandList ();
			}
			else
			{
				currentDelay -= 0.1f;
			}

		}
		#endregion
		
		#region Metodos Publicos
		/**
		 * API principal de comandos, adicionando um comando a lista.
		 */ 	 
		public void AddCommand(KeyCode key)
		{
            //Debug.Log("Add Key : " + key);

			AddCommand (commandAlias[key]);
		} 
		 
		public void AddCommand(CommandType command)
		{
            //Debug.Log ("Add Command " + command);
			//Debug.Log ("Current Command " + currentCommand + " != command " + command);

			if (command != currentCommand) 
			{
				if (command == CommandType.RELEASE_KEY_ATTACK) 
					GameManager.ApplyCommand (CommandChainType.RELEASE_ATTACK);

                if (command == CommandType.KEY_JUMP)
                {
                    GameManager.ApplyCommand (CommandChainType.JUMP);
                    ClearCommandList();
                }

				//Debug.Log ("Current Command " + currentCommand + " != command " + command);
				//lastCommandTime = GameManager.GameTime;	

				//if (!currentCommand.ToString ().Contains (command.ToString ())) 
				//
					if(!inacceptablesAttackCommands.Contains(command))
					{
						//Debug.Log ("Command " + command + " != NONE");

						if (currentCommandChainString.Length > 0)
						{
							currentCommandChainString += "|";

							currentCommandChainString += "" + command;
							string[] commands = currentCommandChainString.Split ('|');

							if (commands.Length > maxKeys) 
							{
								currentCommandChainString = commands [1] + "|" + commands [2] + "|" + commands [3] + "|" + commands [4] + "|" + commands [5] + "|" + commands [6] + "|" + command;
							}
						} 
						else 
						{
							currentCommandChainString += "" + command;
						}

						//currentCommand = command;
					}

					if (fullCommandChainString.Length > 0)
					{
						fullCommandChainString += "|";

						fullCommandChainString += "" + command;
						string[] commands = fullCommandChainString.Split ('|');

						//if (commands.Length > (maxKeys * 2)) 
						//{
						//	int indexOf = fullCommandChainString.IndexOf ('|') + 1;
						//	int end = fullCommandChainString.Length - indexOf;

						//	fullCommandChainString = fullCommandChainString.Substring (indexOf, end); 
						//}
					} 
					else 
					{
						fullCommandChainString += "" + command;
					}

					currentDelay = maxDelay;	

					//commandList.Add (command);

					ApplySingleCommand (command);
					currentCommand = command;
					currentFullCommand = command;

					//Debug.Log ("currentCommandChainString " + currentCommandChainString + " Comandos: " + currentCommandChainString.Split ('|').Length);
					//Debug.Log ("fullCommandChainString " + fullCommandChainString + " Comandos: " + fullCommandChainString.Split ('|').Length);
					//Debug.Log ("Current Command " + currentCommand + " Lista de Comandos: " + commandList.Count);
				//}
			}
		}
		#endregion
		
		#region Metodos Internos
		/**
		 * Verifica a lista de comandos realizados com as listas de Todos os comandos.
		 * Seguindo a ordem de prioridade evitando que um comando invalide outro 
		 * (Ex: Hadouken - Shoryuken , Caso eu verifique o Hadouken antes do Shoryuken eu nunca acharei o Shoryuken
		 * Pois um Shoryuken nada mais � que Frete + Hadouken OBS: fica a dica pra quem sabe mandar Hadouken, mas sofre pra mandar Shoryuken !!!).
		 * 
		 * Ordem de prioridade:
		 * Comandos de 5 Teclas (Ex: Shinku-Hadouken = Baixo, Frente, Baixo, Frente, Ataque)
		 * Comandos de 4 Teclas (Ex: Shoryuken = Frente, Baixo, Frente, Ataque)
		 * Comandos de 3 Teclas (Ex: Hadouken = Baixo, Frente, Ataque)	 
		 * Comandos de 2 Teclas (Ex: Frente, Ataque)
		 * Comandos de Ataque Basico (Ex: Ataque)
	 	 */
		private void VerifyCommands()
		{
			VerifyCommandChainString ();
			VerifyMovimentChain();
		}

		private void VerifyCommandChainString()
		{
			// Inicia a verifica��o da sequencia ao executar um ataque
			if (fullCommandChainString.Contains ("KEY_ATTACK"))
			{
				VerifyAttackChain ();
			}
		}

		private void VerifyMovimentChain()
		{
			string[] commands = fullCommandChainString.Split ('|');

			// Percorre toda a sequencia buscando comandos de movimento com 4 teclas.
			for (int i = 0; i < commands.Length - 3; i++) 
			{
				string segment = commands [i] + "|" + commands [i + 1] + "|" + commands [i + 2] + "|" + commands [i + 3]; 
				VerifyChainSegment (segment);
			}
		}

		private void VerifyAttackChain()
		{
			if (currentCommandChainString.Length > 0) 
			{
				string intialChain = currentCommandChainString; 
				
				string[] commands = currentCommandChainString.Split ('|');
				string analiseString = "";

				// Constroi a String a ser verificada
				for (int i = commands.Length; i > 0; i--) 
				{
					if (i == commands.Length)
					{
						analiseString += commands [i - 1];
					} 
					else
					{
						analiseString = commands [i - 1] + "|" + analiseString;
					}
				}

				// Verifica se existe algum comando cadastrado com essa sequencia analisada
                if (attackCommandStringsAlias.ContainsKey (analiseString))
				{
                    //Debug.Log("ApplyCommand : " + analiseString);
                    GameManager.ApplyCommand (attackCommandStringsAlias [analiseString]);
					ClearCommandList ();
				}
				else
				{
					if (currentCommandChainString.Length > 1) 
					{
						// Caso n�o exista nenhum comando cadastrado, re-analisa a sequencia com -1 comando
						int indexOf = currentCommandChainString.IndexOf ('|') + 1;
						int end = currentCommandChainString.Length - indexOf;

						currentCommandChainString = currentCommandChainString.Substring (indexOf, end); 
						
                        if (!currentCommandChainString.Equals(intialChain))
                        {
                            VerifyAttackChain();
                        }
                        else
                        {
                            GameManager.ApplyCommand (CommandChainType.NORMAL_ATTACK);
                            ClearCommandList ();  
                        }
					} 
					else 
					{
                        if (currentCommandChainString.Contains("HOLD_KEY_ATTACK"))
                        {
                            GameManager.ApplyCommand(CommandChainType.HOLD_ATTACK);
                        }
                        else
                        {
                            GameManager.ApplyCommand (CommandChainType.NORMAL_ATTACK);
                            ClearCommandList ();
                        }
					}
				}
			}

			else 
			{
                if (currentCommandChainString.Contains("HOLD_KEY_ATTACK"))
                {
                    GameManager.ApplyCommand(CommandChainType.HOLD_ATTACK);
                }
                else
                {
                    GameManager.ApplyCommand (CommandChainType.NORMAL_ATTACK);
                    ClearCommandList ();
                }
			}
		}

		private void VerifyChainSegment(string segment)
		{
			if (commandStringsAlias.ContainsKey (segment)) 
			{
				GameManager.ApplyCommand (commandStringsAlias [segment]);
				ClearCommandList ();
			}
		}

		private void ApplySingleCommand(CommandType command)
		{			
			switch (command) 
			{
				case CommandType.NONE:
					isHoldingKey = false;
					GameManager.ApplyCommand (CommandChainType.NONE);
				return;

				case CommandType.STOP:
					isHoldingKey = false;
					GameManager.ApplyCommand (CommandChainType.STOP);
				return;

				// Directional Keys
				case CommandType.KEY_UP:
					if(isHoldingKey)
						GameManager.ApplyCommand (CommandChainType.MOVE_UP);
				return;

				case CommandType.KEY_UP_FORWARD:
					if(isHoldingKey)
						GameManager.ApplyCommand (CommandChainType.MOVE_UP_FORWARD);
				return;

				case CommandType.KEY_FORWARD:
					if(isHoldingKey)
						GameManager.ApplyCommand (CommandChainType.MOVE_FORWARD);
				return;

				case CommandType.KEY_FORWARD_DOWN:
					if(isHoldingKey)
						GameManager.ApplyCommand (CommandChainType.MOVE_FORWARD_DOWN);
				return;

				case CommandType.KEY_DOWN:
					if(isHoldingKey)
						GameManager.ApplyCommand (CommandChainType.MOVE_DOWN);
				return;

				case CommandType.KEY_DOWN_BACKWARD:
					if(isHoldingKey)
						GameManager.ApplyCommand (CommandChainType.MOVE_DOWN_BACKWARD);
				return;

				case CommandType.KEY_BACKWARD:
					if(isHoldingKey)
						GameManager.ApplyCommand (CommandChainType.MOVE_BACKWARD);
				return;

				case CommandType.KEY_BACKWARD_UP:
					if(isHoldingKey)
						GameManager.ApplyCommand (CommandChainType.MOVE_BACKWARD_UP);
				return;	 

				// Directional Keys
				case CommandType.HOLD_KEY_UP:
					isHoldingKey = true;
					GameManager.ApplyCommand (CommandChainType.MOVE_UP);
				return;

				case CommandType.HOLD_KEY_UP_FORWARD:
					isHoldingKey = true;
					GameManager.ApplyCommand (CommandChainType.MOVE_UP_FORWARD);
				return;

				case CommandType.HOLD_KEY_FORWARD:
					isHoldingKey = true;
					GameManager.ApplyCommand (CommandChainType.MOVE_FORWARD);
				return;

				case CommandType.HOLD_KEY_FORWARD_DOWN:
					isHoldingKey = true;
					GameManager.ApplyCommand (CommandChainType.MOVE_FORWARD_DOWN);
				return;

				case CommandType.HOLD_KEY_DOWN:
					isHoldingKey = true;
					GameManager.ApplyCommand (CommandChainType.MOVE_DOWN);
				return;

				case CommandType.HOLD_KEY_DOWN_BACKWARD:
					isHoldingKey = true;
					GameManager.ApplyCommand (CommandChainType.MOVE_DOWN_BACKWARD);
				return;

				case CommandType.HOLD_KEY_BACKWARD:
					isHoldingKey = true;
					GameManager.ApplyCommand (CommandChainType.MOVE_BACKWARD);
				return;

				case CommandType.HOLD_KEY_BACKWARD_UP:
					isHoldingKey = true;
					GameManager.ApplyCommand (CommandChainType.MOVE_BACKWARD_UP);
				return;	
			}
		}

		private void ClearCommandList()
		{
			if (fullCommandChainString.Contains ("HOLD_KEY_ATTACK"))
				GameManager.ApplyCommand (CommandChainType.HOLD_ATTACK);
				
			isHoldingKey = false;
			fullCommandChainString = "";
			currentCommandChainString = "";
			currentDelay = maxDelay;
			currentCommand = CommandType.NONE;
			currentFullCommand = CommandType.NONE;

			/*
			if (commandList.Count > 0) 
			{
				commandList.Clear ();
				currentDelay = maxDelay;
				Debug.Log ("Clear: " + commandList.Count);
			}
			*/
		}

        private CommandChainType MoveToCommand(MovimentCommandType move)
        {
            switch (move) 
            {
                case MovimentCommandType.NONE:                    
                    return CommandChainType.NONE;

                case MovimentCommandType.STOP:
                    return CommandChainType.STOP;

                case MovimentCommandType.MOVE_UP:
                    return CommandChainType.MOVE_UP;

                case MovimentCommandType.MOVE_UP_FORWARD:
                    return CommandChainType.MOVE_UP_FORWARD;

                case MovimentCommandType.MOVE_FORWARD:
                    return CommandChainType.MOVE_FORWARD;

                case MovimentCommandType.MOVE_FORWARD_DOWN:
                    return CommandChainType.MOVE_FORWARD_DOWN;

                case MovimentCommandType.MOVE_DOWN:
                    return CommandChainType.MOVE_DOWN;

                case MovimentCommandType.MOVE_DOWN_BACKWARD:
                    return CommandChainType.MOVE_DOWN_BACKWARD;

                case MovimentCommandType.MOVE_BACKWARD:
                    return CommandChainType.MOVE_BACKWARD;

                case MovimentCommandType.MOVE_BACKWARD_UP:
                    return CommandChainType.MOVE_BACKWARD_UP;


                case MovimentCommandType.RUN_UP:
                    return CommandChainType.RUN_UP;

                case MovimentCommandType.RUN_UP_FORWARD:
                    return CommandChainType.RUN_UP_FORWARD;

                case MovimentCommandType.RUN_FORWARD:
                    return CommandChainType.RUN_FORWARD;

                case MovimentCommandType.RUN_FORWARD_DOWN:
                    return CommandChainType.RUN_FORWARD_DOWN;

                case MovimentCommandType.RUN_DOWN:
                    return CommandChainType.RUN_DOWN;

                case MovimentCommandType.RUN_DOWN_BACKWARD:
                    return CommandChainType.RUN_DOWN_BACKWARD;

                case MovimentCommandType.RUN_BACKWARD:
                    return CommandChainType.RUN_BACKWARD;

                case MovimentCommandType.RUN_BACKWARD_UP:
                    return CommandChainType.RUN_BACKWARD_UP;

                default:
                    return CommandChainType.NONE;
            }
        }
		#endregion
	}

	public enum CommandType
	{
		NONE,
		STOP,
		
		KEY_UP,
		KEY_UP_FORWARD,
		KEY_FORWARD,
		KEY_FORWARD_DOWN,
		KEY_DOWN,
		KEY_DOWN_BACKWARD,
		KEY_BACKWARD,	
		KEY_BACKWARD_UP,	
		
		HOLD_KEY_UP,
		HOLD_KEY_UP_FORWARD,
		HOLD_KEY_FORWARD,
		HOLD_KEY_FORWARD_DOWN,
		HOLD_KEY_DOWN,
		HOLD_KEY_DOWN_BACKWARD,
		HOLD_KEY_BACKWARD,	
		HOLD_KEY_BACKWARD_UP,
		
		KEY_ATTACK,
		HOLD_KEY_ATTACK,
		RELEASE_KEY_ATTACK,

        KEY_JUMP,
        HOLD_KEY_JUMP,
        RELEASE_KEY_JUMP
	}

    public enum MovimentCommandType
    {
        NONE,
        STOP,

        MOVE_UP,
        MOVE_UP_FORWARD,
        MOVE_FORWARD,
        MOVE_FORWARD_DOWN,
        MOVE_DOWN,
        MOVE_DOWN_BACKWARD,
        MOVE_BACKWARD,
        MOVE_BACKWARD_UP,

        // ===== Double Command Chains =====
        RUN_UP,
        RUN_UP_FORWARD,
        RUN_FORWARD,
        RUN_FORWARD_DOWN,
        RUN_DOWN,
        RUN_DOWN_BACKWARD,
        RUN_BACKWARD,
        RUN_BACKWARD_UP,
    }

	public enum CommandChainType
	{	
		NONE,
		STOP,

		HOLD_ATTACK,
		RELEASE_ATTACK,	
        NORMAL_ATTACK,	
        DIRECT_ATTACK,  
        KICK_ATTACK,  
        FORWARD_FORCE_ATTACK, 
        BACKWARD_FORCE_ATTACK,
		DIRECTIONAL_ATTACK,
		RUNNING_ATTACK,

		MOVE_UP,
		MOVE_UP_FORWARD,
		MOVE_FORWARD,
		MOVE_FORWARD_DOWN,
		MOVE_DOWN,
		MOVE_DOWN_BACKWARD,
		MOVE_BACKWARD,
		MOVE_BACKWARD_UP,

        JUMP,
        JUMP_FORWARD,
        JUMP_BACKWARD,

		// ===== Double Command Chains =====
		UP_ATTACK,
		FORWARD_ATTACK,
		DOWN_ATTACK,
		BACKWARD_ATTACK,
		
		RUN_UP,
		RUN_UP_FORWARD,
		RUN_FORWARD,
		RUN_FORWARD_DOWN,
		RUN_DOWN,
		RUN_DOWN_BACKWARD,
		RUN_BACKWARD,
		RUN_BACKWARD_UP,
		
		// ===== Triple Command Chains ===== 	
		// *** Tr�s-Frentes ***
		UP_DOWN_ATTACK,
		DOWN_UP_ATTACK,
		BACK_FORWARD_ATTACK,
		FORWARD_BACK_ATTACK,
		
		// *** Hadoukens ***
		DOWN_FORWARD_ATTACK, 
		FORWARD_DOWN_ATTACK,
		DOWN_BACKWARD_ATTACK,
		BACKWARD_DOWN_ATTACK,
			
		// *** Novos Hadoukens ***
		UP_FORWARD_ATTACK,
		FORWARD_UP_ATTACK,
		UP_BACKWARD_ATTACK,
		BACKWARD_UP_ATTACK,
		
		// ===== Quadruple Command Chains ======	
		// *** Hadoukens Completos ***
		BACK_DOWN_FORWARD_ATTACK,
		FORWARD_DOWN_BACK_ATTACK,
		BACK_UP_FORWARD_ATTACK,
		FORWARD_UP_BACK_ATTACK,
		
		// *** Novos Hadoukens Completos ***
		UP_FORWARD_DOWN_ATTACK,
		DOWN_FORWARD_UP_ATTACK,
		UP_BACKWARD_DOWN_ATTACK,
		DOWN_BACKWARD_UP_ATTACK,
		
		// *** Shoryukens ***
		BACK_DOWN_BACKWARD_ATTACK,
		FORWARD_DOWN_FORWARD_ATTACK,
		BACK_UP_BACKWARD_ATTACK,
		FORWARD_UP_FORWARD_ATTACK,
		
		// *** Kame-Hame-H�s (Hyper Dimension)
		DOWN_BACK_FORWARD_ATTACK,
		UP_BACK_FORWARD_ATTACK,
		DOWN_FORWARD_BACK_ATTACK,
		UP_FORWARD_BACK_ATTACK,	
		
		// ===== Quintuple Command Chains ======	
		// *** Double Hadoukens ***
		DOWN_FORWARD_DOWN_FORWARD_ATTACK,
		DOWN_BACK_DOWN_BACKWARD_ATTACK,
		FORWARD_DOWN_FORWARD_DOWN_ATTACK,
		BACK_DOWN_BACK_DOWN_ATTACK,
		
		// *** Double Tr�s-Frentes ***
		UP_DOWN_UP_DOWN_ATTACK,
		DOWN_UP_DOWN_UP_ATTACK,
		BACK_FORWARD_BACK_FORWARD_ATTACK,
		FORWARD_BACK_FORWARD_BACKWARD_ATTACK,
		
		// *** Double Novos Hadoukens ***
		UP_FORWARD_UP_FORWARD_ATTACK,
		FORWARD_UP_FORWARD_UP_ATTACK,
		UP_BACK_UP_BACKWARD_ATTACK,
		BACK_UP_BACK_UP_ATTACK,
		
		// *** Hadouken Ida e Volta ***
		DOWN_BACK_DOWN_FORWARD_ATTACK,
		DOWN_FORWARD_DOWN_BACKWARD_ATTACK,
		UP_BACK_UP_FORWARD_ATTACK,
		UP_FORWARD_UP_BACKWARD_ATTACK,	
		
		// *** HAO-Shokokens ***
		FORWARD_BACK_DOWN_FORWARD_ATTACK,
		BACK_FORWARD_DOWN_BACKWARD_ATTACK,
		FORWARD_BACK_UP_FORWARD_ATTACK,
		BACK_FORWARD_UP_BACK_ATTACK,
	}

	public static class MovimentCommand
	{
        public static string ToCommandString(this MovimentCommandType me)
		{
		    switch(me)
		    {
                case MovimentCommandType.NONE: return "NONE";
                case MovimentCommandType.STOP: return "STOP";
				
                case MovimentCommandType.MOVE_BACKWARD: return "HOLD_KEY_BACKWARD";
                case MovimentCommandType.MOVE_DOWN_BACKWARD: return "HOLD_KEY_DOWN_BACKWARD";
                case MovimentCommandType.MOVE_DOWN: return "HOLD_KEY_DOWN";
                case MovimentCommandType.MOVE_FORWARD_DOWN: return "HOLD_KEY_FORWARD_DOWN";
                case MovimentCommandType.MOVE_FORWARD: return "HOLD_KEY_FORWARD";
                case MovimentCommandType.MOVE_UP_FORWARD: return "HOLD_KEY_UP_FORWARD";
                case MovimentCommandType.MOVE_UP: return "HOLD_KEY_UP";
                case MovimentCommandType.MOVE_BACKWARD_UP: return "HOLD_KEY_BACKWARD_UP";

                case MovimentCommandType.RUN_BACKWARD: return "KEY_BACKWARD|STOP|KEY_BACKWARD|HOLD_KEY_BACKWARD";
                case MovimentCommandType.RUN_DOWN_BACKWARD: return "KEY_DOWN_BACKWARD|STOP|KEY_DOWN_BACKWARD|HOLD_KEY_DOWN_BACKWARD";
                case MovimentCommandType.RUN_DOWN: return "KEY_DOWN|STOP|KEY_DOWN|HOLD_KEY_DOWN";
                case MovimentCommandType.RUN_FORWARD_DOWN: return "KEY_FORWARD_DOWN|STOP|KEY_FORWARD_DOWN|HOLD_KEY_FORWARD_DOWN";
                case MovimentCommandType.RUN_FORWARD: return "KEY_FORWARD|STOP|KEY_FORWARD|HOLD_KEY_FORWARD";
                case MovimentCommandType.RUN_UP_FORWARD: return "KEY_UP_FORWARD|STOP|KEY_UP_FORWARD|HOLD_KEY_UP_FORWARD";
                case MovimentCommandType.RUN_UP: return "KEY_UP|STOP|KEY_UP|HOLD_KEY_UP";
                case MovimentCommandType.RUN_BACKWARD_UP: return "KEY_BACKWARD_UP|STOP|KEY_BACKWARD_UP|HOLD_KEY_BACKWARD_UP";
				
				default:
					return "";
		    }
		}
	}

    public static class Command
    {
        public static string ToCommandString(this CommandChainType me)
        {
            switch(me)
            {
                case CommandChainType.NONE: return "NONE";
                case CommandChainType.STOP: return "STOP";

                case CommandChainType.NORMAL_ATTACK: return "KEY_ATTACK";
                case CommandChainType.HOLD_ATTACK: return "HOLD_KEY_ATTACK";

                case CommandChainType.DOWN_FORWARD_ATTACK: return "KEY_DOWN|KEY_FORWARD";
                case CommandChainType.DOWN_BACKWARD_ATTACK: return "KEY_DOWN|KEY_BACKWARD";

                case CommandChainType.FORWARD_DOWN_FORWARD_ATTACK: return "KEY_FORWARD|KEY_DOWN|KEY_FORWARD";
                case CommandChainType.BACK_DOWN_BACKWARD_ATTACK: return "KEY_BACKWARD|KEY_DOWN|KEY_BACKWARD";               

                case CommandChainType.FORWARD_DOWN_BACK_ATTACK: return "KEY_FORWARD|KEY_DOWN|KEY_BACKWARD";
                case CommandChainType.BACK_DOWN_FORWARD_ATTACK: return "KEY_BACKWARD|KEY_DOWN|KEY_FORWARD";

                case CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK: return "KEY_DOWN|KEY_FORWARD|KEY_DOWN|KEY_FORWARD";
                case CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK: return "KEY_DOWN|KEY_BACKWARD|KEY_DOWN|KEY_BACKWARD";

                default:
                    return "";
            }
        }
    }
}