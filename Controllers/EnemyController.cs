using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Skills;
using Actions;
using Commands;

namespace Characters
{
	public class EnemyController : MonoBehaviour
	{
        private CharacterController enemyController;
        private GameObject opponent;

        private int minCooldown;    
        private int maxCooldown;

        private int nextActionTime;

		private int sequenceIndex;	
        private bool isWaiting;

        private float lastAction;
        private float actionCoolDown;
		
		// Use this for initialization
		void Start ()
		{
            enemyController = GetComponentInChildren<CharacterController>(); 
            opponent = GameObject.FindGameObjectWithTag("MainPlayer");
            lastAction = Time.fixedTime;
            actionCoolDown = 2f;
            minCooldown = 50;
            maxCooldown = 100;
            nextActionTime = Random.Range(minCooldown, maxCooldown);
		}
		
        void FixedUpdate()
        {
            VerifyActionCoolDown();
        }

        private void VerifyActionCoolDown()
        {
            //Debug.Log("currentCooldown : " + nextActionTime);

            nextActionTime--;
            if (nextActionTime <= 0)
            {             
                RandomNextAction();
            }
        }

        private void RandomNextAction()
        {   
            nextActionTime = Random.Range(minCooldown, maxCooldown); 

            int nextChance = Random.Range(0, 100);
            //Debug.Log("Next Action: " + nextChance);


            // 20% de executar um Movimento
            if (nextChance >= 0 && nextChance < 20)
            {
                DoRandomMoviment();
                nextActionTime = Random.Range(10, 20);
            }

            // 80% de executar uma Skill
            if (nextChance >= 00 && nextChance < 100)
            {
                DoRandomSkill();
                nextActionTime = Random.Range(minCooldown, maxCooldown);
            }
        }
		
		private void DoRandomMoviment()
		{
			int moveChance = Random.Range(0, 100);

            //Debug.Log("opponent " + opponent.transform.position + " me " + this.transform.position);

            float distanceX = this.transform.position.x - opponent.transform.position.x;
            float distanceZ = this.transform.position.z - opponent.transform.position.z ;

            //Debug.Log("distanceX " + distanceX + "distanceZ " + distanceZ);

            if (distanceX > 4)
            {
                moveChance = 45;
                //Debug.Log("Muito X " + distanceX);

                if (distanceZ > 1.5f)
                {
                    moveChance = 95;
                    //Debug.Log("Muito Z " + distanceZ);
                }
                else
                {
                    if (distanceZ < -1.5f)
                    {
                        moveChance = 90;
                        //Debug.Log("Pouco Z " + distanceZ);
                    }
                }
            }
            else
            {
                if (distanceX < -4)
                {
                    moveChance = 10;
                    //Debug.Log("Pouco X " + distanceX);

                    if (distanceZ > 1.5f)
                    {
                        moveChance = 85;
                        //Debug.Log("Muito Z " + distanceZ);
                    }
                    else
                    {
                        if (distanceZ < -1.5f)
                        {
                            moveChance = 80;
                            //Debug.Log("Pouco Z " + distanceZ);
                        }
                    }
                }
            }
			
			// 10% de ficar parado
			if (moveChance >= 0 && moveChance < 10)
			{			
				enemyController.ApplyCommand(CommandChainType.STOP, enemyController.transform.position);
			}
			
			// 35% de Andar pra Frente
			if (moveChance >= 10 && moveChance < 45)
			{			
				enemyController.ApplyCommand(CommandChainType.MOVE_FORWARD, enemyController.transform.position);
			}
			
			// 35% de Andar pra Traz
			if (moveChance >= 45 && moveChance < 80)
			{			
				enemyController.ApplyCommand(CommandChainType.MOVE_BACKWARD, enemyController.transform.position);
			}
			
			// 5% de Andar pra Cima
			if (moveChance >= 60 && moveChance < 70)
			{			
				enemyController.ApplyCommand(CommandChainType.MOVE_UP, enemyController.transform.position);
			}
			
			// 5% de Andar pra Baixo
			if (moveChance >= 70 && moveChance < 80)
			{			
				enemyController.ApplyCommand(CommandChainType.MOVE_DOWN, enemyController.transform.position);
			}
			
			// 2.5% de Andar na Diagonal
			if (moveChance >= 80 && moveChance < 85)
			{			
				enemyController.ApplyCommand(CommandChainType.MOVE_UP_FORWARD, enemyController.transform.position);
			}
			
			// 2.5% de Andar na Diagonal
			if (moveChance >= 85 && moveChance < 90)
			{			
				enemyController.ApplyCommand(CommandChainType.MOVE_FORWARD_DOWN, enemyController.transform.position);
			}
			
			// 2.5% de Andar na Diagonal
			if (moveChance >= 90 && moveChance < 95)
			{			
				enemyController.ApplyCommand(CommandChainType.MOVE_BACKWARD_UP, enemyController.transform.position);
			}
			
			// 2.5% de Andar na Diagonal
			if (moveChance >= 95 && moveChance < 100)
			{			
				enemyController.ApplyCommand(CommandChainType.MOVE_DOWN_BACKWARD, enemyController.transform.position);
			}
		}
		
		private void DoRandomSkill()
		{
			int skillChance = Random.Range(0, 100);
            //Debug.Log("Skill Chance : " + skillChance);

            float distanceX = opponent.transform.position.x - this.transform.position.x;
            float distanceZ = opponent.transform.position.z - this.transform.position.z;

            if (distanceX > 0)
            {
                enemyController.ApplyCommand(CommandChainType.MOVE_FORWARD, enemyController.transform.position);
            }
            else
            {
                enemyController.ApplyCommand(CommandChainType.MOVE_BACKWARD, enemyController.transform.position);
            }			

			// 10% de n realizar nenhuma ação
			if (skillChance >= 0 && skillChance < 10)
			{
                // TODO None 
			}

			// 15% de executar a sequencia de socos
			if (skillChance >= 10 && skillChance < 25)
			{			
				SendContinuosCommand(CommandChainType.NORMAL_ATTACK, 10, 0.3f);
			}		
			
            // 25% de executar um hadouken
            if (skillChance >= 25 && skillChance < 50)
			{
				if (enemyController.isFacingRight)
				{
					enemyController.ApplyCommand(CommandChainType.DOWN_FORWARD_ATTACK, enemyController.transform.position);
				}
				else
				{
					enemyController.ApplyCommand(CommandChainType.DOWN_BACKWARD_ATTACK, enemyController.transform.position);
				}					
			}			

			// 20% de executar um shoryuken
			if (skillChance >= 50 && skillChance < 70)
			{
				if (enemyController.isFacingRight)
				{
					enemyController.ApplyCommand(CommandChainType.FORWARD_DOWN_FORWARD_ATTACK, enemyController.transform.position);
				}
				else
				{
					enemyController.ApplyCommand(CommandChainType.BACK_DOWN_BACKWARD_ATTACK, enemyController.transform.position);
				}
			}
			
			// 25% de executar um Ataque das Corujas
            if (skillChance >= 70 && skillChance < 71)
			{
				if (enemyController.isFacingRight)
				{
					enemyController.ApplyCommand(CommandChainType.FORWARD_DOWN_BACK_ATTACK, enemyController.transform.position);
				}
				else
				{
					enemyController.ApplyCommand(CommandChainType.BACK_DOWN_FORWARD_ATTACK, enemyController.transform.position);
				}
			}
			
			// 10% de Executar o especial 
			if (skillChance >= 72 && skillChance < 100)
			{
				if (enemyController.isFacingRight)
				{
					enemyController.ApplyCommand(CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK, enemyController.transform.position);
				}
				else
				{
					enemyController.ApplyCommand(CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK, enemyController.transform.position);
				}

                enemyController.ApplyCommand(CommandChainType.HOLD_ATTACK, enemyController.transform.position);

                float chargeAmount = Random.Range(10, 16);

                //Debug.Log("Special Charge : " + chargeAmount);
                DelayedReleaseSpecial(chargeAmount);
			}
		}				
		
		private void SendContinuosCommand(CommandChainType command, int amount, float interval)
		{
			sequenceIndex++;			
			enemyController.ApplyCommand(command, enemyController.transform.position);
			
			//yield return new WaitForSeconds (interval);
			
            if (sequenceIndex <= amount)
			{
                //StartCoroutine (SendContinuosCommand(command, amount, interval));
			}	
			else
			{
				sequenceIndex = 0;
			}
		}

        private void DelayedReleaseSpecial(float chargeAmount)
        {
            //yield return new WaitForSeconds (chargeAmount);

            enemyController.ApplyCommand(CommandChainType.RELEASE_ATTACK, enemyController.transform.position);
        }
	}
}