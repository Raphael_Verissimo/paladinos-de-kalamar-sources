using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Skills;
using Sprites;
using Actions;
using Commands;

namespace Characters
{
    public class GokuController : CharacterController
    {       
        void Start ()
        {   
            timeToPush = 0f;
            timeToResume = 0f;
            isFacingRight = true;
            movimentSpeedFactor = 1.5f;
            runningSpeedFactor = 2.5f;
            timeToPushCoolDown = 0.2f;
            timeToResumeCoolDown = 0.2f;

            comboIndex = -1;
            startCombo = false;
            comboTimmer = 0.3f;
            comboCooldown = 0.5f;

            body = GetComponent<Rigidbody> ();

            spriteController = GetComponentInChildren<SpriteController>();

            //characterAnimator = GetComponent<Animator> ();
            //this.transform.LookAt (Camera.main.transform);

            Idle ();

            lockTimes.Add (SkillType.DASH, 1f);    
            maxCoolDowns.Add (SkillType.DASH, 1f); 
            availableSkills.Add(SkillType.DASH);

            commandAllias.Add (CommandChainType.RUN_FORWARD, SkillType.DASH);
            commandDirections.Add (CommandChainType.RUN_FORWARD, true);

            commandAllias.Add (CommandChainType.RUN_BACKWARD, SkillType.DASH); 
            commandDirections.Add (CommandChainType.RUN_BACKWARD, false);

            lockTimes.Add (SkillType.JUMP, 1f);    
            maxCoolDowns.Add (SkillType.JUMP, 0.1f); 
            availableSkills.Add(SkillType.JUMP);
            commandAllias.Add (CommandChainType.JUMP, SkillType.JUMP);

            attackSequence.Add(SkillType.JAB);
            attackSequence.Add(SkillType.PUNCH);
            attackSequence.Add(SkillType.STRONG_KICK);
            attackSequence.Add(SkillType.KICK);
            attackSequence.Add(SkillType.STRONG_PUNCH);

            commandAllias.Add (CommandChainType.NORMAL_ATTACK, SkillType.JAB);

            lockTimes.Add (SkillType.JAB, 0.2f);    
            maxCoolDowns.Add (SkillType.JAB, 0.2f); 
            availableSkills.Add(SkillType.JAB);

            lockTimes.Add (SkillType.PUNCH, 0.3f);    
            maxCoolDowns.Add (SkillType.PUNCH, 0.3f);   
            availableSkills.Add(SkillType.PUNCH);

            lockTimes.Add (SkillType.KICK, 0.35f);    
            maxCoolDowns.Add (SkillType.KICK, 0.35f);   
            availableSkills.Add(SkillType.KICK);

            lockTimes.Add (SkillType.STRONG_KICK, 0.35f);    
            maxCoolDowns.Add (SkillType.STRONG_KICK, 0.35f);   
            availableSkills.Add(SkillType.STRONG_KICK);

            lockTimes.Add (SkillType.STRONG_PUNCH, 1.0f);    
            maxCoolDowns.Add (SkillType.STRONG_PUNCH, 1.0f);   
            availableSkills.Add(SkillType.STRONG_PUNCH);

            lockTimes.Add (SkillType.DASH_ATTACK, 2f);  
            maxCoolDowns.Add (SkillType.DASH_ATTACK, 2f); 
            availableSkills.Add(SkillType.DASH_ATTACK);

            lockTimes.Add (SkillType.VOADORA, 0.2f);  
            maxCoolDowns.Add (SkillType.VOADORA, 0.2f); 
            availableSkills.Add(SkillType.VOADORA);

            commandAllias.Add (CommandChainType.FORWARD_FORCE_ATTACK, SkillType.FORCE_ATTACK);
            commandDirections.Add (CommandChainType.FORWARD_FORCE_ATTACK, true);

            commandAllias.Add (CommandChainType.BACKWARD_FORCE_ATTACK, SkillType.FORCE_ATTACK);    
            commandDirections.Add (CommandChainType.BACKWARD_FORCE_ATTACK, false);

            lockTimes.Add (SkillType.FORCE_ATTACK, 0.1f);  
            maxCoolDowns.Add (SkillType.FORCE_ATTACK, 0.1f); 
            availableSkills.Add(SkillType.FORCE_ATTACK);

            commandAllias.Add (CommandChainType.DOWN_FORWARD_ATTACK, SkillType.KIBLAST);
            commandDirections.Add (CommandChainType.DOWN_FORWARD_ATTACK, true);

            commandAllias.Add (CommandChainType.DOWN_BACKWARD_ATTACK, SkillType.KIBLAST);  
            commandDirections.Add (CommandChainType.DOWN_BACKWARD_ATTACK, false);

            lockTimes.Add (SkillType.KIBLAST, 0.1f);   
            maxCoolDowns.Add (SkillType.KIBLAST, 1f);  
            availableSkills.Add(SkillType.KIBLAST);

            commandAllias.Add (CommandChainType.FORWARD_DOWN_FORWARD_ATTACK, SkillType.COUNTER_TELEPORT);
            commandDirections.Add (CommandChainType.FORWARD_DOWN_FORWARD_ATTACK, true);

            commandAllias.Add (CommandChainType.BACK_DOWN_BACKWARD_ATTACK, SkillType.COUNTER_TELEPORT);    
            commandDirections.Add (CommandChainType.BACK_DOWN_BACKWARD_ATTACK, false);

            lockTimes.Add (SkillType.COUNTER_TELEPORT, 0.1f);  
            maxCoolDowns.Add (SkillType.COUNTER_TELEPORT, 0.1f); 
            availableSkills.Add(SkillType.COUNTER_TELEPORT);

            commandAllias.Add (CommandChainType.FORWARD_DOWN_BACK_ATTACK, SkillType.TRIPLE_STRIKE);
            commandDirections.Add (CommandChainType.FORWARD_DOWN_BACK_ATTACK, true);

            commandAllias.Add (CommandChainType.BACK_DOWN_FORWARD_ATTACK, SkillType.TRIPLE_STRIKE); 
            commandDirections.Add (CommandChainType.BACK_DOWN_FORWARD_ATTACK, false);

            lockTimes.Add (SkillType.TRIPLE_STRIKE, 2f);    
            maxCoolDowns.Add (SkillType.TRIPLE_STRIKE, 5f); 
            availableSkills.Add(SkillType.TRIPLE_STRIKE);

            commandAllias.Add (CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK, SkillType.KAME_HAME_HA);
            commandDirections.Add (CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK, true);

            commandAllias.Add (CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK, SkillType.KAME_HAME_HA); 
            commandDirections.Add (CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK, false);

            lockTimes.Add (SkillType.KAME_HAME_HA, 100f);    
            maxCoolDowns.Add (SkillType.KAME_HAME_HA, 5f); 
            availableSkills.Add(SkillType.KAME_HAME_HA);
        }   

        #region Goku Methods
        override internal void MoveTo (Vector3 origin, Vector3 direction)
		{   
			currentState = CharacterState.WALKING;

            //Debug.Log("Move Goku: " + direction);
            currentrVelocity = 1.5f;

            if (!isRunning)
            {            
                isMoving = true;
                currentDirection = direction.normalized * movimentSpeedFactor;

                if (spriteController)
                {
                    if (direction.x > 0)
                        isFacingRight = true;

                    if (direction.x < 0)
                        isFacingRight = false;

                    spriteController.PlayWalk(isFacingRight);
                } 
            }
        }

        override internal void DashTo(Vector3 origin, Vector3 direction)
		{       
			currentState = CharacterState.DASHING;

            //Debug.Log("Run Goku: " + direction);
            isRunning = true;
            isRunningAttack = false;
             
            if (spriteController)
            {
                if (direction.x > 0 || direction.z > 0)
                    isFacingRight = true;

                if (direction.x < 0 || direction.z < 0)
                    isFacingRight = false;

                spriteController.PlayRun(isFacingRight);
            } 

            if (isFacingRight)
                PlaySkill(CommandChainType.RUN_FORWARD);
            else
                PlaySkill(CommandChainType.RUN_BACKWARD);

            // TODO MOVE Character (Usando Rigidy Body e Velocity para ficar Smooth no Bullet Time)
        }

        override internal void PlaySkill(CommandChainType command)
        {            
            if (availableSkills.Contains(commandAllias[command]))
                availableSkills.Remove(commandAllias[command]);
            
            currentrVelocity = 0;

            //Debug.Log("PlaySkill:" + commandAllias[command]);
            if (commandDirections.ContainsKey (command)) 
            {
                if (commandDirections [command]) 
                {
                    currentDirection = new Vector3 (1, 0, 0);
                    isFacingRight = true;
                } 
                else 
                {
                    currentDirection = new Vector3 (-1, 0, 0);
                    isFacingRight = false;
                }
            } 
            else
            {
                if (isFacingRight) 
                {
                    currentDirection = new Vector3 (1, 0, 0);
                } 
                else 
                {
                    currentDirection = new Vector3 (-1, 0, 0);
                } 
            }

            if (command == CommandChainType.NORMAL_ATTACK)
            {
                if (isRunning)
                {
                    Dash dashAction = this.gameObject.GetComponentInChildren<Dash>();

                    if (dashAction)
                    {
                        dashAction.hasAttacked = true;
                        SkillManager.UseSkill(SkillType.DASH_ATTACK, this);
                        StartCoroutine(ResetCooldown(SkillType.DASH_ATTACK));
                        StartCoroutine(LockCharacter(lockTimes[SkillType.DASH_ATTACK]));
                    }

                    //Debug.Log("lockTimes: " + lockTimes[SkillType.DASH_ATTACK]);                        
                }
                else
                {
                    startCombo = true;
                    comboIndex = (comboIndex + 1) % attackSequence.Count;

                    //Debug.Log ("Attack:" + comboIndex + " > " + attackSequence[comboIndex]);

                    commandAllias[CommandChainType.NORMAL_ATTACK] = attackSequence[comboIndex];
                    //StartCoroutine (ApplyCombo(comboTimmer));    

                    SkillManager.UseSkill (commandAllias[command], this);
                    StartCoroutine (ResetCooldown(commandAllias[command]));
                    StartCoroutine (LockCharacter(lockTimes [commandAllias[command]]));
                }
            }
            else
            {
                SkillManager.UseSkill (commandAllias[command], this);
                StartCoroutine (ResetCooldown(commandAllias[command]));
                StartCoroutine (LockCharacter(lockTimes [commandAllias[command]]));

                if (commandAllias[command] == SkillType.KAME_HAME_HA) 
                {
                    StartCoroutine (RecursiveChargeSpecial(1.6f));
                }    
            }
        }

        override internal void PlaySkillAnimation(SkillType type)
        {
            //Debug.Log("PlaySkillAnimation: " + type);
            currentrVelocity = 0;

            switch (type) 
            {
                case SkillType.JAB:
                    PlayJab ();
                    break;

                case SkillType.PUNCH:
                    PlayPunch ();
                    break;

                case SkillType.KICK:
                    PlayKick ();
                    break;

                case SkillType.STRONG_KICK:
                    PlayStrongKick ();
                    break;

                case SkillType.STRONG_PUNCH:
                    PlayStrongPunch ();
                    break;

                case SkillType.FORCE_ATTACK:
                    PlayForceAttack ();
                    break;

                case SkillType.DASH_ATTACK:
                    PlayDashAttack ();
                    break;

                case SkillType.KIBLAST:
                    PlayKiBlast ();
                    break;

                case SkillType.COUNTER_TELEPORT:
                    PlayCounterTeleport ();
                break;

                case SkillType.TRIPLE_STRIKE:
                    PlayTripleStrike ();
                break;

                case SkillType.KAME_HAME_HA:
                    PlayStartSpecial ();
                break;
            }      
        }

        override internal void HoldAttack ()
        {
            //Debug.Log("HoldAttack Attack !!!");
            isHoldingAttack = true;
        }

        override internal void ReleaseAttack ()
        {
            //Debug.Log("Release Attack !!!");
            isHoldingAttack = false;

            if (isChargingSpecial) 
            {
                ShotSpecial ();
            }
        }
        #endregion       

        private void PlayJab()
        {
            if (spriteController)
            {
                spriteController.PlayAttack1(isFacingRight);
            } 
        }

        private void PlayPunch()
        {
            if (spriteController)
            {
                spriteController.PlayAttack2(isFacingRight);
            } 
        }

        private void PlayKick()
        {
            if (spriteController)
            {
                spriteController.PlayAttack3(isFacingRight);
            } 
        }

        private void PlayStrongKick()
        {
            if (spriteController)
            {
                spriteController.PlayAttack4(isFacingRight);
            } 
        }

        private void PlayStrongPunch()
        {
            if (spriteController)
            {
                spriteController.PlayAttack5(isFacingRight);
            } 
        }

        private void PlayForceAttack()
        {
            if (spriteController)
            {
                spriteController.PlayForceAttack(isFacingRight);
            } 
        }

        private void PlayDashAttack()
        {
            if (spriteController)
            {
                spriteController.PlayDashAttack(isFacingRight);
            } 
        }

        private void PlayKiBlast()
        {
            if (spriteController)
            {
                spriteController.PlaySkill1(isFacingRight);
            } 
        }

        private void PlayCounterTeleport()
        {
            if (spriteController)
            {
                spriteController.PlaySkill2(isFacingRight);
            } 
        }

        private void PlayTripleStrike()
        {
            if (spriteController)
            {
                spriteController.PlaySkill3(isFacingRight);
            } 
        }
      
        private void PlayStartSpecial()
        {
            if (spriteController)
            {
                //spriteController.PlaySpecial1(isFacingRight);
                IList<AnimationType> actions = new List<AnimationType>();

                actions.Add(AnimationType.Special1);
                actions.Add(AnimationType.Special2);

                spriteController.PlaySequence(actions, isFacingRight);

                effectsSources.clip = SpecialStartClip;
                effectsSources.Play();

                voiceSources.clip = SpecialStartVoice;
                voiceSources.Play();

                StartCoroutine(DelayedVoice(ChargingSpecialVoice, 5.0f));
            } 
        }

        private void ShotSpecial()
        {
            isChargingSpecial = false;
            StartCoroutine (LockCharacter(0.3f));

            GetComponentInChildren<KameHameHa> ().OnShot ();

            PlayShotSpecial ();
        }

        private void PlayShotSpecial()
        {
            if (spriteController)
            {
                spriteController.PlaySpecial3(isFacingRight);               

                effectsSources.clip = ShotSpecialClip;
                effectsSources.Play();

                voiceSources.clip = ShotSpecialVoice;
                voiceSources.Play();

                /*
                IList<SpriteController.AnimationType> actions = new List<SpriteController.AnimationType>();

                actions.Add(SpriteController.AnimationType.Special3);
                actions.Add(SpriteController.AnimationType.Idle);

                spriteController.PlaySequence(actions, isFacingRight);
                */
            } 
        }

        IEnumerator RecursiveChargeSpecial(float delay)
        {
            isChargingSpecial = true;

            yield return new WaitForSeconds (delay);

            if (isHoldingAttack)
            {
                if (spriteController && isChargingSpecial) 
                {
                    spriteController.PlaySpecial2 (isFacingRight);

                    GetComponentInChildren<KameHameHa> ().OnCharge ();

                    if (isChargingSpecial)
                        StartCoroutine (RecursiveChargeSpecial (0.9f));
                } 
            }
        }

        IEnumerator ResetCooldown(SkillType skill)
        {
            //Debug.Log ("SkillType:" + skill + " cooldown:" + maxCoolDowns [skill]);

            yield return new WaitForSeconds (maxCoolDowns[skill]);

            availableSkills.Add(skill);

            //Debug.Log ("SkillType:" + skill + " Ready");
        }

        IEnumerator LockCharacter(float lockTime)
        {
            //Debug.Log ("SkillType:" + skill + " cooldown:" + maxCoolDowns [skill]);
            isLocked = true;

            yield return new WaitForSeconds (lockTime);

            isLocked = false;
            //Debug.Log ("Resume Command:" + lastLockedCommand);
            //ApplyCommand(lastLockedCommand, transform.position);
            // Resumir o moviemnto do corrente

            //Debug.Log ("SkillType:" + skill + " Ready");
        }

        IEnumerator DelayedVoice(AudioClip clip, float delay)
        {
            yield return new WaitForSeconds (delay);

            voiceSources.clip = clip;
            voiceSources.Play();
        }
    }
}