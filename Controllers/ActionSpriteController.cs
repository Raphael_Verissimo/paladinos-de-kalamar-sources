﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionSpriteController : MonoBehaviour {

	[SerializeField]
	private Sprite[] idleSpriteSheet;

	[SerializeField]
	private Sprite[] moveSpriteSheet;

	[SerializeField]
	private Sprite[] hitSpriteSheet;

	[SerializeField]
	private Sprite[] explodeSpriteSheet;

	[SerializeField]
	private Sprite[] dismissSpriteSheet;

	[SerializeField]
	private float interval;

	private float lastTime;

	private int currentIndex;
	private SpriteRenderer spriteRenderer;
	private Sprite[] currentSpriteSheet;
	private Vector3 originalDirection;
	private Vector3 invertDirection;
	private bool facingRight;

	private ActionAnimationType currentAction;

	// Use this for initialization
	void Start ()
	{
		currentAction = ActionAnimationType.Idle;

		currentIndex = 0;
		lastTime = 0;
		spriteRenderer = GetComponentInChildren<SpriteRenderer>(); 

		facingRight = true;

		originalDirection = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
		invertDirection = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateCurrentSprite ();
	}

	public void PlayIdle(bool inverted){ currentAction = ActionAnimationType.Idle; this.facingRight = inverted;}
	public void PlayMove(bool inverted){ currentAction = ActionAnimationType.Move; this.facingRight = inverted;}
	public void PlayHit(bool inverted){	currentAction = ActionAnimationType.Hit; this.facingRight = inverted;}
	public void PlayExplode(bool inverted){	currentAction = ActionAnimationType.Explode; this.facingRight = inverted;}
	public void PlayDismiss(){ currentAction = ActionAnimationType.Dismiss; }

	private void UpdateCurrentSprite()
	{
		switch (currentAction)
		{
			case ActionAnimationType.Idle:
					currentSpriteSheet = idleSpriteSheet;
			break;

			case ActionAnimationType.Move:
				currentSpriteSheet = moveSpriteSheet;
			break;

			case ActionAnimationType.Hit:
				currentSpriteSheet = hitSpriteSheet;
			break;

			case ActionAnimationType.Explode:
				currentSpriteSheet = explodeSpriteSheet;
			break;

			case ActionAnimationType.Dismiss:
				currentSpriteSheet = dismissSpriteSheet;
			break;
		}

		//Debug.Log("Tempo : " + (Time.fixedTime - lastTime) + " Indice : " + currentIndex + " max:" + currentSpriteSheet.Length + " facingRight:" + facingRight);
		//Debug.Log(" facingRight:" + facingRight);

		if (Time.fixedTime - lastTime > interval && currentSpriteSheet.Length > 0) 
		{
			lastTime = Time.fixedTime;
			currentIndex = (currentIndex + 1) % currentSpriteSheet.Length;

			if (!facingRight)
			{
				transform.localScale = invertDirection;
			}
			else
			{
				transform.localScale = originalDirection;
			}

			spriteRenderer.sprite = currentSpriteSheet[currentIndex];
		}

	}

	public enum ActionAnimationType
	{
		Idle, 
		Move, 
		Hit, 
		Explode, 
		Dismiss
	}
}
