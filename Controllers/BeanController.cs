﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeanController : MonoBehaviour 
{
    [SerializeField]
    private GameObject BeanStart;

    [SerializeField]
    private GameObject BeanBody;

    [SerializeField]
    private GameObject BeanHead;

    [SerializeField]
    private Sprite[] startSpriteSheet;

    [SerializeField]
    private Sprite[] bodySpriteSheet;

    [SerializeField]
    private Sprite[] headSpriteSheet;

    public float interval;

    private SpriteRenderer startSpriteRenderer;
    private SpriteRenderer bodySpriteRenderer;
    private SpriteRenderer headSpriteRenderer;

    private Vector3 originalDirection;
    private Vector3 invertDirection;

    private float lastTime;
    private int currentIndex;
    private bool facingRight;

    private Vector3 scaleFactor;
    private float counterScaleFactor;
    private float headSpeedFactor;
    private Vector3 dismissFactor;
    private Vector3 startPosition;
    private Vector3 currentPosition;

    private float speed;
    private float width;

    private Vector3 bodyOffSet;
    private Vector3 headSpeedOffSet;
    private Vector3 headSizeOffSet;

    private bool isStreaming;
    private bool hasStarted;
    private bool isDismissing;

	// Use this for initialization
	void Start ()
    {    
        ShotBean();
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (hasStarted)
        {
            UpdateSprites();
            UpdateBodyMoviment();

            if (isDismissing)
            {
                UpdateDismiss();
            }
        }
	}

    internal void ShotBean ()
    {
        Debug.Log("Shot Bean: " + " speed " + speed + " width " + width);
        
        currentIndex = 0;
        lastTime = 0;

        startPosition = this.transform.position;

        startSpriteRenderer = BeanStart.GetComponentInChildren<SpriteRenderer>(); 
        bodySpriteRenderer = BeanBody.GetComponentInChildren<SpriteRenderer>(); 
        headSpriteRenderer = BeanHead.GetComponentInChildren<SpriteRenderer>(); 

        scaleFactor = new Vector3(0.01f, 0, 0);
        dismissFactor = new Vector3(0, 0.03f, 0);
        
        hasStarted = true;

        SetUpSpeed(speed);  
        SetUpSize(width);

    }

    internal void SetUpSpeed (float speed)
    {
        this.speed = speed;
        scaleFactor *= speed;
    }

    internal void SetUpSize (float width)
    {
        this.transform.localScale *= width;

        originalDirection = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
        invertDirection = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    internal void UpdateBodyMoviment ()
    {
        BeanBody.transform.localScale += scaleFactor; 
    }

    internal void Dismiss ()
    {
        isDismissing = true;
    }

    internal void UpdateDismiss ()
    {
        if (BeanStart.transform.localScale.y >= 0f)
            BeanStart.transform.localScale -= dismissFactor; 

        if (BeanBody.transform.localScale.y >= 0f)
            BeanBody.transform.localScale -= dismissFactor; 

        if (BeanHead.transform.localScale.y >= 0f)
            BeanHead.transform.localScale -= dismissFactor * 0.1f;        

        if (BeanStart.transform.localScale.y < 0f)// &&
            //BeanBody.transform.localScale.y < 0f &&
            //BeanHead.transform.localScale.y < 0f)
        {
            Destroy(this.gameObject);
        }
    }

    internal void UpdateSprites () 
    {
        if (Time.fixedTime - lastTime > interval) 
        {
            lastTime = Time.fixedTime;
            currentIndex = (currentIndex + 1) % startSpriteSheet.Length;

            if (!facingRight)
            {
                transform.localScale = invertDirection;
            }
            else
            {
                transform.localScale = originalDirection;
            }

            startSpriteRenderer.sprite = startSpriteSheet[currentIndex];
            bodySpriteRenderer.sprite = bodySpriteSheet[currentIndex];
            headSpriteRenderer.sprite = headSpriteSheet[currentIndex];
        }
    }
}
