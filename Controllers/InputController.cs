using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Commands;

namespace Inputs
{
	public class InputController : MonoBehaviour
	{
		#region Atributos Internos
		private bool rightPressed;
		private bool leftPressed;
		private bool upPressed;
		private bool downPressed;
        private bool holdingAttack;
        private bool holdingJump;
		private bool isMoving;

		private float holdActionTimer;
		private float holdMovimentTimer;
		private float holdActionTimerMax;
		private float holdMovimentTimerMax;
		private CommandType currentCommand;

		private CommandType lastActionKey;
		private CommandType lastMovimentKey;
		private CommandType currentActionKey;
		private CommandType currentMovimentKey;

		private CommandType currentHoldActionKey;
		private CommandType currentHoldMovimentKey;
		
		private bool IsMovimentInput { get {return (upPressed || rightPressed || downPressed || leftPressed);}}
		#endregion
		
		#region MonoBehaviours
		/**
		 * Metodo padr�o do "MonoBehaviour" do Unity3D, 
		 * ele � executado sempre que o GameObject que tenha esse script � Instanciado.
		 */
		void Start ()
		{
			rightPressed = false;
			leftPressed = false;	
			upPressed = false;	
			downPressed = false;

			holdActionTimerMax = 0.5f;
			holdMovimentTimerMax = 0.5f;

			holdActionTimer = holdActionTimerMax;
			holdMovimentTimer = holdMovimentTimerMax;

			currentActionKey = CommandType.NONE;
			lastActionKey = CommandType.NONE;
			currentMovimentKey = CommandType.STOP;
			lastMovimentKey = CommandType.STOP;

			currentCommand = CommandType.NONE;

			currentHoldActionKey = CommandType.NONE;
			currentHoldMovimentKey = CommandType.STOP;
		}

		void Update()
		{
			VerifyActionKeys ();
            VerifyMovimentKeys ();
            VerifyJumpKeys();

			VerifyHoldingActionKey ();
			//VerifyHoldingMovimentKey ();
		}
		#endregion

		#region Metodos Internos
		/**
		 * Verifica e envia ao Hero os comandos de movimento detectados. 
		 * Enviando apenas os comandos "Novos", ou seja, deferentes do ultimo enviado.
		 * Mantendo assim uma movimenta��o constante ate que seja trocada a dire��o.
		 */ 
		private void VerifyMovimentKeys()
		{	
			if (Input.GetKeyUp(KeyCode.RightArrow)) rightPressed = false;
			if (Input.GetKeyUp(KeyCode.LeftArrow)) leftPressed = false;
			if (Input.GetKeyUp(KeyCode.UpArrow)) upPressed = false;
			if (Input.GetKeyUp(KeyCode.DownArrow)) downPressed = false;
			
			if (Input.GetKeyDown (KeyCode.RightArrow)) rightPressed = true;
			if (Input.GetKeyDown(KeyCode.LeftArrow)) leftPressed = true;
			if (Input.GetKeyDown(KeyCode.UpArrow)) upPressed = true;
			if (Input.GetKeyDown(KeyCode.DownArrow)) downPressed = true;	

			//Debug.Log ("ANY " + Input.anyKey + " up:" + upPressed + " down:" + downPressed + " left:" + leftPressed + " right:" + rightPressed);
			//Debug.Log ("STOP MOVIMENT : " + !(upPressed || downPressed || leftPressed || rightPressed));

            if (!(upPressed || downPressed || leftPressed || rightPressed) )//&& isMoving) 
			{	
				isMoving = false;
				AddMovimentKey (CommandType.STOP);
			}

			if (leftPressed)
			{
				if (upPressed) {AddMovimentKey (CommandType.KEY_BACKWARD_UP);}
				if (downPressed) {AddMovimentKey (CommandType.KEY_DOWN_BACKWARD);}
				if (!upPressed && !downPressed) {AddMovimentKey (CommandType.KEY_BACKWARD);}	
			}

			if (rightPressed)
			{
				if (upPressed) {AddMovimentKey (CommandType.KEY_UP_FORWARD);}
				if (downPressed) {AddMovimentKey (CommandType.KEY_FORWARD_DOWN);}
				if (!upPressed && !downPressed)	{AddMovimentKey (CommandType.KEY_FORWARD);}
			}

			if (!rightPressed && !leftPressed)
			{
                if (upPressed) {AddMovimentKey (CommandType.KEY_UP);}
				if (downPressed) {AddMovimentKey (CommandType.KEY_DOWN);}
			}
		}	

		private void AddMovimentKey(CommandType addedKey)
		{
			//Debug.Log ("Current Command: " + currentMovimentKey + " added Moviment Command: " + addedKey);

			if (currentMovimentKey != addedKey) 
            { 
                holdMovimentTimer = holdMovimentTimerMax;
                //Debug.Log ("Add Moviment Key: " + addedKey + " with hold MovimentTimer : " + holdMovimentTimer);

				lastMovimentKey = currentMovimentKey;
				currentMovimentKey = addedKey;
				currentCommand = addedKey;
               
				if (currentCommand != CommandType.NONE) 
				{
					if (isMoving) 
					{
						AddHoldingMovimentKey (addedKey);
					} 
					else 
					{
						CommandManager.AddCommand (addedKey);
					}
				}
			}
			else
			{
				VerifyHoldingMovimentKey(addedKey);
			}
		}		

		private void VerifyActionKeys()
		{
			if (Input.GetKeyDown (KeyCode.X)) 
            {
				//Debug.Log ("Press KeyCode.X");
                AddActionKey (CommandType.KEY_ATTACK);
                lastActionKey = CommandType.KEY_ATTACK;
			}

			if (Input.GetKeyUp(KeyCode.X))
			{
				if (holdingAttack)
				{
                    //Debug.Log ("Release Holding KeyCode.X");
					holdingAttack = false;
					AddActionKey (CommandType.RELEASE_KEY_ATTACK);
				}
				else 
				{
                    //Debug.Log ("Release KeyCode.X");
                    AddActionKey (CommandType.RELEASE_KEY_ATTACK);
                    AddActionKey (CommandType.NONE);
				}
			}
		}

        private void VerifyJumpKeys()
        {
            if (Input.GetKeyDown (KeyCode.Z)) 
            {
                //Debug.Log ("Press KeyCode.Z");
                AddActionKey (CommandType.KEY_JUMP);
                lastActionKey = CommandType.KEY_JUMP;
            }

            if (Input.GetKeyUp(KeyCode.Z))
            {
                if (holdingJump)
                {
                    //Debug.Log ("Release Holding KeyCode.Z");
                    holdingJump = false;
                    AddActionKey (CommandType.RELEASE_KEY_JUMP);
                }
                else 
                {
                    //Debug.Log ("Release KeyCode.Z");
                    AddActionKey (CommandType.RELEASE_KEY_JUMP);
                    AddActionKey (CommandType.NONE);
                }
            }
        }

        private void AddActionKey(CommandType addedKey)
        {
            //Debug.Log ("Current Command: " + currentActionKey + " added Action Command: " + addedKey);

            if (currentActionKey != addedKey) 
            {               
                //Debug.Log ("Add Action Key: " + addedKey);

                lastActionKey = currentActionKey;
                currentActionKey = addedKey;
                currentCommand = addedKey;

                if (addedKey != CommandType.STOP) 
                {                   
                    CommandManager.AddCommand (addedKey);
                }
            }
            else
            {
                // ?
            }
        }

		private void VerifyHoldingMovimentKey(CommandType addedKey)
		{
            //Debug.Log ("Verify Holding Moviment Key " + addedKey + " hold Moviment Timer:" + holdMovimentTimer);

            if (addedKey != CommandType.STOP)
            {
                //Debug.Log ("Verify Holding Moviment Key " + addedKey + " hold Moviment Timer:" + holdMovimentTimer);

                holdMovimentTimer -= 0.1f;

                if (holdMovimentTimer < 0)
                {
                    AddHoldingMovimentKey(addedKey);
                }
            }
		}

		private void VerifyHoldingActionKey()
		{
            //Debug.Log ("currentActionKey:" + currentActionKey + " lastActionKey:" + lastActionKey);

			if (currentActionKey == lastActionKey && currentActionKey != CommandType.NONE) 
			{				
				//Debug.Log ("currentActionKey:" + currentActionKey + " lastActionKey:" + lastActionKey + " holdActionTimer:" + holdActionTimer);
				holdActionTimer -= 0.1f;

				if (holdActionTimer < 0) {
					switch (currentActionKey) {
					case CommandType.KEY_ATTACK:
						holdingAttack = true;
						AddActionKey (CommandType.HOLD_KEY_ATTACK);
						break;	
					}
				}
			}
			else
			{
				holdActionTimer = holdActionTimerMax;
				//AddActionKey (currentActionKey);
			}
		}

		private void AddHoldingMovimentKey(CommandType addedKey)
		{
			isMoving = true;
			
			switch (addedKey) 
            {
			    case CommandType.KEY_UP:
                    CommandManager.AddCommand (CommandType.HOLD_KEY_UP);
				break;

			    case CommandType.KEY_UP_FORWARD:
                    CommandManager.AddCommand (CommandType.HOLD_KEY_UP_FORWARD);
				break;

			    case CommandType.KEY_FORWARD:
                    CommandManager.AddCommand (CommandType.HOLD_KEY_FORWARD);
				break;

			    case CommandType.KEY_FORWARD_DOWN:
                    CommandManager.AddCommand (CommandType.HOLD_KEY_FORWARD_DOWN);
				break;

			    case CommandType.KEY_DOWN:
                    CommandManager.AddCommand (CommandType.HOLD_KEY_DOWN);
				break;

			    case CommandType.KEY_DOWN_BACKWARD:
                    CommandManager.AddCommand (CommandType.HOLD_KEY_DOWN_BACKWARD);
				break;

			    case CommandType.KEY_BACKWARD:
                    CommandManager.AddCommand (CommandType.HOLD_KEY_BACKWARD);
				break;

			    case CommandType.KEY_BACKWARD_UP:
                    CommandManager.AddCommand (CommandType.HOLD_KEY_BACKWARD_UP);
				break;	
			}
		}

		/**
		 * Verifica e cadastra os inputs de teclado nos CommandManager.
		 */ 
		private void VerifyCommands()
		{
			
		}
		#endregion
	}	
}