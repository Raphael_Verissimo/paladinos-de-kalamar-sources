using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Skills;
using Sprites;
using Actions;
using Commands;

namespace Characters
{
    public class RyuController : CharacterController
	{		
		void Start ()
		{	
			timeToPush = 0f;
			timeToResume = 0f;
			originalY = 1.5f;
			isFacingRight = true;
			movimentSpeedFactor = 1.5f;
			runningSpeedFactor = 2.5f;
			timeToPushCoolDown = 0.1f;
			timeToResumeCoolDown = 0.2f;

            comboIndex = -1;
            startCombo = false;
            comboTimmer = 0.3f;
            comboCooldown = 0.5f;

            body = GetComponent<Rigidbody> ();

			spriteController = GetComponentInChildren<SpriteController>();

			//characterAnimator = GetComponent<Animator> ();
			//this.transform.LookAt (Camera.main.transform);

            Idle ();

            lockTimes.Add (SkillType.RYU_DASH, 1f);    
            maxCoolDowns.Add (SkillType.RYU_DASH, 1f); 
            availableSkills.Add(SkillType.RYU_DASH);

            commandAllias.Add (CommandChainType.RUN_FORWARD, SkillType.RYU_DASH);
            commandDirections.Add (CommandChainType.RUN_FORWARD, true);

            commandAllias.Add (CommandChainType.RUN_BACKWARD, SkillType.RYU_DASH); 
            commandDirections.Add (CommandChainType.RUN_BACKWARD, false);

            lockTimes.Add (SkillType.JUMP, 1f);    
            maxCoolDowns.Add (SkillType.JUMP, 0.1f); 
            availableSkills.Add(SkillType.JUMP);
            commandAllias.Add (CommandChainType.JUMP, SkillType.JUMP);

            attackSequence.Add(SkillType.JAB);
            attackSequence.Add(SkillType.JAB);
            attackSequence.Add(SkillType.DIRETO);
            attackSequence.Add(SkillType.CANELADA);
            attackSequence.Add(SkillType.FORCE_ATTACK);

			commandAllias.Add (CommandChainType.NORMAL_ATTACK, SkillType.JAB);

			lockTimes.Add (SkillType.JAB, 0.15f);	
			maxCoolDowns.Add (SkillType.JAB, 0.2f);	
			availableSkills.Add(SkillType.JAB);

            lockTimes.Add (SkillType.DIRETO, 0.4f);    
            maxCoolDowns.Add (SkillType.DIRETO, 0.5f);   
            availableSkills.Add(SkillType.DIRETO);

            lockTimes.Add (SkillType.CANELADA, 0.8f);    
            maxCoolDowns.Add (SkillType.CANELADA, 1f);   
            availableSkills.Add(SkillType.CANELADA);

            lockTimes.Add (SkillType.FORCE_ATTACK, 1.5f);	
            maxCoolDowns.Add (SkillType.FORCE_ATTACK, 3f);	
            availableSkills.Add (SkillType.FORCE_ATTACK);

            lockTimes.Add (SkillType.RYU_DASH_ATTACK, 2f);  
            maxCoolDowns.Add (SkillType.RYU_DASH_ATTACK, 2f); 
            availableSkills.Add(SkillType.RYU_DASH_ATTACK);

            lockTimes.Add (SkillType.VOADORA, 0.2f);  
            maxCoolDowns.Add (SkillType.VOADORA, 0.2f); 
            availableSkills.Add(SkillType.VOADORA);
			
			commandAllias.Add (CommandChainType.DOWN_FORWARD_ATTACK, SkillType.HADOUKEN);
			commandDirections.Add (CommandChainType.DOWN_FORWARD_ATTACK, true);

			commandAllias.Add (CommandChainType.DOWN_BACKWARD_ATTACK, SkillType.HADOUKEN);	
			commandDirections.Add (CommandChainType.DOWN_BACKWARD_ATTACK, false);
				
			lockTimes.Add (SkillType.HADOUKEN, 1.5f);	
			maxCoolDowns.Add (SkillType.HADOUKEN, 5f);	
			availableSkills.Add(SkillType.HADOUKEN);
			
			commandAllias.Add (CommandChainType.FORWARD_DOWN_FORWARD_ATTACK, SkillType.SHORYUKEN);
			commandDirections.Add (CommandChainType.FORWARD_DOWN_FORWARD_ATTACK, true);

			commandAllias.Add (CommandChainType.BACK_DOWN_BACKWARD_ATTACK, SkillType.SHORYUKEN);	
			commandDirections.Add (CommandChainType.BACK_DOWN_BACKWARD_ATTACK, false);

			lockTimes.Add (SkillType.SHORYUKEN, 1.8f);	
			maxCoolDowns.Add (SkillType.SHORYUKEN, 5f);	
			availableSkills.Add(SkillType.SHORYUKEN);

			commandAllias.Add (CommandChainType.FORWARD_DOWN_BACK_ATTACK, SkillType.ATTACK_DAS_CORUJA);
			commandDirections.Add (CommandChainType.FORWARD_DOWN_BACK_ATTACK, true);

			commandAllias.Add (CommandChainType.BACK_DOWN_FORWARD_ATTACK, SkillType.ATTACK_DAS_CORUJA);	
			commandDirections.Add (CommandChainType.BACK_DOWN_FORWARD_ATTACK, false);

			lockTimes.Add (SkillType.ATTACK_DAS_CORUJA, 2f);	
			maxCoolDowns.Add (SkillType.ATTACK_DAS_CORUJA, 5f);	
			availableSkills.Add(SkillType.ATTACK_DAS_CORUJA);

			commandAllias.Add (CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK, SkillType.SHINKU_HADOUKEN);
			commandDirections.Add (CommandChainType.DOWN_FORWARD_DOWN_FORWARD_ATTACK, true);

			commandAllias.Add (CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK, SkillType.SHINKU_HADOUKEN);	
			commandDirections.Add (CommandChainType.DOWN_BACK_DOWN_BACKWARD_ATTACK, false);

            lockTimes.Add (SkillType.SHINKU_HADOUKEN, 100f);	
			maxCoolDowns.Add (SkillType.SHINKU_HADOUKEN, 10f);	
			availableSkills.Add(SkillType.SHINKU_HADOUKEN);
		}	

		#region Ryu Methods
        override internal void MoveTo (Vector3 origin, Vector3 direction)
		{	
			currentState = CharacterState.WALKING;

            Debug.Log("Move Ryu: " + direction);
            currentrVelocity = 1.7f;

            if (!isRunning)
            {            
                isMoving = true;
                currentDirection = direction.normalized * movimentSpeedFactor;

                if (spriteController)
                {
                    if (direction.x > 0)
                        isFacingRight = true;

                    if (direction.x < 0)
                        isFacingRight = false;

                    spriteController.PlayWalk(isFacingRight);
                } 
            }
		}

        override internal void DashTo(Vector3 origin, Vector3 direction)
		{		
			currentState = CharacterState.DASHING;

            //Debug.Log("Run Goku: " + direction);
            isRunning = true;
            isRunningAttack = false;

            if (spriteController)
            {
                if (direction.x > 0 || direction.z > 0)
                    isFacingRight = true;

                if (direction.x < 0 || direction.z < 0)
                    isFacingRight = false;

                spriteController.PlayRun(isFacingRight);
            } 

            if (isFacingRight)
                PlaySkill(CommandChainType.RUN_FORWARD);
            else
                PlaySkill(CommandChainType.RUN_BACKWARD);
		}

        override internal void PlaySkill(CommandChainType command)
		{    
			//currentState = CharacterState.HITED;

            availableSkills.Remove(commandAllias[command]);
            currentrVelocity = 0;

            //Debug.Log("PlaySkill:" + commandAllias[command]);
            if (commandDirections.ContainsKey (command)) 
            {
                if (commandDirections [command]) 
                {
                    currentDirection = new Vector3 (1, 0, 0);
                    isFacingRight = true;
                } 
                else 
                {
                    currentDirection = new Vector3 (-1, 0, 0);
                    isFacingRight = false;
                }
            } 
            //else
            //{
                //if (isFacingRight) 
                //{
                //    currentDirection = new Vector3 (1, 0, 0);
                //} 
                //else 
                //{
                //    currentDirection = new Vector3 (-1, 0, 0);
                //} 
            //}

            if (command == CommandChainType.NORMAL_ATTACK)
            {
                if (isRunning || isJumping)
                {
                    if (isRunning)
                    {
                        //RyuDash dashAction = this.gameObject.GetComponentInChildren<RyuDash>();

                        //if (dashAction)
                        //{
                        //    dashAction.hasAttacked = true;
                            SkillManager.UseSkill(SkillType.RYU_DASH_ATTACK, this);
                            StartCoroutine(ResetCooldown(SkillType.RYU_DASH_ATTACK));
                            StartCoroutine(LockCharacter(lockTimes[SkillType.RYU_DASH_ATTACK]));
                        //}
                    }
                    else
                    {
                        Jump jumpAction = this.gameObject.GetComponentInChildren<Jump>();

                        if (jumpAction)
                        {
                            //jumpAction.hasAttacked = true;
                            SkillManager.UseSkill(SkillType.VOADORA, this);
                            StartCoroutine(ResetCooldown(SkillType.VOADORA));
                            StartCoroutine(LockCharacter(lockTimes[SkillType.VOADORA]));
                        }
                    }
                }
                else
                {
                    startCombo = true;
                    comboIndex = (comboIndex + 1) % attackSequence.Count;

                    //Debug.Log ("Attack:" + comboIndex + " > " + attackSequence[comboIndex]);

                    commandAllias[CommandChainType.NORMAL_ATTACK] = attackSequence[comboIndex];
                    SkillManager.UseSkill (commandAllias[command], this);
                    StartCoroutine (ResetCooldown(commandAllias[command]));
                    StartCoroutine (LockCharacter(lockTimes [commandAllias[command]]));
                }
            }
            else
            {
                SkillManager.UseSkill (commandAllias[command], this);
                StartCoroutine (ResetCooldown(commandAllias[command]));
                StartCoroutine (LockCharacter(lockTimes [commandAllias[command]]));

                if (commandAllias[command] == SkillType.SHINKU_HADOUKEN) 
                {
                    StartCoroutine (RecursiveChargeSpecial(2f));
                }
            }
		}

        override internal void PlaySkillAnimation(SkillType type)
		{
            //Debug.Log("PlaySkillAnimation: " + type);
            currentrVelocity = 0;
             
            switch (type) 
			{
				case SkillType.JAB:
					PlayJab ();
				break;

                case SkillType.DIRETO:
                    PlayDireto ();
                break;

                case SkillType.CANELADA:
                    PlayCanelada ();
                break;

                case SkillType.FORCE_ATTACK:
                    PlayStrongKick ();
				break;

                case SkillType.DASH_ATTACK:
                    PlayDashAttack ();
                break;

                case SkillType.VOADORA:
                    PlayVoadora ();
                break;

				case SkillType.HADOUKEN:
					PlayHadouken ();
				break;

				case SkillType.SHORYUKEN:
					PlayShoryuken ();
				break;

				case SkillType.ATTACK_DAS_CORUJA:
					PlayAtaqueDasCoruja ();
				break;

				case SkillType.SHINKU_HADOUKEN:
					PlayStartSpecial ();
				break;
			}      
		}

        override internal void HoldAttack ()
        {
            //Debug.Log("HoldAttack Attack !!!");
            isHoldingAttack = true;
        }

        override internal void ReleaseAttack ()
        {
            //Debug.Log("Release Attack !!!");
            isHoldingAttack = false;

            if (isChargingSpecial) 
            {
                ShotSpecial ();
            }
        }
        #endregion       

        private void PlayJab()
		{
			if (spriteController)
			{
				spriteController.PlayAttack1(isFacingRight);
			} 
		}

        private void PlayDireto()
        {
            if (spriteController)
            {
                spriteController.PlayAttack2(isFacingRight);
            } 
        }

        private void PlayCanelada()
        {
            if (spriteController)
            {
                spriteController.PlayAttack4(isFacingRight);
            } 
        }

        private void PlayStrongKick()
		{
			if (spriteController)
			{
				spriteController.PlayForceAttack(isFacingRight);
			} 
		}

        private void PlayDashAttack()
        {
            if (spriteController)
            {
                spriteController.PlayDashAttack(isFacingRight);
            } 
        }

        private void PlayVoadora()
        {
            if (spriteController)
            {
                spriteController.PlayVoadora(isFacingRight);
            } 
        }

        private void PlayHadouken()
		{
			if (spriteController)
			{
				spriteController.PlaySkill1(isFacingRight);
			} 
		}

        private void PlayShoryuken()
		{
			if (spriteController)
			{
				spriteController.PlaySkill2(isFacingRight);
			} 
		}

        private void PlayAtaqueDasCoruja()
		{
			if (spriteController)
			{
				spriteController.PlaySkill3(isFacingRight);
			} 
		}

        private void PlayStartSpecial()
		{
			if (spriteController)
			{
				//spriteController.PlaySpecial1(isFacingRight);
				IList<AnimationType> actions = new List<AnimationType>();

				actions.Add(AnimationType.Special1);
				actions.Add(AnimationType.Special2);

				spriteController.PlaySequence(actions, isFacingRight);
                //spriteController.PlaySpecial1(isFacingRight);

                effectsSources.clip = SpecialStartClip;
                effectsSources.Play();

                StartCoroutine(DelayedVoice(ChargingSpecialVoice, 1.8f));
			} 
		}

        private void ShotSpecial()
		{
            isChargingSpecial = false;
            StartCoroutine (LockCharacter(0.3f));

			GetComponentInChildren<ShinkuHadouken> ().OnShot ();

			PlayShotSpecial ();
		}

        private void PlayShotSpecial()
		{
			if (spriteController)
			{
                //spriteController.PlaySpecial1(isFacingRight);
                IList<AnimationType> actions = new List<AnimationType>();

                actions.Add(AnimationType.Special3);
                actions.Add(AnimationType.Idle);

                spriteController.PlaySequence(actions, isFacingRight);

                voiceSources.clip = ShotSpecialVoice;
                voiceSources.Play();
			} 
		}

        IEnumerator RecursiveChargeSpecial(float delay)
        {
            isChargingSpecial = true;

            yield return new WaitForSeconds (delay);

            if (isHoldingAttack)
            {
                if (spriteController && isChargingSpecial) 
                {
                    spriteController.PlaySpecial2 (isFacingRight);

                    GetComponentInChildren<ShinkuHadouken> ().OnCharge ();

                    if (isChargingSpecial)
                        StartCoroutine (RecursiveChargeSpecial (0.9f));
                } 
            }
        }

        IEnumerator ResetCooldown(SkillType skill)
        {
            //Debug.Log ("SkillType:" + skill + " cooldown:" + maxCoolDowns [skill]);

            yield return new WaitForSeconds (maxCoolDowns[skill]);

            availableSkills.Add(skill);

            //Debug.Log ("SkillType:" + skill + " Ready");
        }

        IEnumerator LockCharacter(float lockTime)
        {
            
           // Debug.Log ("Lock Character:" + lockTime);
            isLocked = true;

            yield return new WaitForSeconds (lockTime);

           // Debug.Log ("Unlock Character");
            isLocked = false;
            //Debug.Log ("Resume Command:" + lastLockedCommand);
            //ApplyCommand(lastLockedCommand, transform.position);
            // Resumir o moviemnto do corrente

            //Debug.Log ("SkillType:" + skill + " Ready");
        }

        IEnumerator DelayedVoice(AudioClip clip, float delay)
        {
            yield return new WaitForSeconds (delay);

            voiceSources.clip = clip;
            voiceSources.Play();
        }
	}
}