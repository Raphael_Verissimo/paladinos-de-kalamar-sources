﻿using UnityEngine;
using System.Collections;

public class FraseScreen : MonoBehaviour 
{
	public int StartLoadingTime;
	private bool loadingStarted;
	public Menu NextMenu;
	private Menu currentMenu;

	public void Awake()
	{
		loadingStarted = false;
		currentMenu = GetComponent<Menu>();
	}

	public void Update()
	{
		if (currentMenu.IsOpen) 
		{
			loadingStarted = true;
		}

		if (loadingStarted) 
		{
			if(StartLoadingTime >= 0)
			{
				StartLoadingTime--;
			}
		}

		if(StartLoadingTime < 0)
		{
			LoadNextScreen ();
		}
	}

	public void LoadNextScreen()
	{
		loadingStarted = false;
		DestroyObject(this.gameObject);
		NextMenu.IsOpen = true;
	}
}
