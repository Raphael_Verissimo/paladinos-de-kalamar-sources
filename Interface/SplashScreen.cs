﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour 
{
	private Animator animator;
	private CanvasGroup canvasGroup;

	public int Duration;	
	public Menu NextMenu;

	private int ExpirationTime;

	public bool IsFadinOut
	{
		get {return animator.GetBool("IsFadinOut");}
		set {animator.SetBool("IsFadinOut", value);}
	}

	public void Awake()
	{
		animator = GetComponent<Animator>();
		canvasGroup = GetComponent<CanvasGroup>();
		
		RectTransform menuRect = GetComponent<RectTransform>();
		menuRect.offsetMax = menuRect.offsetMin = Vector2.zero;

		IsFadinOut = false;
	}

	public void Update()
	{
		if(Duration > 0)
		{
			Duration--;
		}
		else
		{
			IsFadinOut = true;
			ExpirationTime--;
		}

		if(ExpirationTime < 0 )
		{
			OpenStartMenu();
		}
	}

	private void OpenStartMenu()
	{
		NextMenu.IsOpen = true;
		DestroyObject(this.gameObject);
	}
}
