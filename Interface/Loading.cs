﻿using UnityEngine;
using System.Collections;

public class Loading : MonoBehaviour
{
	private Animator animator;
	private CanvasGroup canvasGroup;
	
	public bool IsLoading
	{
		get 
		{
			return animator.GetBool("IsLoading");
		}
		
		set 
		{			
			animator.SetBool("IsLoading", value);
		}
	}
	
	public void Awake()
	{
		animator = GetComponent<Animator>();
		canvasGroup = GetComponent<CanvasGroup>();
		
		RectTransform menuRect = GetComponent<RectTransform>();
		menuRect.offsetMax = menuRect.offsetMin = Vector2.zero;
	}
}
