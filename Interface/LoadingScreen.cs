﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour 
{
	public int StartLoadingTime;
	private UnityEngine.UI.Text progressText;
	private bool loadingStarted;
	private Menu currentMenu;
	private AsyncOperation async = null; // When assigned, load is in progress.
	private string nextScene;

	/*
	void Awake()
	{
		progressText = GameObject.Find("Progresso").GetComponent<UnityEngine.UI.Text>();
		progressText.text = "0";
		loadingStarted = false;
		currentMenu = GetComponent<Menu>();
	}
	*/

	public void LoadScene(string scene)
	{
		progressText = GameObject.Find("Progresso").GetComponent<UnityEngine.UI.Text>();
		progressText.text = "0";
		loadingStarted = false;
		currentMenu = GetComponent<Menu>();
		nextScene = scene;
	}

	void Update()
	{
		if (currentMenu.IsOpen) 
		{
			loadingStarted = true;
		}

		if (loadingStarted) 
		{			
			if (StartLoadingTime < 0)
			{
				StartCoroutine (StartLoading ());
				loadingStarted = true;
			}
			else
			{
				StartLoadingTime--;
			}
			
			if(async != null)
			{
				progressText.text = (100 * async.progress).ToString();
			}
		}
	}

	IEnumerator StartLoading() 
	{
		async = Application.LoadLevelAsync(nextScene);
		yield return async;
	}
}
