﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Commands;
using Characters;

namespace Battle
{
    public class BattleManager
    {
        private static BattleController _controller;

        public static BattleController Controller
        {
            get
            {
                if (_controller == null)
                    _controller = GameObject.Find("BattleController").GetComponent<BattleController>();

                return _controller;
            }
        }

        public static void AddCommand(float time, HeroController hero, CommandChainType command) { Controller.AddCommand(time, hero, command); }
    }
}
