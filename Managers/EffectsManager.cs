using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Skills;
using Actions;
using Commands;
using Characters;

namespace Effects
{
    public class EffectsManager
    {
        private static EffectsController _controller;

        public static EffectsController Controller
    	{
    		get
    		{
    			if (_controller == null)
                    _controller = GameObject.Find("EffectsController").GetComponent<EffectsController>();

    			return _controller;
    		}
    	}

        public static void PlayEffect(EffectType effect, Vector3 position)
    	{
            Controller.PlayEffect (effect, position);
    	}
    }
}