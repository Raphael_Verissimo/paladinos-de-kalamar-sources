﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour 
{
	public Menu CurrentMenu;

	public void Start()
	{
		//ShowMenu (CurrentMenu);
	}

	public void ChangeMenu(Menu menu)
	{
		if (CurrentMenu != null)
			CurrentMenu.IsOpen = false;

		CurrentMenu = menu;
		CurrentMenu.IsOpen = true;
	}

	public void OpenMenu(Menu menu)
	{
		if (CurrentMenu != null && menu != null)
		{
			CurrentMenu = menu;
			CurrentMenu.IsOpen = true;
		}
	}

	public void CloseMenu(Menu menu)
	{
		if (CurrentMenu != null && menu != null)
		{
			CurrentMenu = menu;
			CurrentMenu.IsOpen = false;
		}
	}
}
