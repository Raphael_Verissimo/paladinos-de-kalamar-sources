﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;

namespace Actions
{
	public class ActionManager
	{
		private static ActionController _controller;

		public static ActionController Controller
		{
			get
			{
				if (_controller == null)
					_controller = GameObject.Find("ActionController").GetComponent<ActionController>();

				return _controller;
			}
		}

		public static GameObject UseAction(ActionType type, Characters.CharacterController caster) { return Controller.UseAction (type, caster); }
		public static GameObject UseAction(ActionType type, Characters.HeroController caster) { return Controller.UseAction (type, caster); }

	}

	public enum ActionType
	{
		NONE, 

		MOVE_UP,  
		MOVE_UP_FORWARD,
		MOVE_FORWARD,
		MOVE_FORWARD_DOWN,
		MOVE_DOWN,
		MOVE_DOWN_BACKWARD,
		MOVE_BACKWARD,	
		MOVE_BACKWARD_UP,

        DASH,
        RYU_DASH,
        DASH_ATTACK,
        RYU_DASH_ATTACK,

		RUN_UP,
		RUN_UP_FORWARD,
		RUN_FORWARD,
		RUN_FORWARD_DOWN,
		RUN_DOWN,
		RUN_DOWN_BACKWARD,
		RUN_BACKWARD,	
		RUN_BACKWARD_UP,

        JUMP,
        VOADORA,

		// Basic Attacks (Basic and 2 command chains)
        NORMAL_ATTACK,
        PUNCH_ATTACK,
        STRONG_PUNCH_ATTACK,
        DIRECT_ATTACK,
        KICK_ATTACK,  
        STRONG_KICK_ATTACK,
        CANELADA_ATTACK,
		FORCE_ATTACK,
		DIRECTIONAL_ATTACK,
		RUNNING_ATTACK,

        // RYU SKILLS
		HADOUKEN,
		SHORYUKEN,
		ATTACK_DAS_CORUJA,
		SHINKU_HADOUKEN,

		EXPLOSION,

        //GOKU SKILLS
        KIBLAST,
        COUNTER_TELEPORT,
        TRIPLE_STRIKE,
        KAME_HAME_HA
	}
}