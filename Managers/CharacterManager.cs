﻿using UnityEngine;
using System.Collections;

namespace Characters
{	
	public class CharacterManager 
	{
		private static CharacterController GetController(GameObject characterGO)
		{
			CharacterController returnedController = null;

			if (characterGO != null)
			{
				returnedController = characterGO.GetComponent<CharacterController> ();
			}

			return returnedController;
		}

		public static HeroController GetHeroController(GameObject characterGO)
		{
			HeroController returnedController = null;

			if (characterGO != null)
			{
				returnedController = characterGO.GetComponent<HeroController> ();
			}

			return returnedController;
		}

		public static CharacterController CreateCharacter(CharacterType type, bool isMainCharacter)
		{
			switch(type)
			{
				case CharacterType.RYU:
					GameObject characterObject = ObjectManager.CreateCharacter ("Character/Ryu", Vector3.zero);
					CharacterController characterController = characterObject.GetComponent<CharacterController> ();
				return characterController;

				default:
				return null;
			}
		}
	}

	public enum CharacterType
	{
		RYU,
		GOKU
	}

	public enum CharacterState
	{
        WAITING_FOR_IDLE,
		IDLE, 
		WALKING,
		DASHING,
        DASHATTACK,
        JUMPING,
        JUMPATTACK,
        ATTACKING,
		ACTING,
		HITED,
		PUSHED,
		FALLING,
		HITING_FLOOR,
		LAY_DOWN,
		GETTING_UP
	}
}
