using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Skills;
using Battle;
using Actions;
using Commands;
using Characters;

public class GameManager
{
	private static GameController _controller;

	private static GameController Controller
	{
		get
		{
			if (_controller == null)
				_controller = GameObject.Find("GameController").GetComponent<GameController>();

			return _controller;
		}
	}

	private static GameObject mainPlayerGO;
	private static Characters.HeroController mainPlayer;

	public static float GameTime {get { return Controller.GameTime; } }
	
	public static GameObject MainPlayerGO
	{
		get
		{
			if (mainPlayerGO == null)
				mainPlayerGO = GameObject.FindWithTag("MainPlayer");
			
			return mainPlayerGO;
		}
	}
	
	public static HeroController MainPlayer
	{
		get
		{
			if (mainPlayer == null)
				mainPlayer = MainPlayerGO.GetComponent<Characters.HeroController>();
			
			return mainPlayer;
		}
	}		

	public static void ApplyCommand(CommandChainType commandChain)
	{
        //MainPlayer.ApplyCommand(commandChain, MainPlayerGO.transform.position);
        BattleManager.Controller.AddCommand(Time.fixedTime, MainPlayer, commandChain);
	}

	public static void CreateCharacter(CharacterType type, bool isMainCharacter)
	{
		CharacterManager.CreateCharacter (type, isMainCharacter);
	}
	
	public static void LoadScene(string scene)
	{
		
	}
}