﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using Effects;

namespace Actions
{
    public class BasicAttack : Action
    {
        private Vector3 moviment;

        private StaticShot staticShot;

        void Start()
        {

        }

        private void FixedUpdate()
        {
            if (isActive)
            {
                if (this.caster.State == CharacterState.DASHATTACK)
                {
                    if (this.caster.IsFacingRight)
                        this.caster.Velocity -= desacelerationFactor;
                    else
                        this.caster.Velocity += desacelerationFactor;

                    Debug.Log("Velocidade: " + this.caster.Velocity.magnitude);

                    if (this.caster.Velocity.magnitude < 0.1f)
                    {
                        DestroyAction();
                    }

                    if (((Time.fixedTime - startTime) > maxTime) && this.caster.Velocity.magnitude < 0.1f)
                    {
                        DestroyAction();
                    }
                }
                else
                {
                    Debug.Log("Tempo: " + (Time.fixedTime - startTime) + " Max: " + maxTime);

                    if ((Time.fixedTime - startTime) > maxTime)
                    {
                        DestroyAction();
                    }
                }
            }            
        }

        public void UseAction(HeroController hero, float width, float maxTime, float coolDown, float movimentCoolDown, Vector3 offset, Vector3 moviment, Vector3 pushForce)
        {
            this.isActive = true;

            this.caster = hero;
            this.width = width;
            this.maxTime = maxTime;
            this.coolDown = coolDown;
            this.movimentCoolDown = movimentCoolDown;
            this.startPosition = hero.transform.position;
            this.desacelerationFactor = new Vector3(0.2f, 0, 0);

            this.startTime = Time.fixedTime;
            this.actionDirection = this.caster.Direction;
            this.pushForce = pushForce;

            this.startPosition += new Vector3(offset.x, offset.y, offset.z);

            this.staticShot = (StaticShot)AddActionShoter(ShoterType.STATIC_SHOT, this, OnHit, null);

            this.caster.ActionMoviment(moviment, this.coolDown);
        }

        public void OnHit(HeroController target, GameObject shotGO)
		{
			target.PushCharacter(pushForce, 0.1f);
			shotGO.GetComponent<HitTarget> ().HitCharacter (0.1f);

            Vector3 effectPosition = new Vector3 (shotGO.transform.position.x + (0.4f * actionDirection.x), shotGO.transform.position.y, shotGO.transform.position.z);
            EffectsManager.PlayEffect(EffectType.HIT, effectPosition);
			//target.ApplyDamage (DamageType.PHYSICAL, caster.Model.AttackDamage, EffectType.NONE, 0, 0f); 
		}

        private void DestroyAction()
        {
            if (this.caster.State == CharacterState.JUMPATTACK || this.caster.State == CharacterState.JUMPING)
                this.caster.ReleaseHeroOnAir();
            else
                this.caster.ReleaseHeroOnFloor();

            isActive = false;
			staticShot.Dismiss(0.2f);
        }
    }
}