﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using Effects;

namespace Actions
{
	public class ShinkuHadouken : Action 
	{
		public GameObject spriteGO;
		public SingleShot singleShot;

        public int currentCharge;

        // Use this for initialization
        void Start () 
		{

		}
		
		// Update is called once per frame
		void Update () 
		{
			
		}

		public void UseAction (HeroController caster, float width, float range, float speed, float hitSpeed, int maxHits, float coolDown, float hitCoolDown,
                               Vector3 offset, Vector3 pushForce)
		{	
            Debug.Log ("UseAction Special ");

			this.caster = caster;
			isActive = true;
			
			this.width = width;
            this.range = range;
            this.speed = speed;
            this.maxHits = maxHits;
            this.hitSpeed = hitSpeed;
            this.coolDown = coolDown;
            this.hitCoolDown = hitCoolDown;
            this.pushForce = pushForce;

            this.maxTime = 5f;
            this.currentHits = 0;	
            this.currentCharge = 0;		
			
			this.startTime = Time.fixedTime;
            this.actionDirection = this.caster.Direction;
            this.pushForce = pushForce;

            this.startPosition += new Vector3(offset.x, offset.y, offset.z);

			beforeCastPrefab = "";
			afterCastPrefab = "";
			actionPrefab = "";

			singleShot = (SingleShot)AddActionShoter(ShoterType.SINGLE_SHOT, this, OnHit, null);
			singleShot.transform.parent = this.transform;
			//singleShot.DelayShotMultipleHit ();	
		}

		public void OnHit(HeroController target, GameObject shotGO)
		{
			currentHits++;
            if (currentHits <= maxHits)
            {
				
				target.PushCharacter(this.pushForce, this.hitCoolDown);

				shotGO.GetComponent<HitTarget> ().HitCharacter (this.hitCoolDown);
				caster.ActionHitDelayedResume(this.hitCoolDown);

				Vector3 effectPosition = new Vector3 (shotGO.transform.position.x + (0.4f * actionDirection.x), shotGO.transform.position.y, shotGO.transform.position.z);
				EffectsManager.PlayEffect(EffectType.HIT, effectPosition);
				//caster.MovingActionHitSpeed ();
				//target.ApplyDamage (DamageType.PHYSICAL, caster.Model.AttackDamage, EffectType.NONE, 0, 0f); 
			}			
		}

		public void OnReach(Vector3 position, GameObject shotGO)
		{
			singleShot.Dismiss(0.2f);
		}

		public void OnTime(Vector3 position, GameObject shotGO)
		{
			//Destroy (currentShootGo);
		}

		public void OnCharge()
		{
			currentCharge++;

			Debug.Log ("Charge " + currentCharge + " !!!");

			range += 0.5f;
			width += 0.15f;
			speed += 0.5f;
			hitSpeed = speed/2;
			maxHits += 1;
		}

		public void OnShot()
		{
			Debug.Log ("Shoot Special Level " + this.currentCharge + " | range:" + this.range + "  width:" + this.width + "  speed:" + this.speed + "  maxHits:" + this.maxHits);

			//singleShot.DelayShotMultipleHit ();
		}
	}
}