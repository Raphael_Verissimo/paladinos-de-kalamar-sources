﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;

namespace Actions
{
	public class CounterTeleport : Action 
	{
		public GameObject spriteGO;
		public SingleShot singleShot;
		private float actionSpeed;
		private float hitCooldown;

		// Use this for initialization
		void Start () 
		{

		}
		
		// Update is called once per frame
		void Update () 
		{
			
		}

		public void UseAction (HeroController caster, float coolDown, Vector3 offset)
		{
            isActive = true;
			this.caster = caster;

			this.coolDown = coolDown;		

			beforeCastPrefab = "";
			afterCastPrefab = "";
			actionPrefab = "";

			actionDirection = caster.Direction;

            this.caster.ChangePosition(offset, coolDown);
            this.caster.ReleaseOnFloor(coolDown + 0.5f);
		}

		public void OnHit(Characters.CharacterController target, GameObject shotGO)
		{			            
		}

		public void OnReach(Vector3 position, GameObject shotGO)
		{
		}

		public void OnTime(Vector3 position, GameObject shotGO)
		{
		}
	}
}