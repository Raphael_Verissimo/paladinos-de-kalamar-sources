﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using Effects;

namespace Actions
{
	public class Shoryuken : Action 
	{
		public StaticShot staticShot;

        // Use this for initialization
        void Start () 
		{

		}
		
		// Update is called once per frame
		void Update () 
		{
            if (caster.transform.position.y > 3 && caster.Body.velocity.y < 0)
                DestroyAction();
		}

		public void UseAction (HeroController caster, float width, float speed, float hitSpeed, int maxHits, float coolDown, float hitCoolDown,
                               Vector3 offset, Vector3 moviment, Vector3 pushForce)
		{
            isActive = true;
            this.caster = caster;

            this.width = width;
            this.speed = speed;
            this.maxHits = maxHits;
            this.hitSpeed = hitSpeed;
            this.coolDown = coolDown;
            this.hitCoolDown = hitCoolDown;
            this.moviment = moviment;
            this.pushForce = pushForce;

            currentHits = 0;

			beforeCastPrefab = "";
			afterCastPrefab = "";
			actionPrefab = "";

			actionDirection = caster.Direction;

            staticShot = (StaticShot)AddActionShoter(ShoterType.STATIC_SHOT, this, OnHit, null);
            this.caster.ActionMoviment(moviment, this.coolDown);
        }

        public void OnHit(HeroController target, GameObject shotGO)
        {
            currentHits++;
            if (currentHits <= maxHits)
            {
                Vector3 pushForceHit = new Vector3(0.2f * pushForce.x, 0.5f * pushForce.x, 0);

                if (currentHits == maxHits)
                    pushForceHit = pushForce;

                target.PushCharacter(pushForceHit, hitCoolDown);
                shotGO.GetComponent<HitTarget>().HitCharacter(1f);
                caster.ActionHitDelayedResume(hitCoolDown);

                Vector3 effectPosition = new Vector3(shotGO.transform.position.x + (0.4f * actionDirection.x), shotGO.transform.position.y, shotGO.transform.position.z);
                EffectsManager.PlayEffect(EffectType.HIT, effectPosition);
                //caster.MovingActionHitSpeed ();
                //target.ApplyDamage (DamageType.PHYSICAL, caster.Model.AttackDamage, EffectType.NONE, 0, 0f); 
            }
		}

        private void DestroyAction()
        {
            this.caster.ReleaseHeroOnAir();

            isActive = false;
			staticShot.Dismiss(0.2f);
        }
    }
}