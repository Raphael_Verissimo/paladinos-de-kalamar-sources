﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using Effects;

namespace Actions
{
    public class Jump : Action 
	{
        // Use this for initialization
		void Start () 
		{
			UseAction (CharacterManager.GetHeroController(transform.parent.gameObject));
		}
		
		// Update is called once per frame
		void Update () 
		{
			
		}

		public void UseAction (HeroController caster)
		{
            this.caster = caster;

            Vector3 actionMoviment = Vector3.zero;

            float speedFactor = this.caster.Velocity.x * 0.3f;

            if (speedFactor == 0)
                speedFactor = caster.Direction.x;

            float minSpeed = 1f;
            float maxSpeed = 2.5f;

            if (speedFactor > 0 && speedFactor < minSpeed)
                speedFactor = minSpeed;
            if (speedFactor < 0 && speedFactor > -minSpeed)
                speedFactor = -minSpeed;

            if (speedFactor > maxSpeed)
                speedFactor = maxSpeed;

            if (speedFactor < -maxSpeed)
                speedFactor = -maxSpeed;

            actionMoviment = new Vector3(2f * speedFactor, 6f, caster.Direction.z);
            this.caster.ActionMoviment(actionMoviment, 0f);
        }
	}
}