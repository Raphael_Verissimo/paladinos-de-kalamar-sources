using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Characters;

public delegate void OnShot(HeroController target, GameObject shotGO);
public delegate void OnHit(HeroController target, GameObject shotGO);
public delegate void OnReach(Vector3 position, GameObject shotGO);
public delegate void OnTime(Vector3 position, GameObject shotGO);

namespace Actions
{
    public class Action : MonoBehaviour 
	{	
        internal bool isActive;
        internal bool isActionMoviment;
		internal bool isMainPlayerAction;

		internal int expirationTime;

        internal Vector3 moviment;
        internal Vector3 pushForce;
        internal Vector3 currentPosition;
        internal Vector3 actionDirection;
        internal Vector3 startPosition;
        internal Vector3 desacelerationFactor;
        
        internal float coolDown;
        internal float hitCoolDown;
        internal float movimentCoolDown;
        internal float castDelay;
        internal float startTime;
        internal float maxTime;
		internal float effectAmount;
		internal float effectDuration;

        internal float width;
        internal float range;
        internal float speed;
        internal int maxHits;
        internal float hitSpeed;
        internal int currentHits;

        internal string beforeCastPrefab;
		internal string afterCastPrefab;
		internal string actionPrefab;

		internal GameObject actionGO;
        internal GameObject spriteGO;

        internal IList<ActionShoter> actionShoters = new List<ActionShoter>();
		 
		internal Characters.HeroController caster;

		void start()
		{
			
		}

        internal ActionShoter AddActionShoter (ShoterType type, Action source, OnHit onHit, OnReach onReach)
        {
            this.isActionMoviment = isActionMoviment;
            
			switch (type)
			{
                case ShoterType.SINGLE_SHOT:
					SingleShot singleShot = InstantiateSingleShot ();
                    singleShot.PrepareSingleShot (source, onHit, onReach);						
					actionShoters.Add (singleShot);
					return singleShot;
				break;

                case ShoterType.STATIC_SHOT:
                    StaticShot staticShot = InstantiateStaticShot();
                    staticShot.PrepareStaticShot(source, onHit, onReach);
                    actionShoters.Add(staticShot);
                    return staticShot;
                break;

                /*
                case ShoterType.LOCALIZED_SHOT:
					LocalizedShot localizedShot = InstantiateLocalizedShot ();
                    localizedShot.PrepareLocalizedShot (source, onHit, onReach);						
					actionShoters.Add (localizedShot);
					return localizedShot;
				break;

				case ShoterType.MOBILITY_SHOT:
					MobilityShot mobilityShot = InstantiateMobilityShot ();
					mobilityShot.PrepareMobilityShot (source, onHit, onReach);						
					actionShoters.Add (mobilityShot);
					return mobilityShot;
				break;
                */
            }

			return null;
		}

        private StaticShot InstantiateStaticShot()
        {
            string singleShotPrefab = "Actions/TargetTypes/StaticShot";
            GameObject returnedPrefab = ObjectManager.InstantiateObject(singleShotPrefab, this.transform.position);
            StaticShot staticShot = returnedPrefab.GetComponentInChildren<StaticShot>();

            return staticShot;
        }

        private SingleShot InstantiateSingleShot()
		{
			string singleShotPrefab = "Actions/TargetTypes/SingleShot";
			GameObject returnedPrefab = ObjectManager.InstantiateObject(singleShotPrefab, this.transform.position);		
			SingleShot singleShot = returnedPrefab.GetComponentInChildren<SingleShot>();

			return singleShot;
		}

		private LocalizedShot InstantiateLocalizedShot()
		{
			string localizedShotPrefab = "Actions/TargetTypes/LocalizedShot";
			GameObject returnedPrefab = ObjectManager.InstantiateObject(localizedShotPrefab, this.transform.position);		
			LocalizedShot localizedShot = returnedPrefab.GetComponentInChildren<LocalizedShot>();

			return localizedShot;
		}

		private MobilityShot InstantiateMobilityShot()
		{
			string mobilityShotPrefab = "Actions/TargetTypes/MobilityShot";
			GameObject returnedPrefab = ObjectManager.InstantiateObject(mobilityShotPrefab, this.transform.position);		
			MobilityShot mobilityShot = returnedPrefab.GetComponentInChildren<MobilityShot>();

			return mobilityShot;
		}
	}
}	