﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using Effects;

namespace Actions
{
    public class Dash : Action 
	{
        public float distance;
        public float range;

        public bool hasAttacked;

		// Use this for initialization
		void Start () 
		{
			UseAction (Characters.CharacterManager.GetHeroController(transform.parent.gameObject));
		}
		
		// Update is called once per frame
		void FixedUpdate () 
		{
            if (this.caster.State == CharacterState.DASHING)
            {
                if (this.caster.IsFacingRight)               
                    this.caster.Velocity -= desacelerationFactor;
                else
                    this.caster.Velocity += desacelerationFactor;                
            }

            if (startPosition != null && this.caster != null)
            {
                distance = (this.caster.transform.position - startPosition).magnitude;

                if (distance > 1 && this.caster.Velocity.magnitude < 0.5f)
                {
                    StopAction();
                }
            }
		}

		public void UseAction (Characters.HeroController caster)
		{
            this.caster = caster;

            startPosition = caster.gameObject.transform.position;

            range = 5;
            desacelerationFactor = new Vector3(0.2f, 0, 0);

            Vector3 actionMoviment = new Vector3 (8f * caster.Direction.x, this.caster.Velocity.y, 0);
			this.caster.ActionMoviment (actionMoviment, 0f);
		}

        public void StopAction()
        {
            this.caster.ReleaseHeroOnFloor();

            if (this.gameObject != null)
            {
                DestroyObject(this.gameObject);
            }
        }
	}
}