﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using Effects;

namespace Actions
{
	public class KiBlast : Action 
	{
		public GameObject spriteGO;
		public SingleShot singleShot;

		private Vector3 actionDirection;
		private float hitCooldown;

		void Start () 
		{

		}

		public void UseAction (HeroController caster, float width, float range, float speed, float hitSpeed, int maxHits, float coolDown, float hitCoolDown,
                               Vector3 offset, Vector3 pushForce)
		{	
			isActive = true;
            this.caster = caster;

            this.width = width;
            this.range = range;
            this.speed = speed;
            this.maxHits = maxHits;
            this.hitSpeed = hitSpeed;
            this.coolDown = coolDown;
            this.hitCoolDown = hitCoolDown;
            this.pushForce = pushForce;
			this.startPosition = caster.transform.position;

            this.maxTime = 5f;
            this.currentHits = 0;	

			beforeCastPrefab = "";
			afterCastPrefab = "";
			actionPrefab = "";

			actionDirection = caster.Direction;

			singleShot = (SingleShot)AddActionShoter(ShoterType.SINGLE_SHOT, this, OnHit, OnReach);
			singleShot.transform.parent = this.transform;
			//singleShot.DelayShotMultipleHit ();		
		}

		public void OnHit(HeroController target, GameObject shotGO)
		{
			currentHits++;
            if (currentHits <= maxHits)
            {
				target.PushCharacter(this.pushForce, this.hitCoolDown);
				shotGO.GetComponent<HitTarget> ().HitCharacter (this.hitCoolDown);

				Vector3 effectPosition = new Vector3 (shotGO.transform.position.x + (0.4f * actionDirection.x), shotGO.transform.position.y, shotGO.transform.position.z);
				EffectsManager.PlayEffect(EffectType.HIT, effectPosition);
				singleShot.Dismiss(0.2f);
			}
		}

		public void OnReach(Vector3 position, GameObject shotGO)
		{
			singleShot.Dismiss(0.2f);
		}

		public void OnTime(Vector3 position, GameObject shotGO)
		{
			singleShot.Dismiss(0.2f);
		}
	}
}
