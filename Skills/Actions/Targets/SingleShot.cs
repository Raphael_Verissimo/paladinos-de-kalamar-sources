using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Characters;

namespace Actions
{
	public class SingleShot : ActionShoter
	{		
		public int sourceActionID;	
		public GameObject actionPrefabGO;		
		public int maxExpirationTime = 100;
		public GameObject spriteGO;
        public HitTarget hitTarget;
		public bool isDismissing;

        private BeanController currentBean;

		void FixedUpdate()
		{			
			if (startTime > 0) 
			{
				if ((GameManager.GameTime - startTime) > this.sourceAction.coolDown)
				{
					Shot();
				}
			}

			if (startDestroy > 0) 
			{
				if ((GameManager.GameTime - startDestroy) > destroyTime)
				{
					DelayedDestroy();
				}
			}
		}

		#region Public API 
        public void PrepareSingleShot(Action sourceAction, OnHit onHit, OnReach onReach)
		{
			this.sourceAction = sourceAction;

			startTime = GameManager.GameTime;

			if (this.sourceAction.spriteGO)
				this.sourceAction.spriteGO.GetComponent<SpriteRenderer> ().enabled = false;
		}

		public void Dismiss(float destroyDelay)
		{
			if (!isDismissing) 
			{
				startDestroy = GameManager.GameTime;
				destroyTime = destroyDelay;

				isDismissing = true;
			}
		}

        public void DismissBean()
        {
            if (!isDismissing)
            {
                isDismissing = true;

                currentBean.Dismiss();
            }
        }
		#endregion

		#region Interal Delay Methods 
		private void Shot()
		{
			string singleShotPrefab = "Actions/TargetTypes/SphereHit";
			actionPrefabGO = ObjectManager.InstantiateObject(singleShotPrefab, this.sourceAction.startPosition);	
			//actionPrefabGO.transform.position += new Vector3(offSet.x, offSet.y, offSet.z);
			actionPrefabGO.transform.localScale = actionPrefabGO.transform.localScale * this.sourceAction.width;
			
			HitTarget hitTarget = actionPrefabGO.GetComponentInChildren<HitTarget>();	
			hitTarget.PrepareShot (this.sourceAction);
			hitTargets.Add(hitTarget);
			
			if (spriteGO) {
				spriteGO.transform.position = origPosition;
				spriteGO.GetComponent<ActionSpriteController> ().PlayMove (this.sourceAction.caster.IsFacingRight);
				spriteGO.transform.parent = actionPrefabGO.transform;
				spriteGO.transform.localScale = spriteGO.transform.localScale * this.sourceAction.width;
				spriteGO.GetComponent<SpriteRenderer> ().enabled = true;
			}
			
			hitTarget.Shot();
			startTime = 0;
			shotTime = 0;
		}

		private void DelayedDestroy()
		{			
			if (spriteGO)
				spriteGO.GetComponent<ActionSpriteController> ().PlayDismiss ();
			
			if (this.gameObject)
				Destroy(this.gameObject);
			
			startDestroy = 0;
			destroyTime = 0;
		}
		#endregion
	}
}
