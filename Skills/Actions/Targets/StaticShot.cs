using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Characters;

namespace Actions
{
	public class StaticShot : ActionShoter
	{
        public bool isActive;
		public int sourceActionID;	
		public GameObject actionPrefabGO;
        public GameObject hero;
        public int maxExpirationTime = 100;
        public HitTarget hitTarget;

        public Vector3 offSet;
        public float width;
        public int maxHits;
        public float hitCoolDown;
        public float shotDelay;
        
		void FixedUpdate()
		{
			// TODO Controlar tempo
            //maxExpirationTime--;
            //if (maxExpirationTime < 0) {
            //DestroyObject (actionPrefabGO);
            //}

            if (isActive)
            {				
				if (startTime > 0) 
				{
					if ((GameManager.GameTime - startTime) > this.sourceAction.coolDown)
					{
						Shot();
					}
				}
				
				if (startDestroy > 0) 
				{
					if ((GameManager.GameTime - startDestroy) > destroyTime)
					{
						DelayedDestroy();
					}
				}          
            }
		}

		#region Public API 
        public void PrepareStaticShot(Action sourceAction, OnHit onHit, OnReach onReach)
		{
			this.sourceAction = sourceAction;

			startTime = GameManager.GameTime;

			isActive = true;
        }

		public void Dismiss(float destroyDelay)
        {
			startDestroy = GameManager.GameTime;
			destroyTime = destroyDelay;
        }
        #endregion

        #region Interal Delay Methods 
        private void Shot()
        {
            string singleShotPrefab = "Actions/TargetTypes/StaticHit";
            actionPrefabGO = ObjectManager.InstantiateObject(singleShotPrefab, this.sourceAction.startPosition);
            //actionPrefabGO.transform.position += new Vector3(offSet.x, offSet.y, offSet.z);
            actionPrefabGO.transform.localScale = actionPrefabGO.transform.localScale * this.sourceAction.width;
            actionPrefabGO.transform.parent = this.sourceAction.caster.transform;

            HitTarget hitTarget = actionPrefabGO.GetComponentInChildren<HitTarget>();
            hitTarget.PrepareStaticShot(this.sourceAction);
			hitTarget.Shot();
			startTime = 0;
			shotTime = 0;
        }
		
		private void DelayedDestroy()
		{						
			if (this.gameObject)
				Destroy(this.gameObject);
			
			startDestroy = 0;
			destroyTime = 0;
		}
		#endregion
	}
}
