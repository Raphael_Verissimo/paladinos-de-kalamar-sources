﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;

// TODO Organizar
namespace Actions
{
	public class HitTarget : MonoBehaviour 
	{
		private bool isActive;
		private bool isCollinding;

		private float currentDistance;
		private float currentTime;

		private Vector3 origPosition;

		private bool isBean;
		private Vector3 beanDirection;

		private bool isExpanding;
		private Vector3 expandSpeed;
		private float maxExpansion;

		private bool isPierce;

		private Rigidbody body;
        private Action sourceAction;

        private OnShot onShot;
		private OnHit onHit;
		private OnTime onTime;
		private OnReach onReach;

        // Use this for initialization
        void Start ()
		{
			beanDirection = new Vector3 (0.1f, 0, 0);
			body = GetComponent<Rigidbody> ();
		}
		
		// Update is called once per frame
		void FixedUpdate () 
		{            
			UpdateDistance();
            UpdateBean();
            UpdateExpanding();			
		}

        internal void PrepareStaticShot(Action sourceAction)
        {
            PrepareShot(sourceAction);
        }


        internal void PrepareShot(Action sourceAction)
		{
			this.sourceAction = sourceAction;

			// TODO Adicionar os Delegates
            //this.onShot = sourceAction;
            //this.onHit = onHit;
            //this.onTime = onTime;
            //this.onReach = onReach;
        }

		internal void Shot()
		{
			isBean = false;
			isActive = true;
			isPierce = false;
			isCollinding = true;
		
			origPosition = transform.position;

            if (sourceAction.speed != 0)
            {
                if (!body)
                    body = GetComponent<Rigidbody>();

                body.AddRelativeForce(sourceAction.actionDirection * sourceAction.speed, ForceMode.Impulse);
            }			
		}

		internal void Bean()
		{
			isBean = true;
			isActive = true;
			isPierce = false;
			isCollinding = true;

			origPosition = transform.position;

			body.AddRelativeForce(sourceAction.actionDirection * sourceAction.speed, ForceMode.Impulse);	
		}

		internal void Pierce()
		{
			isBean = false;
			isActive = true;
			isPierce = true;
			isCollinding = true;

			origPosition = transform.position;

			body.AddRelativeForce(sourceAction.actionDirection * sourceAction.speed, ForceMode.Impulse);
		}

		internal void Expand(float width)
		{
			isExpanding = true;

			expandSpeed = new Vector3 (0.1f, 0.1f, 0.1f);
			expandSpeed *= sourceAction.speed;
			maxExpansion = sourceAction.width;
		}

		internal void SpecifcPosition(Vector3 position)
		{
			isActive = true;
			isCollinding = true;
			body.isKinematic = true;
		}

		internal void Stop()
		{
			body.velocity = Vector3.zero;
			body.isKinematic = true;
		}

		internal void DestroyTarget()
		{
			Destroy (this.gameObject);
		}

		internal void HitCollider(GameObject collider, HeroController controller)
		{
            //Debug.Log (currentHits + " Hits < " + maxHits);
            onHit(controller, this.gameObject);
		}

		public void HitCharacter(float delay)
		{
			StartCoroutine (DelayedResumeSpeed (delay));
		}

		IEnumerator DelayedResumeSpeed(float delay)
		{
			Vector3 currentSpeed = body.velocity;
			Stop();
			//Debug.Log ("Parou o Hadouken " + body.velocity);

			yield return new WaitForSeconds (delay);

			//Debug.Log ("Voltou");
			body.isKinematic = false;
			body.AddRelativeForce(currentSpeed, ForceMode.Impulse);	
		}

		void OnTriggerStay(Collider other)
		{
            //Debug.Log ("On Trigger Stay !!! " + other);
            if(other.gameObject.tag.Equals("Player") || other.gameObject.tag.Equals("MainPlayer"))
			{
				HeroController controller = CharacterManager.GetHeroController(other.gameObject);

                if (sourceAction)
                {
                    //Debug.Log (sourceTarget.sourceAction.caster + " != " + controller);
                    if (sourceAction.caster != controller)
                    {
                        if (onHit != null)
                        {
                            if (isActive && isCollinding)
                            {
                                HitCollider(other.gameObject, controller);
                            }
                        }
                    }
                }
			}

            if (other.gameObject.tag.Equals("ActionHit"))
            {          
                //UnityEditor.EditorApplication.isPaused = true;                
                Action casterAction = sourceAction;

                if (sourceAction.caster != casterAction.caster)
                {
                    if (casterAction.isActionMoviment)
                    {
                        DestroyObject(this.gameObject);
                    }
                    else
                    {
                        if (sourceAction.isActionMoviment)
                        {
                            DestroyObject(other.gameObject);
                        }
                        else
                        {
                            DestroyObject(other.gameObject);
                            DestroyObject(this.gameObject);
                        }
                    }
                }
            }
		}

		private void UpdateDistance()
		{
			float distance = (origPosition - transform.position).magnitude;

			//Debug.Log ("Distance:" + distance + " MaxDistance:" + sourceAction.range);

			if (distance > sourceAction.range) 
			{
				if (onReach != null) 
				{
					onReach (transform.position, this.gameObject);
				}
			}
		}

        private void UpdateBean()
        {
            if (isBean)
            {
                if (transform.localScale.x < sourceAction.range)
                {
                    transform.localScale = transform.localScale + beanDirection * (sourceAction.speed / 2.5f);
                }
                else
                {
                    body.velocity = Vector3.zero;
                }
            }
        }

        private void UpdateExpanding()
        {
            if (isExpanding)
            {
                if (transform.localScale.magnitude < maxExpansion)
                {
                    transform.localScale += expandSpeed;
                }
                else
                {
                    if (onReach != null)
                    {
                        onReach(transform.position, this.gameObject);
                    }
                }
            }
        }
    }

	public enum HitType
	{
		BOX,
		SPHERE,
		VERTICAL_CILINDER,
		HORIZONTAL_CILINDER
	}
}
