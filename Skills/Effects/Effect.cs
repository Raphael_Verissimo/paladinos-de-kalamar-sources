﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Effects
{
    public class Effect : MonoBehaviour
    {
    	[SerializeField]	
    	private Sprite[] effectSprite; 
    	
    	[SerializeField]
    	private float interval;	
    	
    	[SerializeField]
    	private bool animationLoops;
    	
    	private float lastTime;
    	private int currentIndex;
    	
    	private SpriteRenderer spriteRenderer;
    	
    	// Use this for initialization
    	void Start ()
    	{
    		currentIndex = 0;
    		lastTime = 0;
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();		
    	}
    	
    	// Update is called once per frame
    	void Update () 
    	{
    		UpdateCurrentSprite ();
    	}
    	
    	private void UpdateCurrentSprite()  
    	{
    		if (Time.fixedTime - lastTime > interval) 
    		{
    			lastTime = Time.fixedTime;
                currentIndex = (currentIndex + 1) % effectSprite.Length;			

                spriteRenderer.sprite = effectSprite[currentIndex];
    			
                if (currentIndex == effectSprite.Length - 1)
    			{
    				if (!animationLoops)
    				{
                        Destroy(this.gameObject);
    				}
    			}
    		}
    	}
    }
}
